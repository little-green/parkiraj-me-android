package hr.fer.littlegreen.parkirajme.androidapp;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.espresso.intent.rule.IntentsTestRule;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.main.AdministratorMainActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.main.CompanyMainActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.login.LoginActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.main.PersonMainActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.intent.Intents.intended;
import static androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class LoginTest {

    @Rule
    public ActivityScenarioRule<LoginActivity> activityRule = new ActivityScenarioRule<>(LoginActivity.class);

    @Rule
    public IntentsTestRule<LoginActivity> intentsTestRule = new IntentsTestRule<>(LoginActivity.class);

    @Test
    public void emptyFormTest() {
        onView(withId(R.id.login))
            .perform(click());
    }

    @Test
    public void correctPersonLoginTest() throws InterruptedException {
        onView(withId(R.id.email)).perform(typeText("ivo.ivic@gmail.com"));
        onView(withId(R.id.password)).perform(typeText("789"), closeSoftKeyboard());
        onView(withId(R.id.login)).perform(click());
        intended(hasComponent(PersonMainActivity.class.getName()));
    }

    @Test
    public void correctCompanyLoginTest() throws InterruptedException {
        onView(withId(R.id.email)).perform(typeText("onemillion@mil.hr"));
        onView(withId(R.id.password)).perform(typeText("onemillion"), closeSoftKeyboard());
        onView(withId(R.id.login)).perform(click());
        intended(hasComponent(CompanyMainActivity.class.getName()));
    }

    @Test
    public void correctAdministratorLoginTest() throws InterruptedException {
        onView(withId(R.id.email)).perform(typeText("admin@gmail.com"));
        onView(withId(R.id.password)).perform(typeText("admin"), closeSoftKeyboard());
        onView(withId(R.id.login)).perform(click());
        intended(hasComponent(AdministratorMainActivity.class.getName()));
    }
}
