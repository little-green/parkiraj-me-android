package hr.fer.littlegreen.parkirajme.androidapp.ui.login.events;

import java.util.Objects;

import androidx.annotation.NonNull;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Company;

public final class CompanyLoginSuccessEvent implements LoginEvent {

    @NonNull
    private final Company company;

    public CompanyLoginSuccessEvent(@NonNull Company company) {
        this.company = company;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CompanyLoginSuccessEvent that = (CompanyLoginSuccessEvent) o;
        return company.equals(that.company);
    }

    @Override
    public int hashCode() {
        return Objects.hash(company);
    }

    @NonNull
    public Company getCompany() {
        return company;
    }
}
