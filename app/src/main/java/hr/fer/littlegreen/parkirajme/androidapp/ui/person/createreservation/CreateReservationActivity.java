package hr.fer.littlegreen.parkirajme.androidapp.ui.person.createreservation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Toast;

import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.timepicker.MaterialTimePicker;
import com.google.android.material.timepicker.TimeFormat;

import java.time.DayOfWeek;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import dagger.hilt.android.AndroidEntryPoint;
import hr.fer.littlegreen.parkirajme.androidapp.R;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.ActivityCreateReservationBinding;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.ParkingObject;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.ReservationType;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.createreservation.events.CreateReservationEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.createreservation.events.ErrorEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.createreservation.events.ReservationCreatedEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.createreservation.state.CreateReservationChooseDateDialog;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.createreservation.state.CreateReservationChooseTimeDialog;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.createreservation.state.CreateReservationDialog;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.createreservation.state.ReservationTypeOption;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.TextViewUtil;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.ViewUtil;

@AndroidEntryPoint
public final class CreateReservationActivity extends BaseActivity {

    @NonNull
    private static final String EXTRA_PARKING_OBJECT = "EXTRA_PARKING_OBJECT";

    private CreateReservationViewModel viewModel;
    private ActivityCreateReservationBinding binding;
    private ArrayAdapter<ReservationTypeOption> reservationTypeOptionAdapter;

    @NonNull
    private final List<ReservationTypeOption> reservationTypeOptions = Arrays.asList(
        new ReservationTypeOption(ReservationType.ONE_TIME, "Jednokratna rezervacija"),
        new ReservationTypeOption(ReservationType.PERIODIC, "Ponavljajuća rezervacija"),
        new ReservationTypeOption(ReservationType.ALL_TIME, "Trajna rezervacija")
    );

    @NonNull
    private final Observer<CreateReservationEvent> eventsObserver = event -> {
        if (event instanceof ReservationCreatedEvent) {
            finish();
        } else if (event instanceof ErrorEvent) {
            String message = ((ErrorEvent) event).getMessage();
            if (message == null) {
                message = getString(R.string.generic_error);
            }
            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        }
    };
    @Nullable
    private DialogFragment currentDialog = null;

    @NonNull
    public static Intent newIntent(@NonNull Context context, @NonNull ParkingObject parkingObject) {
        return new Intent(context, CreateReservationActivity.class)
            .putExtra(EXTRA_PARKING_OBJECT, parkingObject);
    }

    @NonNull
    private final Observer<Set<DayOfWeek>> daysObserver = days -> {
        updateCheckBox(binding.createReservationMondayCheckBox, days.contains(DayOfWeek.MONDAY));
        updateCheckBox(binding.createReservationTuesdayCheckBox, days.contains(DayOfWeek.TUESDAY));
        updateCheckBox(binding.createReservationWednesdayCheckBox, days.contains(DayOfWeek.WEDNESDAY));
        updateCheckBox(binding.createReservationThursdayCheckBox, days.contains(DayOfWeek.THURSDAY));
        updateCheckBox(binding.createReservationFridayCheckBox, days.contains(DayOfWeek.FRIDAY));
        updateCheckBox(binding.createReservationSaturdayCheckBox, days.contains(DayOfWeek.SATURDAY));
        updateCheckBox(binding.createReservationSundayCheckBox, days.contains(DayOfWeek.SUNDAY));
    };

    @NonNull
    private final Observer<CreateReservationDialog> dialogObserver = dialog -> {
        if (dialog == null) {
            if (currentDialog != null) {
                currentDialog.dismiss();
            }
            currentDialog = null;
        } else if (dialog instanceof CreateReservationChooseDateDialog) {
            CreateReservationChooseDateDialog data = (CreateReservationChooseDateDialog) dialog;
            MaterialDatePicker<Long> materialDatePicker = MaterialDatePicker.Builder.datePicker()
                .setInputMode(MaterialDatePicker.INPUT_MODE_CALENDAR)
                .setTitleText(data.getTitle())
                .setSelection(data.getSelection())
                .build();
            materialDatePicker.addOnPositiveButtonClickListener(data.getListener()::onDateSelected);
            materialDatePicker.addOnCancelListener(d -> viewModel.dismissDialog());
            materialDatePicker.addOnDismissListener(d -> viewModel.dismissDialog());
            materialDatePicker.addOnNegativeButtonClickListener(d -> viewModel.dismissDialog());
            materialDatePicker.show(getSupportFragmentManager(), "");
            currentDialog = materialDatePicker;
        } else if (dialog instanceof CreateReservationChooseTimeDialog) {
            CreateReservationChooseTimeDialog data = (CreateReservationChooseTimeDialog) dialog;
            MaterialTimePicker materialTimePicker = new MaterialTimePicker.Builder()
                .setInputMode(MaterialTimePicker.INPUT_MODE_CLOCK)
                .setTimeFormat(TimeFormat.CLOCK_24H)
                .setTitleText(data.getTitle())
                .setHour(data.getHour())
                .setMinute(data.getMinute())
                .build();
            materialTimePicker.addOnPositiveButtonClickListener(v ->
                data.getListener().onTimeSelected(materialTimePicker.getHour(), materialTimePicker.getMinute())
            );
            materialTimePicker.addOnCancelListener(d -> viewModel.dismissDialog());
            materialTimePicker.addOnDismissListener(d -> viewModel.dismissDialog());
            materialTimePicker.addOnNegativeButtonClickListener(d -> viewModel.dismissDialog());
            materialTimePicker.show(getSupportFragmentManager(), "");
            currentDialog = materialTimePicker;
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(CreateReservationViewModel.class);
        viewModel.getParkingObjectAddress().observe(this, t -> TextViewUtil.update(binding.createReservationParkingObjectAddressValue, t));
        viewModel.getReservationType().observe(this, t ->
            binding.createReservationTypeValueField.setText(getReservationTypeText(t), false)
        );
        viewModel.getDateSectionVisibility().observe(this, v -> ViewUtil.updateVisibility(binding.createReservationDateLayout, v));
        viewModel.getDate().observe(this, t -> TextViewUtil.update(binding.createReservationDateValueField, t));
        viewModel.getTimeSectionVisibility().observe(this, v -> ViewUtil.updateVisibility(binding.createReservationTimeLayout, v));
        viewModel.getStartTime().observe(this, t -> TextViewUtil.update(binding.createReservationStartTimeValueField, t));
        viewModel.getEndTime().observe(this, t -> TextViewUtil.update(binding.createReservationEndTimeValueField, t));
        viewModel.getDaysSectionVisibility().observe(this, v -> ViewUtil.updateVisibility(binding.createReservationDaysLayout, v));
        viewModel.getDays().observe(this, daysObserver);
        viewModel.getDialog().observe(this, dialogObserver);
        viewModel.getEvents().observe(this, eventsObserver);
        viewModel.initialize((ParkingObject) getIntent().getSerializableExtra(EXTRA_PARKING_OBJECT));
        reservationTypeOptionAdapter = new ArrayAdapter<>(this, R.layout.item_reservation_type, reservationTypeOptions);
        binding = ActivityCreateReservationBinding.inflate(getLayoutInflater());
        binding.createReservationCloseButton.setOnClickListener(v -> finish());
        binding.createReservationTypeValueField.setAdapter(reservationTypeOptionAdapter);
        binding.createReservationTypeValueField.setOnItemClickListener((parent, view, position, id) ->
            viewModel.selectReservationType(reservationTypeOptions.get(position).getReservationType())
        );
        binding.createReservationDateValueField.setOnClickListener(v -> viewModel.selectDate());
        binding.createReservationStartTimeValueField.setOnClickListener(v -> viewModel.selectStartTime());
        binding.createReservationEndTimeValueField.setOnClickListener(v -> viewModel.selectEndTime());
        binding.createReservationMondayCheckBox.setOnCheckedChangeListener((v, isChecked) -> viewModel.updateDaySelection(
            DayOfWeek.MONDAY, isChecked
        ));
        binding.createReservationTuesdayCheckBox.setOnCheckedChangeListener((v, isChecked) -> viewModel.updateDaySelection(
            DayOfWeek.TUESDAY, isChecked
        ));
        binding.createReservationWednesdayCheckBox.setOnCheckedChangeListener((v, isChecked) -> viewModel.updateDaySelection(
            DayOfWeek.WEDNESDAY, isChecked
        ));
        binding.createReservationThursdayCheckBox.setOnCheckedChangeListener((v, isChecked) -> viewModel.updateDaySelection(
            DayOfWeek.THURSDAY, isChecked
        ));
        binding.createReservationFridayCheckBox.setOnCheckedChangeListener((v, isChecked) -> viewModel.updateDaySelection(
            DayOfWeek.FRIDAY, isChecked
        ));
        binding.createReservationSaturdayCheckBox.setOnCheckedChangeListener((v, isChecked) -> viewModel.updateDaySelection(
            DayOfWeek.SATURDAY, isChecked
        ));
        binding.createReservationSundayCheckBox.setOnCheckedChangeListener((v, isChecked) -> viewModel.updateDaySelection(
            DayOfWeek.SUNDAY, isChecked
        ));
        binding.createReservationActionButton.setOnClickListener(v -> viewModel.createReservation());
        setContentView(binding.getRoot());
    }

    private void updateCheckBox(@NonNull CheckBox checkBox, boolean isChecked) {
        if (checkBox.isChecked() != isChecked) {
            checkBox.setChecked(isChecked);
        }
    }

    @NonNull
    private String getReservationTypeText(@NonNull ReservationType reservationType) {
        for (ReservationTypeOption reservationTypeOption : reservationTypeOptions) {
            if (reservationTypeOption.getReservationType() == reservationType) {
                return reservationTypeOption.getText();
            }
        }
        return "";
    }
}
