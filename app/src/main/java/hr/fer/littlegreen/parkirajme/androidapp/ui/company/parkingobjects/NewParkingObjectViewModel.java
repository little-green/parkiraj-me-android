package hr.fer.littlegreen.parkirajme.androidapp.ui.company.parkingobjects;

import org.jetbrains.annotations.NotNull;

import java.math.BigDecimal;

import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.ParkirajMeService;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.models.parkingobject.ParkingObjectRequest;
import hr.fer.littlegreen.parkirajme.androidapp.data.local.LocalStorage;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseViewModel;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.parkingobjects.events.FailedAddNewParkingObjectEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.parkingobjects.events.FinishAddNewParkingObjectEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.parkingobjects.events.ParkingObjectEvent;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class NewParkingObjectViewModel extends BaseViewModel<ParkingObjectEvent> {

    @NotNull
    private final ParkirajMeService parkirajMeService;

    @NonNull
    private final LocalStorage localStorage;

    @ViewModelInject
    public NewParkingObjectViewModel(
        @NotNull ParkirajMeService parkirajMeService,
        @NonNull LocalStorage localStorage
    ) {
        this.parkirajMeService = parkirajMeService;
        this.localStorage = localStorage;
    }

    public void addNewParkingObject(
        String name, String address, int capacity, int thirtyMinPrice, BigDecimal longitude,
        BigDecimal latitude
    ) {
        disposable.add(parkirajMeService.addParkingObject(new ParkingObjectRequest(name, address, capacity, thirtyMinPrice,
            (int) (Math.random() * capacity),
            latitude, longitude
        ), localStorage.getSession().getToken()).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(
            () -> emitEvent(new FinishAddNewParkingObjectEvent())
            , throwable -> {
                emitEvent(new FailedAddNewParkingObjectEvent());
            }
        ));
    }
}
