package hr.fer.littlegreen.parkirajme.androidapp.di;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.time.LocalDateTime;
import java.time.YearMonth;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ApplicationComponent;
import hr.fer.littlegreen.parkirajme.androidapp.BuildConfig;
import hr.fer.littlegreen.parkirajme.androidapp.data.LocalDateTimeJsonConverter;
import hr.fer.littlegreen.parkirajme.androidapp.data.UserConverter;
import hr.fer.littlegreen.parkirajme.androidapp.data.YearMonthJsonConverter;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.AuthenticationInterceptor;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.ParkirajMeService;
import hr.fer.littlegreen.parkirajme.androidapp.data.local.LocalStorage;
import hr.fer.littlegreen.parkirajme.androidapp.data.local.SharedPreferencesLocalStorage;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.User;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
@InstallIn(ApplicationComponent.class)
public class AppModule {

    @Provides
    @Singleton
    public OkHttpClient provideOkHttpClient(AuthenticationInterceptor authenticationInterceptor) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
            .addInterceptor(authenticationInterceptor);
        if (BuildConfig.DEBUG) {
            builder.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
        }
        return builder.build();
    }

    @Provides
    @Singleton
    public Retrofit provideRetrofit(OkHttpClient okHttpClient, Gson gson) {
        return new Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BuildConfig.API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .build();
    }

    @Provides
    @Singleton
    public ParkirajMeService provideApiService(Retrofit retrofit) {
        return retrofit.create(ParkirajMeService.class);
    }

    @Provides
    @Singleton
    public Gson provideGson(
        YearMonthJsonConverter yearMonthJsonConverter,
        UserConverter userConverter,
        LocalDateTimeJsonConverter localDateTimeJsonConverter
    ) {
        return new GsonBuilder()
            .registerTypeAdapter(LocalDateTime.class, localDateTimeJsonConverter)
            .registerTypeAdapter(YearMonth.class, yearMonthJsonConverter)
            .registerTypeAdapter(User.class, userConverter)
            .create();
    }

    @Provides
    @Singleton
    public LocalStorage provideDefaultSharedPreferencesLocalStorage(SharedPreferencesLocalStorage sharedPreferencesLocalStorage) {
        return sharedPreferencesLocalStorage;
    }
}
