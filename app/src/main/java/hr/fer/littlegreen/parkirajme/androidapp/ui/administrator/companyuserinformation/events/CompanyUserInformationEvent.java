package hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.companyuserinformation.events;

import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseEvent;

public interface CompanyUserInformationEvent extends BaseEvent {
}
