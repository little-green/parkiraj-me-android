package hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.users;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import dagger.hilt.android.AndroidEntryPoint;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.FragmentUsersBinding;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Administrator;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Company;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Person;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.User;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseFragment;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.adminuserinformation.AdministratorUserInformationActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.companyuserinformation.CompanyUserInformationActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.personuserdetails.PersonUserInformationActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.users.events.UserSelectedEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.users.adapters.UserAdapter;

@AndroidEntryPoint
public class UsersFragment extends BaseFragment {

    private final Observer<BaseEvent> eventObserver = event -> {
        if (event instanceof UserSelectedEvent) {
            User user = ((UserSelectedEvent) event).getUser();
            if (user instanceof Person){
                startActivity(PersonUserInformationActivity.newIntent(
                    requireContext(),
                    (Person)user
               ));
            }
            else if(user instanceof Company){
                startActivity(CompanyUserInformationActivity.newIntent(
                    requireContext(),
                    (Company)user
                ));
            }
            else if(user instanceof Administrator){
                startActivity(AdministratorUserInformationActivity.newIntent(
                    requireContext(),
                    (Administrator)user
                ));
            }
        }
    };
    private UserAdapter adapter;

    private UsersViewModel viewModel;
    private FragmentUsersBinding binding;

    @NonNull
    private final Observer<List<User>> userObserver = user -> {
        adapter.submitList(user);
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentUsersBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(this).get(UsersViewModel.class);
        viewModel.getUsers().observe(getViewLifecycleOwner(), userObserver);
        viewModel.getEvents().observe(getViewLifecycleOwner(), eventObserver);
        viewModel.initialize();

        adapter = new UserAdapter(viewModel::onUserSelected);
        binding.usersList.setLayoutManager(new LinearLayoutManager(requireContext()));
        binding.usersList.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
