package hr.fer.littlegreen.parkirajme.androidapp.ui.registration.steps.person.personalinformation;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.LiveData;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseViewModel;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state.PersonPersonalInformationFormState;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state.RegistrationFlowState;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state.RegistrationStepSequenceManager;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.StringUtil;

public final class PersonalInformationViewModel extends BaseViewModel<BaseEvent> {

    @NonNull
    private final RegistrationStepSequenceManager registrationStepSequenceManager;

    @NonNull
    private final RegistrationFlowState registrationFlowState;

    @ViewModelInject
    public PersonalInformationViewModel(
        @NonNull RegistrationStepSequenceManager registrationStepSequenceManager,
        @NonNull RegistrationFlowState registrationFlowState
    ) {
        this.registrationStepSequenceManager = registrationStepSequenceManager;
        this.registrationFlowState = registrationFlowState;
    }

    public void onOibChanged(String oib) {
        PersonPersonalInformationFormState personPersonalInformationFormState = getOrCreatePersonPersonalInformationFormState();
        registrationFlowState.personPersonalInformationFormState.setValue(new PersonPersonalInformationFormState(
            oib,
            personPersonalInformationFormState.getName(),
            personPersonalInformationFormState.getSurname()
        ));
    }

    public void onNameChanged(String name) {
        PersonPersonalInformationFormState personPersonalInformationFormState = getOrCreatePersonPersonalInformationFormState();
        registrationFlowState.personPersonalInformationFormState.setValue(new PersonPersonalInformationFormState(
            personPersonalInformationFormState.getOib(),
            name,
            personPersonalInformationFormState.getSurname()
        ));
    }

    public void onSurnameChanged(String surname) {
        PersonPersonalInformationFormState personPersonalInformationFormState = getOrCreatePersonPersonalInformationFormState();
        registrationFlowState.personPersonalInformationFormState.setValue(new PersonPersonalInformationFormState(
            personPersonalInformationFormState.getOib(),
            personPersonalInformationFormState.getName(),
            surname
        ));
    }

    public void onContinueClicked() {
        if (isValid(registrationFlowState.personPersonalInformationFormState.getValue())) {
            registrationStepSequenceManager.iterateForward();
        }
    }

    @NonNull
    public LiveData<PersonPersonalInformationFormState> getPersonPersonalInformationFormState() {
        return registrationFlowState.personPersonalInformationFormState;
    }

    @NonNull
    private PersonPersonalInformationFormState getOrCreatePersonPersonalInformationFormState() {
        PersonPersonalInformationFormState personPersonalInformationFormState =
            registrationFlowState.personPersonalInformationFormState.getValue();
        if (personPersonalInformationFormState != null) {
            return personPersonalInformationFormState;
        } else {
            return new PersonPersonalInformationFormState(null, null, null);
        }
    }

    private boolean isValid(@Nullable PersonPersonalInformationFormState personPersonalInformationFormState) {
        return personPersonalInformationFormState != null && StringUtil.areNotNullOrEmpty(
            personPersonalInformationFormState.getOib(),
            personPersonalInformationFormState.getName(),
            personPersonalInformationFormState.getSurname()
        );
    }
}
