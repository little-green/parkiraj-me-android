package hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.lifecycle.ViewModelProvider;
import dagger.hilt.android.AndroidEntryPoint;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import hr.fer.littlegreen.parkirajme.androidapp.R;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.ActivityAdministratorMainBinding;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Administrator;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Company;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseFragment;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.information.AdministratorInformationFragment;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.users.UsersFragment;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.main.state.AdministratorScreenType;

@AndroidEntryPoint
public class AdministratorMainActivity extends BaseActivity {

    @NonNull
    private static final String EXTRA_ADMINISTRATOR = "EXTRA_ADMINISTRATOR";
    private AdministratorMainViewModel viewModel;
    private ActivityAdministratorMainBinding binding;
    @Nullable
    private BaseFragment currentStep;
    @NonNull
    private final Observer<AdministratorScreenType> administratorScreenTypeObserver = administratorScreenType -> {
        switch (administratorScreenType) {
            case USERS:
                setFragment(new UsersFragment());
                break;
            case ADMINDETAILS:
                setFragment(new AdministratorInformationFragment());
                break;
            default:
                removeFragment();
        }
    };

    @NonNull
    public static Intent newIntent(@NonNull Context context, @NonNull Administrator company) {
        Intent intent = new Intent(context, AdministratorMainActivity.class)
            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(EXTRA_ADMINISTRATOR, company);
        return intent;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Administrator administrator = (Administrator) getIntent().getSerializableExtra(EXTRA_ADMINISTRATOR);
        viewModel = new ViewModelProvider(this).get(AdministratorMainViewModel.class);
        viewModel.initialize(administrator);
        viewModel.getClientScreenType().observe(this, administratorScreenTypeObserver);
        binding = ActivityAdministratorMainBinding.inflate(getLayoutInflater());
        binding.administratorScreenNavigation.setOnNavigationItemSelectedListener(item -> {
            int id = item.getItemId();
            if (id == R.id.administratorScreenUsers) {
                viewModel.onAdministratorScreenSelected(AdministratorScreenType.USERS);
                return true;
            } else {
                viewModel.onAdministratorScreenSelected(AdministratorScreenType.ADMINDETAILS);
                return true;
            }});
        setContentView(binding.getRoot());
    }

    private void setFragment(BaseFragment fragment) {
        currentStep = fragment;
        getSupportFragmentManager()
            .beginTransaction()
            .replace(R.id.administratorScreenContainer, fragment)
            .commit();
    }

    private void removeFragment() {
        getSupportFragmentManager()
            .beginTransaction()
            .remove(currentStep)
            .commit();
    }
}
