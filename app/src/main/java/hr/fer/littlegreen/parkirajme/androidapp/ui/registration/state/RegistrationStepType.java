package hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state;

public enum RegistrationStepType {

    USER_TYPE,
    PERSON_PERSONAL_INFORMATION,
    PERSON_VEHICLE_INFORMATION,
    PERSON_CARD_INFORMATION,
    BUSINESS_INFORMATION,
    LOGIN_INFORMATION
}
