package hr.fer.littlegreen.parkirajme.androidapp.ui.registration.di;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ActivityRetainedComponent;
import dagger.hilt.android.scopes.ActivityRetainedScoped;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state.RegistrationFlowState;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state.RegistrationStepSequenceManager;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state.RegistrationStepSequenceManagerImpl;

@Module
@InstallIn(ActivityRetainedComponent.class)
public final class RegistrationModule {

    @Provides
    @ActivityRetainedScoped
    public RegistrationStepSequenceManager provideRegistrationStepSequenceManager(RegistrationStepSequenceManagerImpl registrationStepSequenceManagerImpl) {
        return registrationStepSequenceManagerImpl;
    }

    @Provides
    @ActivityRetainedScoped
    public RegistrationFlowState provideRegistrationFlowState() {
        return new RegistrationFlowState();
    }
}
