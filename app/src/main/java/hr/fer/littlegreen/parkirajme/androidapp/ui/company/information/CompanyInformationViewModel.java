package hr.fer.littlegreen.parkirajme.androidapp.ui.company.information;

import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.ParkirajMeService;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.models.company.CompanyEditRequest;
import hr.fer.littlegreen.parkirajme.androidapp.data.local.LocalStorage;
import hr.fer.littlegreen.parkirajme.androidapp.data.local.Session;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Company;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseViewModel;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.information.events.AccountDeletedEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.information.events.CompanyAddressEditSuccessfulEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.information.events.CompanyEditFailedEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.information.events.CompanyInformationEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.information.events.CompanyNameEditSuccessfulEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.information.events.LogoutEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.information.state.CompanyInformationDialogType;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class CompanyInformationViewModel extends BaseViewModel<CompanyInformationEvent> {

    @NonNull
    private final ParkirajMeService parkirajMeService;

    @NonNull
    private final LocalStorage localStorage;

    @NonNull
    private final MutableLiveData<Company> company = new MutableLiveData<>();

    @NonNull
    private final MutableLiveData<CompanyInformationDialogType> dialogType = new MutableLiveData<>();

    @ViewModelInject
    public CompanyInformationViewModel(
        @NonNull ParkirajMeService parkirajMeService,
        @NonNull LocalStorage localStorage
    ) {
        this.parkirajMeService = parkirajMeService;
        this.localStorage = localStorage;
    }

    public void initialize() {
        company.setValue((Company) localStorage.getSession().getUser());
    }

    public void onLogoutClicked() {
        dialogType.setValue(CompanyInformationDialogType.LOGOUT_CONFIRMATION);
    }

    public void onDeleteAccountClicked() {
        dialogType.setValue(CompanyInformationDialogType.DELETE_ACCOUNT_CONFIRMATION);
    }

    public void dismissDialog() {
        dialogType.setValue(null);
    }

    public void logout() {
        dialogType.setValue(null);
        disposable.add(
            parkirajMeService.logout()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    localStorage.clearSession();
                    emitEvent(new LogoutEvent());
                }, throwable -> dialogType.setValue(CompanyInformationDialogType.ERROR))
        );
    }

    public void deleteAccount() {
        dialogType.setValue(null);
        disposable.add(
            parkirajMeService.deleteAccount(company.getValue().getUserUuid())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    localStorage.clearSession();
                    emitEvent(new AccountDeletedEvent());
                }, throwable -> dialogType.setValue(CompanyInformationDialogType.ERROR))
        );
    }

    public void editAddress(String address) {
        disposable.add(
            parkirajMeService.editCompany(company.getValue().getUserUuid(),
                new CompanyEditRequest(((Company) localStorage.getSession().getUser()).getName(), address),
                localStorage.getSession().getToken()
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    Company currentCompany = (Company) localStorage.getSession().getUser();
                    Company company = new Company(currentCompany.getUserUuid(), currentCompany.getEmail(), currentCompany.getOib(),
                        currentCompany.getName(),
                        address
                    );
                    String token = localStorage.getSession().getToken();
                    localStorage.clearSession();
                    localStorage.putSession(new Session(token, company));
                    emitEvent(new CompanyAddressEditSuccessfulEvent(address));
                }, throwable -> emitEvent(new CompanyEditFailedEvent()))
        );
    }

    public void editName(String name) {
        disposable.add(
            parkirajMeService.editCompany(company.getValue().getUserUuid(),
                new CompanyEditRequest(name, ((Company) localStorage.getSession().getUser()).getHeadquarterAddress()),
                localStorage.getSession().getToken()
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    Company currentCompany = (Company) localStorage.getSession().getUser();
                    Company company = new Company(currentCompany.getUserUuid(), currentCompany.getEmail(), currentCompany.getOib(), name,
                        currentCompany.getHeadquarterAddress()
                    );
                    String token = localStorage.getSession().getToken();
                    localStorage.clearSession();
                    localStorage.putSession(new Session(token, company));
                    emitEvent(new CompanyNameEditSuccessfulEvent(name));
                }, throwable -> emitEvent(new CompanyEditFailedEvent()))
        );
    }

    @NonNull
    public LiveData<Company> getCompany() {
        return company;
    }

    @NonNull
    public LiveData<CompanyInformationDialogType> getDialogType() {
        return dialogType;
    }
}
