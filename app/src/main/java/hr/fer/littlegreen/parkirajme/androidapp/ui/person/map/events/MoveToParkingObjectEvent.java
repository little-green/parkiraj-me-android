package hr.fer.littlegreen.parkirajme.androidapp.ui.person.map.events;

import java.util.Objects;

import androidx.annotation.NonNull;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.ParkingObject;

public final class MoveToParkingObjectEvent implements MapEvent {

    @NonNull
    private final ParkingObject parkingObject;

    public MoveToParkingObjectEvent(@NonNull ParkingObject parkingObject) {
        this.parkingObject = parkingObject;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MoveToParkingObjectEvent that = (MoveToParkingObjectEvent) o;
        return parkingObject.equals(that.parkingObject);
    }

    @Override
    public int hashCode() {
        return Objects.hash(parkingObject);
    }

    @NonNull
    public ParkingObject getParkingObject() {
        return parkingObject;
    }
}
