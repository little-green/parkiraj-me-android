package hr.fer.littlegreen.parkirajme.androidapp.ui.person.main.state;

public enum PersonScreenType {

    MAP,
    RESERVATIONS,
    DETAILS
}
