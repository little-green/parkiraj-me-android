package hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state;

import java.util.Objects;

import androidx.annotation.Nullable;

public final class CardInformationFormState {

    @Nullable
    private final String cardNumber;

    @Nullable
    private final String expiryDate;

    @Nullable
    private final String cvv;

    public CardInformationFormState(@Nullable String cardNumber, @Nullable String expiryDate, @Nullable String cvv) {
        this.cardNumber = cardNumber;
        this.expiryDate = expiryDate;
        this.cvv = cvv;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CardInformationFormState that = (CardInformationFormState) o;
        return Objects.equals(cardNumber, that.cardNumber) &&
            Objects.equals(expiryDate, that.expiryDate) &&
            Objects.equals(cvv, that.cvv);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cardNumber, expiryDate, cvv);
    }

    @Nullable
    public String getCardNumber() {
        return cardNumber;
    }

    @Nullable
    public String getExpiryDate() {
        return expiryDate;
    }

    @Nullable
    public String getCvv() {
        return cvv;
    }
}
