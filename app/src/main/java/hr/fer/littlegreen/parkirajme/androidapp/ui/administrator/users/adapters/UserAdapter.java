package hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.users.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.ItemUsersBinding;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Administrator;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Company;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Person;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.User;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.SimpleDiffCallback;

public final class UserAdapter extends ListAdapter<User, UserAdapter.ViewHolder> {

    @NonNull
    private UserClickListener clickListener;

    public UserAdapter(@NotNull UserClickListener clickListener) {
        super(new SimpleDiffCallback<>());
        this.clickListener = clickListener;
    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        return new ViewHolder(ItemUsersBinding.inflate(LayoutInflater.from(viewGroup.getContext()), viewGroup,
            false
        ));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(getItem(position));
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @NonNull
        private ItemUsersBinding binding;

        @NonNull
        private Context context;

        ViewHolder(@NonNull ItemUsersBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            context = binding.getRoot().getContext();
        }

        void bind(@NonNull User user){
            binding.userItemTitle.setText(user.getEmail());
            binding.userItemType.setText(getUserTypeText(user));
            binding.getRoot().setOnClickListener(v -> clickListener.onClick(user));
        }

        @NonNull
        private String getUserTypeText(@NonNull User user){
            if(user instanceof Person) return "Korisnik";
            else if (user instanceof Company) return "Tvrtka";
            else if (user instanceof Administrator) return "Administrator";
            else if (user instanceof User) return "User";
            else return "ERROR";
        }

        public TextView getTextView() {
            return binding.userItemTitle;
        }
    }
}
