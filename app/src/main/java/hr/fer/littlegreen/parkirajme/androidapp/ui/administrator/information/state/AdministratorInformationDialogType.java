package hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.information.state;

public enum AdministratorInformationDialogType {

    LOGOUT_CONFIRMATION,
    ERROR
}
