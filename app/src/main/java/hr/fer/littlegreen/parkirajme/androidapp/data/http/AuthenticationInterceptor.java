package hr.fer.littlegreen.parkirajme.androidapp.data.http;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import hr.fer.littlegreen.parkirajme.androidapp.data.local.LocalStorage;
import hr.fer.littlegreen.parkirajme.androidapp.data.local.Session;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public final class AuthenticationInterceptor implements Interceptor {

    @NonNull
    private static final String HEADER_AUTHENTICATION_TOKEN = "Authentication-Token";

    @NonNull
    private final LocalStorage localStorage;

    @Inject
    public AuthenticationInterceptor(@NonNull LocalStorage localStorage) {
        this.localStorage = localStorage;
    }

    @Override
    public @NotNull Response intercept(@NotNull Chain chain) throws IOException {
        Session session = localStorage.getSession();
        Request.Builder requestBuilder = chain.request().newBuilder();
        if (session != null) {
            requestBuilder.header(HEADER_AUTHENTICATION_TOKEN, session.getToken());
        }
        return chain.proceed(requestBuilder.build());
    }
}
