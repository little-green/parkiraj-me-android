package hr.fer.littlegreen.parkirajme.androidapp.ui.login.state;

import androidx.annotation.Nullable;

/**
 * Data validation state of the login form.
 */
public final class LoginFormState {

    @Nullable
    private final String email;

    @Nullable
    private final String password;

    public LoginFormState(@Nullable String email, @Nullable String password) {
        this.email = email;
        this.password = password;
    }

    @Nullable
    public String getEmail() {
        return email;
    }

    @Nullable
    public String getPassword() {
        return password;
    }
}
