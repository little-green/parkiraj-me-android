package hr.fer.littlegreen.parkirajme.androidapp.ui.person.reservations;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.ParkirajMeService;
import hr.fer.littlegreen.parkirajme.androidapp.data.local.LocalStorage;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.ReservationAndParkingObjectPair;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseViewModel;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.reservations.events.ErrorEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.reservations.events.ReservationSelectedEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.reservations.events.ReservationsEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.ErrorExtractor;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.schedulers.Schedulers;
import timber.log.Timber;

public final class ReservationsViewModel extends BaseViewModel<ReservationsEvent> {

    @NonNull
    private final ParkirajMeService parkirajMeService;

    @NonNull
    private final LocalStorage localStorage;

    @NonNull
    private final ErrorExtractor errorExtractor;

    @NonNull
    private final MutableLiveData<List<ReservationAndParkingObjectPair>> reservations = new MutableLiveData<>();

    @ViewModelInject
    public ReservationsViewModel(
        @NonNull ParkirajMeService parkirajMeService, @NonNull LocalStorage localStorage,
        @NonNull ErrorExtractor errorExtractor
    ) {
        this.parkirajMeService = parkirajMeService;
        this.localStorage = localStorage;
        this.errorExtractor = errorExtractor;
    }

    public void initialize() {
        disposable.add(
            parkirajMeService.getReservations(localStorage.getSession().getUser().getUserUuid())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    reservations.setValue(result.getReservations());
                }, throwable -> {
                    Timber.e(throwable);
                    emitEvent(new ErrorEvent(errorExtractor.extract(throwable)));
                })
        );
    }

    public void onReservationSelected(ReservationAndParkingObjectPair reservation) {
        emitEvent(new ReservationSelectedEvent(reservation));
    }

    @NonNull
    public LiveData<List<ReservationAndParkingObjectPair>> getReservations() {
        return reservations;
    }
}
