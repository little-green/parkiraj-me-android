package hr.fer.littlegreen.parkirajme.androidapp.ui.registration.steps.logininformation.events;

import java.util.Objects;

import androidx.annotation.NonNull;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Company;

public final class CompanyRegistrationSuccessEvent implements LoginInformationStepEvent {

    @NonNull
    private final Company company;

    public CompanyRegistrationSuccessEvent(@NonNull Company company) {
        this.company = company;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CompanyRegistrationSuccessEvent that = (CompanyRegistrationSuccessEvent) o;
        return company.equals(that.company);
    }

    @Override
    public int hashCode() {
        return Objects.hash(company);
    }

    @NonNull
    public Company getCompany() {
        return company;
    }
}
