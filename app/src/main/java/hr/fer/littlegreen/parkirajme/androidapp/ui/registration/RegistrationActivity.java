package hr.fer.littlegreen.parkirajme.androidapp.ui.registration;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import dagger.hilt.android.AndroidEntryPoint;
import hr.fer.littlegreen.parkirajme.androidapp.R;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.ActivityRegistrationBinding;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseFragment;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.events.FinishEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.events.RegistrationEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state.RegistrationStepType;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.steps.company.companyinformation.CompanyInformationStepFragment;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.steps.logininformation.LoginInformationStepFragment;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.steps.person.cardinformation.CardInformationStepFragment;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.steps.person.personalinformation.PersonalInformationStepFragment;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.steps.person.vehicleinformation.VehicleInformationStepFragment;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.steps.usertype.UserTypeStepFragment;

@AndroidEntryPoint
public final class RegistrationActivity extends BaseActivity {

    private RegistrationViewModel viewModel;
    private ActivityRegistrationBinding binding;

    @NonNull
    private final Observer<Double> progressObserver = progress -> {
        int maxProgress = binding.registrationFlowProgressBar.getMax();
        binding.registrationFlowProgressBar.setProgress((int) (progress * maxProgress));
    };

    @NonNull
    private final Observer<RegistrationEvent> eventObserver = event -> {
        if (event instanceof FinishEvent) {
            finish();
        }
    };

    @Nullable
    private BaseFragment currentStep;

    private final Observer<RegistrationStepType> registrationStepTypeObserver = registrationStepType -> {
        switch (registrationStepType) {
            case USER_TYPE:
                setFragment(new UserTypeStepFragment());
                break;
            case PERSON_PERSONAL_INFORMATION:
                setFragment(new PersonalInformationStepFragment());
                break;
            case PERSON_VEHICLE_INFORMATION:
                setFragment(new VehicleInformationStepFragment());
                break;
            case PERSON_CARD_INFORMATION:
                setFragment(new CardInformationStepFragment());
                break;
            case BUSINESS_INFORMATION:
                setFragment(new CompanyInformationStepFragment());
                break;
            case LOGIN_INFORMATION:
                setFragment(new LoginInformationStepFragment());
                break;
            default:
                removeFragment();
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(RegistrationViewModel.class);
        viewModel.getRegistrationStepType().observe(this, registrationStepTypeObserver);
        viewModel.getProgress().observe(this, progressObserver);
        viewModel.getEvents().observe(this, eventObserver);
        binding = ActivityRegistrationBinding.inflate(getLayoutInflater());
        binding.registrationFlowBackButton.setOnClickListener(v -> onBackPressed());
        setContentView(binding.getRoot());
    }

    @Override
    public void onBackPressed() {
        viewModel.onBackPressed();
    }

    private void setFragment(BaseFragment fragment) {
        currentStep = fragment;
        getSupportFragmentManager()
            .beginTransaction()
            .replace(R.id.registrationFlowStepContainer, fragment)
            .commit();
    }

    private void removeFragment() {
        getSupportFragmentManager()
            .beginTransaction()
            .remove(currentStep)
            .commit();
    }
}
