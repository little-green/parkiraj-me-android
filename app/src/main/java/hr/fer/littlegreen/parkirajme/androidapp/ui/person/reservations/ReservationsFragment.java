package hr.fer.littlegreen.parkirajme.androidapp.ui.person.reservations;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import dagger.hilt.android.AndroidEntryPoint;
import hr.fer.littlegreen.parkirajme.androidapp.R;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.FragmentReservationsBinding;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.ReservationAndParkingObjectPair;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseFragment;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.reservationdetails.ReservationDetailsActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.reservations.adapters.ReservationAdapter;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.reservations.events.ErrorEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.reservations.events.ReservationSelectedEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.reservations.events.ReservationsEvent;

import static android.app.Activity.RESULT_OK;

@AndroidEntryPoint
public final class ReservationsFragment extends BaseFragment {

    private static int RC_RESERVATION_DETAILS = 1;

    private ReservationsViewModel viewModel;
    private FragmentReservationsBinding binding;
    private ReservationAdapter reservationAdapter;

    @NonNull
    private final Observer<ReservationsEvent> eventObserver = event -> {
        if (event instanceof ReservationSelectedEvent) {
            startActivityForResult(ReservationDetailsActivity.newIntent(
                requireContext(),
                ((ReservationSelectedEvent) event).getSelectedReservation()
            ), RC_RESERVATION_DETAILS);
        } else if (event instanceof ErrorEvent) {
            String message = ((ErrorEvent) event).getMessage();
            if (message == null) {
                message = getString(R.string.generic_error);
            }
            Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show();
            binding.reservationsRefreshLayout.setRefreshing(false);
        }
    };

    @NonNull
    private final Observer<List<ReservationAndParkingObjectPair>> reservationsObserver = reservations -> {
        reservationAdapter.submitList(reservations);
        binding.reservationsRefreshLayout.setRefreshing(false);
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentReservationsBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(this).get(ReservationsViewModel.class);
        viewModel.getReservations().observe(getViewLifecycleOwner(), reservationsObserver);
        viewModel.getEvents().observe(getViewLifecycleOwner(), eventObserver);
        viewModel.initialize();
        reservationAdapter = new ReservationAdapter(viewModel::onReservationSelected);
        binding.reservationsList.setLayoutManager(new LinearLayoutManager(requireContext()));
        binding.reservationsList.setAdapter(reservationAdapter);
        binding.reservationsRefreshLayout.setOnRefreshListener(() -> viewModel.initialize());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_RESERVATION_DETAILS) {
            if (resultCode == RESULT_OK) {
                viewModel.initialize();
            }
        }
    }
}
