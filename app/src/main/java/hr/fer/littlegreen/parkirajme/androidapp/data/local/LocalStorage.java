package hr.fer.littlegreen.parkirajme.androidapp.data.local;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public interface LocalStorage {

    void putSession(@NonNull Session session);

    void clearSession();

    @Nullable
    Session getSession();
}
