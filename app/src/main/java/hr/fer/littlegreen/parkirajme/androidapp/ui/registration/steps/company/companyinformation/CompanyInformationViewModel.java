package hr.fer.littlegreen.parkirajme.androidapp.ui.registration.steps.company.companyinformation;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.LiveData;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseViewModel;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state.CompanyFormState;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state.RegistrationFlowState;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state.RegistrationStepSequenceManager;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.StringUtil;

public final class CompanyInformationViewModel extends BaseViewModel<BaseEvent> {

    @NonNull
    private final RegistrationStepSequenceManager registrationStepSequenceManager;

    @NonNull
    private final RegistrationFlowState registrationFlowState;

    @ViewModelInject
    public CompanyInformationViewModel(
        @NonNull RegistrationStepSequenceManager registrationStepSequenceManager,
        @NonNull RegistrationFlowState registrationFlowState
    ) {
        this.registrationStepSequenceManager = registrationStepSequenceManager;
        this.registrationFlowState = registrationFlowState;
    }

    public void onOibModified(@Nullable String oib) {
        CompanyFormState companyFormState = getOrCreateCompanyFormState();
        registrationFlowState.companyFormState.setValue(new CompanyFormState(
            oib,
            companyFormState.getName(),
            companyFormState.getAddress()
        ));
    }

    public void onNameModified(@Nullable String name) {
        CompanyFormState companyFormState = getOrCreateCompanyFormState();
        registrationFlowState.companyFormState.setValue(new CompanyFormState(
            companyFormState.getOib(),
            name,
            companyFormState.getAddress()
        ));
    }

    public void onAddressModified(@Nullable String address) {
        CompanyFormState companyFormState = getOrCreateCompanyFormState();
        registrationFlowState.companyFormState.setValue(new CompanyFormState(
            companyFormState.getOib(),
            companyFormState.getName(),
            address
        ));
    }

    public void onContinueClicked() {
        if (isValid(registrationFlowState.companyFormState.getValue())) {
            registrationStepSequenceManager.iterateForward();
        }
    }

    @NonNull
    public LiveData<CompanyFormState> getCompanyFormState() {
        return registrationFlowState.companyFormState;
    }

    @NonNull
    private CompanyFormState getOrCreateCompanyFormState() {
        CompanyFormState companyFormState = registrationFlowState.companyFormState.getValue();
        if (companyFormState != null) {
            return companyFormState;
        } else {
            return new CompanyFormState(null, null, null);
        }
    }

    private boolean isValid(@Nullable CompanyFormState companyFormState) {
        return companyFormState != null && StringUtil.areNotNullOrEmpty(
            companyFormState.getOib(),
            companyFormState.getName(),
            companyFormState.getAddress()
        );
    }
}
