package hr.fer.littlegreen.parkirajme.androidapp.ui.registration.steps.logininformation.events;

import java.util.Objects;

import androidx.annotation.NonNull;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Person;

public final class PersonRegistrationSuccessEvent implements LoginInformationStepEvent {

    @NonNull
    private final Person person;

    public PersonRegistrationSuccessEvent(@NonNull Person person) {
        this.person = person;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PersonRegistrationSuccessEvent that = (PersonRegistrationSuccessEvent) o;
        return person.equals(that.person);
    }

    @Override
    public int hashCode() {
        return Objects.hash(person);
    }

    @NonNull
    public Person getPerson() {
        return person;
    }
}
