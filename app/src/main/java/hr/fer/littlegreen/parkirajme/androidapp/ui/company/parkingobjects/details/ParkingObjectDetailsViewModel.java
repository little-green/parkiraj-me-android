package hr.fer.littlegreen.parkirajme.androidapp.ui.company.parkingobjects.details;

import org.jetbrains.annotations.NotNull;

import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.MutableLiveData;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.ParkirajMeService;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.models.parkingobject.ParkingObjectEditRequest;
import hr.fer.littlegreen.parkirajme.androidapp.data.local.LocalStorage;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.ParkingObject;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseViewModel;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.parkingobjects.events.EditFailedEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.parkingobjects.events.FailedDeleteParkingObjectEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.parkingobjects.events.FinishDeleteParkingObjectEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.parkingobjects.events.ParkingObjectEvent;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class ParkingObjectDetailsViewModel extends BaseViewModel<ParkingObjectEvent> {

    @NonNull
    private final ParkirajMeService parkirajMeService;

    @NotNull
    private final LocalStorage localStorage;

    @NonNull
    private final MutableLiveData<ParkingObject> parkingObject = new MutableLiveData<>();

    @ViewModelInject
    public ParkingObjectDetailsViewModel(
        @NonNull ParkirajMeService parkirajMeService,
        @NotNull LocalStorage localStorage
    ) {
        this.parkirajMeService = parkirajMeService;
        this.localStorage = localStorage;
    }

    public void initialize(ParkingObject parkingObject) {
        this.parkingObject.postValue(parkingObject);
    }

    public void deleteParkingObject() {
        disposable.add(
            parkirajMeService.deleteParkingObject(parkingObject.getValue().getObjectUuid(), localStorage.getSession().getToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    emitEvent(new FinishDeleteParkingObjectEvent());
                }, throwable -> {
                    emitEvent(new FailedDeleteParkingObjectEvent());
                })
        );
    }

    public void editCapacity(int capacity) {
        int freeSlots = (int) (Math.random() * capacity);
        disposable.add(
            parkirajMeService.editParkingObject(parkingObject.getValue().getObjectUuid(), new ParkingObjectEditRequest(capacity,
                    parkingObject.getValue().getThirtyMinutePrice(), freeSlots
                ),
                localStorage.getSession().getToken()
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    () -> {
                        ParkingObject current = getParkingObject().getValue();
                        getParkingObject().setValue(new ParkingObject(
                            current.getObjectUuid(),
                            current.getCompanyUuid(),
                            freeSlots,
                            current.getThirtyMinutePrice(),
                            current.getAddress(),
                            current.getName(),
                            capacity,
                            current.getLatitude(),
                            current.getLongitude()
                        ));
                    },
                    throwable -> emitEvent(new EditFailedEvent())
                )
        );
    }

    public void editThirtyMinutePrice(int thirtyMinutePrice) {
        disposable.add(
            parkirajMeService.editParkingObject(
                parkingObject.getValue().getObjectUuid(),
                new ParkingObjectEditRequest(parkingObject.getValue().getCapacity(),
                    thirtyMinutePrice, parkingObject.getValue().getFreeSlots()
                ),
                localStorage.getSession().getToken()
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    () -> {
                        ParkingObject current = getParkingObject().getValue();
                        getParkingObject().setValue(new ParkingObject(
                            current.getObjectUuid(),
                            current.getCompanyUuid(),
                            current.getFreeSlots(),
                            thirtyMinutePrice,
                            current.getAddress(),
                            current.getName(),
                            current.getCapacity(),
                            current.getLatitude(),
                            current.getLongitude()
                        ));
                    },
                    throwable -> emitEvent(new EditFailedEvent())
                )
        );
    }

    @NonNull
    public MutableLiveData<ParkingObject> getParkingObject() {
        return parkingObject;
    }
}
