package hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.main;

import org.jetbrains.annotations.NonNls;

import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Administrator;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Company;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseViewModel;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.main.state.AdministratorScreenType;

public class AdministratorMainViewModel extends BaseViewModel<BaseEvent> {

    private final MutableLiveData<AdministratorScreenType> administratorScreenType =
        new MutableLiveData<>(AdministratorScreenType.ADMINDETAILS);

    private Administrator administrator;

    public void initialize(Administrator administrator) {
        this.administrator=administrator;
    }
    @ViewModelInject
    public AdministratorMainViewModel(){

    }

    @NonNull
    public LiveData<AdministratorScreenType> getClientScreenType() {
        return administratorScreenType;
    }

    public void onAdministratorScreenSelected(AdministratorScreenType administratorScreenType) {
        this.administratorScreenType.setValue(administratorScreenType);
    }
}
