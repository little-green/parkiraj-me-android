package hr.fer.littlegreen.parkirajme.androidapp.ui.registration.steps.person.personalinformation;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import dagger.hilt.android.AndroidEntryPoint;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.FragmentPersonalInformationStepBinding;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseFragment;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state.PersonPersonalInformationFormState;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.TextViewUtil;

@AndroidEntryPoint
public final class PersonalInformationStepFragment extends BaseFragment {

    private PersonalInformationViewModel viewModel;
    private FragmentPersonalInformationStepBinding binding;

    @NonNull
    private final Observer<PersonPersonalInformationFormState> personPersonalInformationFormStateObserver =
        personPersonalInformationFormState -> {
            if (personPersonalInformationFormState == null) {
                return;
            }
            TextViewUtil.update(binding.personalOib, personPersonalInformationFormState.getOib());
            TextViewUtil.update(binding.personalName, personPersonalInformationFormState.getName());
            TextViewUtil.update(binding.personalSurname, personPersonalInformationFormState.getSurname());
        };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentPersonalInformationStepBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(this).get(PersonalInformationViewModel.class);
        viewModel.getPersonPersonalInformationFormState().observe(getViewLifecycleOwner(), personPersonalInformationFormStateObserver);
        TextViewUtil.addTextChangedListener(binding.personalOib, viewModel::onOibChanged);
        TextViewUtil.addTextChangedListener(binding.personalName, viewModel::onNameChanged);
        TextViewUtil.addTextChangedListener(binding.personalSurname, viewModel::onSurnameChanged);
        binding.continueButton.setOnClickListener(v -> viewModel.onContinueClicked());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
