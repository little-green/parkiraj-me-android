package hr.fer.littlegreen.parkirajme.androidapp.ui;

import android.app.Application;

import dagger.hilt.android.HiltAndroidApp;
import hr.fer.littlegreen.parkirajme.androidapp.BuildConfig;
import timber.log.Timber;

@HiltAndroidApp
public final class ParkirajMeApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }
}
