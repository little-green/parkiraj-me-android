package hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.companyuserinformation;

import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.ParkirajMeService;
import hr.fer.littlegreen.parkirajme.androidapp.data.local.LocalStorage;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Company;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseViewModel;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.companyuserinformation.events.CompanyAccountDeletedEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.companyuserinformation.events.CompanyUserInformationEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.information.state.CompanyInformationDialogType;
import androidx.annotation.NonNull;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class CompanyUserInformationViewModel extends BaseViewModel<CompanyUserInformationEvent> {

    @NonNull
    private final ParkirajMeService parkirajMeService;

    @NonNull
    private final LocalStorage localStorage;

    @NonNull
    private final MutableLiveData<Company> company = new MutableLiveData<>();

    @NonNull
    private final MutableLiveData<CompanyInformationDialogType> dialogType = new MutableLiveData<>();


    @ViewModelInject
    public CompanyUserInformationViewModel(
        @NonNull ParkirajMeService parkirajMeService,
        @NonNull LocalStorage localStorage
    ){
        this.parkirajMeService = parkirajMeService;
        this.localStorage = localStorage;
    }

    public void initialize(Company company) {this.company.setValue(company);}

    public void onDeleteAccountClicked(){dialogType.setValue(CompanyInformationDialogType.DELETE_ACCOUNT_CONFIRMATION);}

    public void dismissDialog(){ dialogType.setValue(null);}

    public void deleteAccount(){
        dialogType.setValue(null);
        disposable.add(
            parkirajMeService.deleteAccount(company.getValue().getUserUuid())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    emitEvent(new CompanyAccountDeletedEvent());
                }, throwable -> dialogType.setValue(CompanyInformationDialogType.ERROR))
        );
    }

    @NonNull
    public LiveData<Company> getCompany() {
        return company;
    }

    @NonNull
    public LiveData<CompanyInformationDialogType> getDialogType() {
        return dialogType;
    }

}
