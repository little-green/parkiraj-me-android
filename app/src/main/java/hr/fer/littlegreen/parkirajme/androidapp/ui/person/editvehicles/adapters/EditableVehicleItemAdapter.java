package hr.fer.littlegreen.parkirajme.androidapp.ui.person.editvehicles.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.ItemEditableVehicleBinding;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.editvehicles.state.EditableVehicleItem;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.TextViewUtil;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.ViewUtil;

public final class EditableVehicleItemAdapter extends ListAdapter<EditableVehicleItem, EditableVehicleItemAdapter.ViewHolder> {

    @NonNull
    private final DeleteVehicleListener listener;

    public EditableVehicleItemAdapter(@NonNull DeleteVehicleListener listener) {
        super(new DiffUtil.ItemCallback<EditableVehicleItem>() {

            @Override
            public boolean areItemsTheSame(@NonNull EditableVehicleItem oldItem, @NonNull EditableVehicleItem newItem) {
                return oldItem.getVehicle().equals(newItem.getVehicle());
            }

            @Override
            public boolean areContentsTheSame(@NonNull EditableVehicleItem oldItem, @NonNull EditableVehicleItem newItem) {
                return oldItem.equals(newItem);
            }
        });
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(ItemEditableVehicleBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(getItem(position));
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @NonNull
        private final ItemEditableVehicleBinding binding;

        public ViewHolder(@NonNull ItemEditableVehicleBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(@NonNull EditableVehicleItem editableVehicleItem) {
            ViewUtil.updateVisibility(binding.editableVehicleDataContainer, !editableVehicleItem.isActionInProgress());
            ViewUtil.updateVisibility(binding.editableVehicleProgressBar, editableVehicleItem.isActionInProgress());
            TextViewUtil.update(binding.editableVehicleRegistrationNumber, editableVehicleItem.getVehicle().getRegistrationNumber());
            binding.editableVehicleDeleteButton.setOnClickListener(v -> listener.deleteVehicle(editableVehicleItem));
        }
    }
}
