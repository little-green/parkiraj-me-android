package hr.fer.littlegreen.parkirajme.androidapp.ui.registration.steps.person.vehicleinformation;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.LiveData;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseViewModel;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state.RegistrationFlowState;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state.RegistrationStepSequenceManager;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state.VehicleInformationFormState;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.StringUtil;

public final class VehicleInformationStepViewModel extends BaseViewModel<BaseEvent> {

    @NonNull
    private final RegistrationStepSequenceManager registrationStepSequenceManager;

    @NonNull
    private final RegistrationFlowState registrationFlowState;

    @ViewModelInject
    public VehicleInformationStepViewModel(
        @NonNull RegistrationStepSequenceManager registrationStepSequenceManager,
        @NonNull RegistrationFlowState registrationFlowState
    ) {
        this.registrationStepSequenceManager = registrationStepSequenceManager;
        this.registrationFlowState = registrationFlowState;
    }

    public void onRegistrationPlateChanged(String registrationPlate) {
        registrationFlowState.vehicleInformationFormState.setValue(new VehicleInformationFormState(registrationPlate));
    }

    public void onContinueClicked() {
        if (isValid(registrationFlowState.vehicleInformationFormState.getValue())) {
            registrationStepSequenceManager.iterateForward();
        }
    }

    @NonNull
    public LiveData<VehicleInformationFormState> getVehicleInformationFormState() {
        return registrationFlowState.vehicleInformationFormState;
    }

    private boolean isValid(@Nullable VehicleInformationFormState vehicleInformationFormState) {
        return vehicleInformationFormState != null && StringUtil.isNotNullOrEmpty(vehicleInformationFormState.getRegistrationPlate());
    }
}
