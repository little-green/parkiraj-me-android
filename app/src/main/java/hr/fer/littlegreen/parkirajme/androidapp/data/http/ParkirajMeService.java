package hr.fer.littlegreen.parkirajme.androidapp.data.http;

import hr.fer.littlegreen.parkirajme.androidapp.data.http.models.company.CompanyEditRequest;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.models.createreservation.CreateReservationRequest;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.models.createreservation.CreateReservationResponse;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.models.editperson.EditPersonRequest;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.models.login.LoginRequest;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.models.login.LoginResponse;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.models.parkingobject.ParkingObjectEditRequest;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.models.parkingobject.ParkingObjectRequest;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.models.parkingobject.ParkingObjectResponse;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.models.parkingobjects.AllParkingObjectsResponse;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.models.parkingobjects.ParkingObjectsResponse;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.models.registration.CompanyRegistrationRequest;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.models.registration.PersonRegistrationRequest;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.models.registration.UserRegistrationResponse;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.models.reservations.ReservationsResponse;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.models.users.RegisteredUsersResponse;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Single;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ParkirajMeService {

    @POST("/login")
    Single<LoginResponse> login(@Body LoginRequest request);

    @POST("/register/person")
    Single<UserRegistrationResponse> registerPerson(@Body PersonRegistrationRequest request);

    @POST("/register/company")
    Single<UserRegistrationResponse> registerCompany(@Body CompanyRegistrationRequest request);

    @POST("/logout")
    Completable logout();

    @DELETE("/user/{userId}")
    Completable deleteAccount(@Path("userId") String userId);

    @GET("/user/{userId}/reservations")
    Single<ReservationsResponse> getReservations(@Path("userId") String userId);

    @GET("/company/{companyId}/parkingObjects")
    Single<ParkingObjectsResponse> getParkingObjects(@Path("companyId") String companyId, @Header("Authentication-Token") String token);

    @GET("/parkingObjects/{objectUuid}")
    Single<ParkingObjectResponse> getParkingObject(@Path("objectUuid") String objectUuid);

    @GET("/closestParkingObject")
    Single<ParkingObjectResponse> getParkingObject(@Query("lat") double latitude, @Query("long") double longitude);

    @DELETE("/parkingObject/{parkingObjectId}")
    Completable deleteParkingObject(@Path("parkingObjectId") String parkingObjectId, @Header("Authentication-Token") String token);

    @POST("/company/addParkingObject")
    Completable addParkingObject(@Body ParkingObjectRequest request, @Header("Authentication-Token") String token);

    @DELETE("/reservation/{reservationId}")
    Completable cancelReservation(@Path("reservationId") String reservationId);

    @POST("/user/addReservation")
    Single<CreateReservationResponse> createReservation(@Body CreateReservationRequest createReservationRequest);

    @GET("/registeredUsers")
    Single<RegisteredUsersResponse> getRegisteredUsers();

    @PATCH("/parkingObject/{parkingObjectId}")
    Completable editParkingObject(
        @Path("parkingObjectId") String parkingObjectId,
        @Body ParkingObjectEditRequest request, @Header("Authentication-Token") String token
    );

    @PATCH("/company/{companyId}")
    Completable editCompany(
        @Path("companyId") String companyId, @Body CompanyEditRequest request,
        @Header("Authentication-Token") String token
    );

    @PATCH("/person/{personUuid}")
    Completable editPerson(@Path("personUuid") String personUuid, @Body EditPersonRequest request);

    @DELETE("/user/{userUuid}/vehicles/{registrationNumber}")
    Completable deleteVehicle(@Path("userUuid") String userUuid, @Path("registrationNumber") String registrationNumber);

    @POST("/user/{userUuid}/vehicles/{registrationNumber}")
    Completable addVehicle(@Path("userUuid") String userUuid, @Path("registrationNumber") String registrationNumber);

    @GET("/parkingObjects")
    Single<AllParkingObjectsResponse> getAllParkingObjects();
}
