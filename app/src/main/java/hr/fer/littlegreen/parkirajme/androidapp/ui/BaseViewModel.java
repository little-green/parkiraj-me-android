package hr.fer.littlegreen.parkirajme.androidapp.ui;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.SingleLiveEvent;
import io.reactivex.rxjava3.disposables.CompositeDisposable;

public abstract class BaseViewModel<E extends BaseEvent> extends ViewModel {

    @NonNull
    protected final CompositeDisposable disposable = new CompositeDisposable();

    @NonNull
    private final SingleLiveEvent<E> events = new SingleLiveEvent<>();

    @Override
    protected void onCleared() {
        super.onCleared();
        disposable.clear();
    }

    protected void emitEvent(E event) {
        events.setValue(event);
    }

    @NonNull
    public LiveData<E> getEvents() {
        return events;
    }
}
