package hr.fer.littlegreen.parkirajme.androidapp.ui.company.information.state;

public enum CompanyInformationDialogType {

    LOGOUT_CONFIRMATION,
    DELETE_ACCOUNT_CONFIRMATION,
    ERROR
}
