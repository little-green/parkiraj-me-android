package hr.fer.littlegreen.parkirajme.androidapp.ui.person.editvehicles.state;

import java.util.Objects;

import androidx.annotation.NonNull;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Vehicle;

public final class EditableVehicleItem {

    @NonNull
    private final Vehicle vehicle;

    private final boolean isActionInProgress;

    public EditableVehicleItem(@NonNull Vehicle vehicle, boolean isActionInProgress) {
        this.vehicle = vehicle;
        this.isActionInProgress = isActionInProgress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EditableVehicleItem that = (EditableVehicleItem) o;
        return isActionInProgress == that.isActionInProgress &&
            vehicle.equals(that.vehicle);
    }

    @Override
    public int hashCode() {
        return Objects.hash(vehicle, isActionInProgress);
    }

    @NonNull
    public Vehicle getVehicle() {
        return vehicle;
    }

    public boolean isActionInProgress() {
        return isActionInProgress;
    }
}
