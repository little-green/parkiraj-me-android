package hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.adminuserinformation;

import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.ParkirajMeService;
import hr.fer.littlegreen.parkirajme.androidapp.data.local.LocalStorage;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Administrator;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseViewModel;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.adminuserinformation.events.AdministratorUserInformationEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.SingleLiveEvent;
import io.reactivex.rxjava3.annotations.NonNull;

public class AdministratorUserInformationViewModel extends BaseViewModel<AdministratorUserInformationEvent> {

    @NonNull
    private final ParkirajMeService parkirajMeService;

    @NonNull
    private final LocalStorage localStorage;

    @NonNull
    private final MutableLiveData<Administrator> administrator = new MutableLiveData<>();


    @ViewModelInject
    public AdministratorUserInformationViewModel(
        @NonNull ParkirajMeService parkirajMeService,
        @NonNull LocalStorage localStorage
    ) {
        this.parkirajMeService = parkirajMeService;
        this.localStorage = localStorage;
    }

    public SingleLiveEvent<Object> getDialogType;

    public LiveData<Administrator> getAdministrator() { return administrator; }

    public void initialize(Administrator administrator) {
        this.administrator.setValue(administrator);
    }
}
