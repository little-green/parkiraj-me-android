package hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.steps.usertype.state.UserType;

public final class RegistrationFlowState {

    @NonNull
    public final MutableLiveData<UserType> userType = new MutableLiveData<>();

    @NonNull
    public final MutableLiveData<CompanyFormState> companyFormState = new MutableLiveData<>();

    @NonNull
    public final MutableLiveData<LoginInformationFormState> loginInformationFormState = new MutableLiveData<>();

    @NonNull
    public final MutableLiveData<PersonPersonalInformationFormState> personPersonalInformationFormState = new MutableLiveData<>();

    @NonNull
    public final MutableLiveData<VehicleInformationFormState> vehicleInformationFormState = new MutableLiveData<>();

    @NonNull
    public final MutableLiveData<CardInformationFormState> cardInformationFormState = new MutableLiveData<>();
}
