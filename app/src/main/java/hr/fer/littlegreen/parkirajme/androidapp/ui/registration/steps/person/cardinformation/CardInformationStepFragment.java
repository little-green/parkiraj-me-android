package hr.fer.littlegreen.parkirajme.androidapp.ui.registration.steps.person.cardinformation;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import dagger.hilt.android.AndroidEntryPoint;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.FragmentCardInformationBinding;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseFragment;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state.CardInformationFormState;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.TextViewUtil;

@AndroidEntryPoint
public final class CardInformationStepFragment extends BaseFragment {

    private CardInformationStepViewModel viewModel;
    private FragmentCardInformationBinding binding;

    @NonNull
    private final Observer<CardInformationFormState> cardInformationFormStateObserver = cardInformationFormState -> {
        if (cardInformationFormState == null) {
            return;
        }
        TextViewUtil.update(binding.cardNumberField, cardInformationFormState.getCardNumber());
        TextViewUtil.update(binding.expiryDateField, cardInformationFormState.getExpiryDate());
        TextViewUtil.update(binding.cvvField, cardInformationFormState.getCvv());
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentCardInformationBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(this).get(CardInformationStepViewModel.class);
        viewModel.getCardInformationFormState().observe(getViewLifecycleOwner(), cardInformationFormStateObserver);
        TextViewUtil.addTextChangedListener(binding.cardNumberField, viewModel::onCardNumberChanged);
        TextViewUtil.addTextChangedListener(binding.expiryDateField, viewModel::onExpiryDateChanged);
        TextViewUtil.addTextChangedListener(binding.cvvField, viewModel::onCvvChanged);
        binding.continueButton.setOnClickListener(v -> viewModel.onContinueClicked());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
