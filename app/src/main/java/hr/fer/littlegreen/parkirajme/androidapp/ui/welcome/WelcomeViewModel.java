package hr.fer.littlegreen.parkirajme.androidapp.ui.welcome;

import androidx.hilt.lifecycle.ViewModelInject;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseViewModel;
import hr.fer.littlegreen.parkirajme.androidapp.ui.welcome.events.NavigateToLoginEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.welcome.events.NavigateToRegistrationEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.welcome.events.NavigateToUnregisteredUserScreenEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.welcome.events.WelcomeEvent;

public final class WelcomeViewModel extends BaseViewModel<WelcomeEvent> {

    @ViewModelInject
    public WelcomeViewModel() {
    }

    public void onLoginClicked() {
        emitEvent(new NavigateToLoginEvent());
    }

    public void onRegisterClicked() {
        emitEvent(new NavigateToRegistrationEvent());
    }

    public void onContinueWithoutLoginClicked() {
        emitEvent(new NavigateToUnregisteredUserScreenEvent());
    }
}
