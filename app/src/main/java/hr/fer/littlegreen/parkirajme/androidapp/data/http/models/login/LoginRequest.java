package hr.fer.littlegreen.parkirajme.androidapp.data.http.models.login;

import java.util.Objects;

import androidx.annotation.NonNull;

public final class LoginRequest {

    @NonNull
    private final String email;

    @NonNull
    private final String password;

    public LoginRequest(@NonNull String email, @NonNull String password) {
        this.email = email;
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LoginRequest that = (LoginRequest) o;
        return email.equals(that.email) &&
            password.equals(that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, password);
    }

    @NonNull
    public String getEmail() {
        return email;
    }

    @NonNull
    public String getPassword() {
        return password;
    }
}
