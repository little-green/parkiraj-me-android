package hr.fer.littlegreen.parkirajme.androidapp.ui.person.createreservation.events;

import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseEvent;

public interface CreateReservationEvent extends BaseEvent {
}
