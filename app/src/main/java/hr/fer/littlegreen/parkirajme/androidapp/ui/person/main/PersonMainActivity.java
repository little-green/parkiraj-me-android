package hr.fer.littlegreen.parkirajme.androidapp.ui.person.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import dagger.hilt.android.AndroidEntryPoint;
import hr.fer.littlegreen.parkirajme.androidapp.R;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.ActivityPersonMainBinding;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Person;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseFragment;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.details.PersonDetailsFragment;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.main.state.PersonScreenType;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.map.MapFragment;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.reservations.ReservationsFragment;

import static hr.fer.littlegreen.parkirajme.androidapp.ui.person.main.state.PersonScreenType.DETAILS;
import static hr.fer.littlegreen.parkirajme.androidapp.ui.person.main.state.PersonScreenType.MAP;
import static hr.fer.littlegreen.parkirajme.androidapp.ui.person.main.state.PersonScreenType.RESERVATIONS;

@AndroidEntryPoint
public final class PersonMainActivity extends BaseActivity {

    @NonNull
    private static final String EXTRA_PERSON = "EXTRA_PERSON";

    @NonNull
    private final Observer<PersonScreenType> personScreenTypeObserver = personScreenType -> {
        switch (personScreenType) {
            case MAP:
                setFragment(new MapFragment());
                break;
            case RESERVATIONS:
                setFragment(new ReservationsFragment());
                break;
            case DETAILS:
                setFragment(new PersonDetailsFragment());
                break;
        }
    };
    private PersonMainViewModel viewModel;
    private ActivityPersonMainBinding binding;

    @NonNull
    public static Intent newIntent(@NonNull Context context, @NonNull Person person) {
        return new Intent(context, PersonMainActivity.class)
            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)
            .putExtra(EXTRA_PERSON, person);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Person person = (Person) getIntent().getSerializableExtra(EXTRA_PERSON);
        viewModel = new ViewModelProvider(this).get(PersonMainViewModel.class);
        viewModel.initialize(person);
        viewModel.getPersonScreenType().observe(this, personScreenTypeObserver);
        binding = ActivityPersonMainBinding.inflate(getLayoutInflater());
        binding.personScreenNavigation.setOnNavigationItemSelectedListener(item -> {
            int id = item.getItemId();
            if (id == R.id.personScreenMap) {
                viewModel.onPersonScreenSelected(MAP);
                return true;
            } else if (id == R.id.personScreenReservations) {
                viewModel.onPersonScreenSelected(RESERVATIONS);
                return true;
            } else if (id == R.id.personScreenDetails) {
                viewModel.onPersonScreenSelected(DETAILS);
                return true;
            } else {
                return false;
            }
        });
        setContentView(binding.getRoot());
    }

    private void setFragment(BaseFragment fragment) {
        getSupportFragmentManager()
            .beginTransaction()
            .replace(R.id.personScreenContainer, fragment)
            .commit();
    }
}
