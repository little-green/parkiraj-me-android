package hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.users.events;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;


import hr.fer.littlegreen.parkirajme.androidapp.domain.models.User;


public class UserSelectedEvent implements UserEvent{

    @NotNull
    private final User user;

    public UserSelectedEvent(@NotNull User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserSelectedEvent that = (UserSelectedEvent) o;
        return user.equals(that.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user);
    }

    public User getUser() {
        return user;
    }
}
