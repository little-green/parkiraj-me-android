package hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state;

import java.util.Objects;

import androidx.annotation.Nullable;

public final class VehicleInformationFormState {

    @Nullable
    private final String registrationPlate;

    public VehicleInformationFormState(@Nullable String registrationPlate) {
        this.registrationPlate = registrationPlate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        VehicleInformationFormState that = (VehicleInformationFormState) o;
        return Objects.equals(registrationPlate, that.registrationPlate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(registrationPlate);
    }

    @Nullable
    public String getRegistrationPlate() {
        return registrationPlate;
    }
}
