package hr.fer.littlegreen.parkirajme.androidapp.domain.models;

import androidx.annotation.NonNull;

public final class Administrator extends User {

    public Administrator(
        @NonNull String userUuid,
        @NonNull String email,
        @NonNull String oib
    ) {
        super(userUuid, email, oib);
    }
}
