package hr.fer.littlegreen.parkirajme.androidapp.ui.utilities;

import android.view.View;

import androidx.annotation.NonNull;

public final class ViewUtil {

    public static void updateVisibility(@NonNull View view, boolean isVisible) {
        if (isVisible) {
            view.setVisibility(View.VISIBLE);
        } else {
            view.setVisibility(View.GONE);
        }
    }
}
