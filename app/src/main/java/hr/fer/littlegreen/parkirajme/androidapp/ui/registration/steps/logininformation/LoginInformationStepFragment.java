package hr.fer.littlegreen.parkirajme.androidapp.ui.registration.steps.logininformation;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import dagger.hilt.android.AndroidEntryPoint;
import hr.fer.littlegreen.parkirajme.androidapp.R;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.FragmentLoginInformationStepBinding;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseFragment;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.main.CompanyMainActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.main.PersonMainActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state.LoginInformationFormState;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.steps.logininformation.events.CompanyRegistrationSuccessEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.steps.logininformation.events.LoginInformationStepEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.steps.logininformation.events.PersonRegistrationSuccessEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.steps.logininformation.events.RegistrationFailedEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.TextViewUtil;

@AndroidEntryPoint
public final class LoginInformationStepFragment extends BaseFragment {

    private LoginInformationViewModel viewModel;
    private FragmentLoginInformationStepBinding binding;

    @NonNull
    private final Observer<LoginInformationFormState> loginInformationFormStateObserver = loginInformationFormState -> {
        if (loginInformationFormState == null) {
            return;
        }
        TextViewUtil.update(binding.email, loginInformationFormState.getEmail());
        TextViewUtil.update(binding.password, loginInformationFormState.getPassword());
    };

    @NonNull
    private final Observer<LoginInformationStepEvent> eventObserver = event -> {
        if (event instanceof PersonRegistrationSuccessEvent) {
            startActivity(PersonMainActivity.newIntent(requireContext(), ((PersonRegistrationSuccessEvent) event).getPerson()));
            requireActivity().finish();
        } else if (event instanceof CompanyRegistrationSuccessEvent) {
            startActivity(CompanyMainActivity.newIntent(requireContext(), ((CompanyRegistrationSuccessEvent) event).getCompany()));
            requireActivity().finish();
        } else if (event instanceof RegistrationFailedEvent) {
            String message = ((RegistrationFailedEvent) event).getMessage();
            if (message == null) {
                message = getString(R.string.registration_failed);
            }
            Toast.makeText(requireContext(), message, Toast.LENGTH_LONG).show();
        }
    };

    @NonNull
    private final Observer<Boolean> isProgressShownObserver = isProgressShown -> {
        if (isProgressShown) {
            binding.loading.setVisibility(View.VISIBLE);
        } else {
            binding.loading.setVisibility(View.GONE);
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentLoginInformationStepBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(this).get(LoginInformationViewModel.class);
        viewModel.getLoginInformationFormState().observe(getViewLifecycleOwner(), loginInformationFormStateObserver);
        viewModel.isProgressShown().observe(getViewLifecycleOwner(), isProgressShownObserver);
        viewModel.getEvents().observe(getViewLifecycleOwner(), eventObserver);
        TextViewUtil.addTextChangedListener(binding.email, viewModel::onEmailChanged);
        TextViewUtil.addTextChangedListener(binding.password, viewModel::onPasswordChanged);
        binding.registerButton.setOnClickListener(v -> {
            hideKeyboard();
            viewModel.onFinishRegistrationClicked();
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
