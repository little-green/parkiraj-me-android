package hr.fer.littlegreen.parkirajme.androidapp.data.http.models.reservations;

import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.ReservationAndParkingObjectPair;

public final class ReservationsResponse {

    @NonNull
    private final List<ReservationAndParkingObjectPair> reservations;

    public ReservationsResponse(@NonNull List<ReservationAndParkingObjectPair> reservations) {
        this.reservations = reservations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ReservationsResponse that = (ReservationsResponse) o;
        return reservations.equals(that.reservations);
    }

    @Override
    public int hashCode() {
        return Objects.hash(reservations);
    }

    @NonNull
    public List<ReservationAndParkingObjectPair> getReservations() {
        return reservations;
    }
}
