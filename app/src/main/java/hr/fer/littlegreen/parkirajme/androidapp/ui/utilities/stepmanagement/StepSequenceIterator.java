package hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.stepmanagement;

import androidx.annotation.NonNull;

public interface StepSequenceIterator {

    boolean canDereference();

    @NonNull
    StepSequence dereference() throws IllegalStateException;

    void iterateForward();

    void iterateBackward();

    double getIteratedWeight();
}
