package hr.fer.littlegreen.parkirajme.androidapp.data.http.models.registration;

import java.time.YearMonth;
import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;

public final class PersonRegistrationRequest {

    @NonNull
    private final String oib;

    @NonNull
    private final String firstName;

    @NonNull
    private final String lastName;

    @NonNull
    private final List<String> registrationNumbers;

    @NonNull
    private final String creditCardNumber;

    @NonNull
    private final YearMonth creditCardExpirationDate;

    @NonNull
    private final String email;

    @NonNull
    private final String password;

    public PersonRegistrationRequest(
        @NonNull String oib,
        @NonNull String firstName,
        @NonNull String lastName,
        @NonNull List<String> registrationNumbers,
        @NonNull String creditCardNumber,
        @NonNull YearMonth creditCardExpirationDate,
        @NonNull String email,
        @NonNull String password
    ) {
        this.oib = oib;
        this.firstName = firstName;
        this.lastName = lastName;
        this.registrationNumbers = registrationNumbers;
        this.creditCardNumber = creditCardNumber;
        this.creditCardExpirationDate = creditCardExpirationDate;
        this.email = email;
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PersonRegistrationRequest that = (PersonRegistrationRequest) o;
        return oib.equals(that.oib) &&
            firstName.equals(that.firstName) &&
            lastName.equals(that.lastName) &&
            registrationNumbers.equals(that.registrationNumbers) &&
            creditCardNumber.equals(that.creditCardNumber) &&
            creditCardExpirationDate.equals(that.creditCardExpirationDate) &&
            email.equals(that.email) &&
            password.equals(that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(oib, firstName, lastName, registrationNumbers, creditCardNumber, creditCardExpirationDate, email, password);
    }

    @NonNull
    public String getOib() {
        return oib;
    }

    @NonNull
    public String getFirstName() {
        return firstName;
    }

    @NonNull
    public String getLastName() {
        return lastName;
    }

    @NonNull
    public List<String> getRegistrationNumbers() {
        return registrationNumbers;
    }

    @NonNull
    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    @NonNull
    public YearMonth getCreditCardExpirationDate() {
        return creditCardExpirationDate;
    }

    @NonNull
    public String getEmail() {
        return email;
    }

    @NonNull
    public String getPassword() {
        return password;
    }
}
