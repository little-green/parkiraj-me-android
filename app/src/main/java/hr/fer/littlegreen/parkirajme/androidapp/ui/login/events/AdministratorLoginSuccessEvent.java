package hr.fer.littlegreen.parkirajme.androidapp.ui.login.events;

import java.util.Objects;

import androidx.annotation.NonNull;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Administrator;

public final class AdministratorLoginSuccessEvent implements LoginEvent {

    @NonNull
    private final Administrator administrator;

    public AdministratorLoginSuccessEvent(@NonNull Administrator administrator) {
        this.administrator = administrator;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AdministratorLoginSuccessEvent that = (AdministratorLoginSuccessEvent) o;
        return administrator.equals(that.administrator);
    }

    @Override
    public int hashCode() {
        return Objects.hash(administrator);
    }

    @NonNull
    public Administrator getAdministrator() {
        return administrator;
    }
}
