package hr.fer.littlegreen.parkirajme.androidapp.ui.person.map.dialogs;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import hr.fer.littlegreen.parkirajme.androidapp.R;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.DialogParkingObjectDetailsBinding;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.ParkingObject;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.createreservation.CreateReservationActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.TextViewUtil;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.ViewUtil;

public final class ParkingObjectDetailsDialog extends BottomSheetDialogFragment {

    private static final String ARG_PARKING_OBJECT = "ARG_PARKING_OBJECT";
    private static final String ARG_IS_LOGGED_IN = "ARG_IS_LOGGED_IN";

    @NonNull
    public static final String TAG = "ParkingObjectDetailsDialog";

    private DialogParkingObjectDetailsBinding binding;

    public ParkingObjectDetailsDialog(ParkingObject parkingObject, boolean isLoggedIn) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(ARG_PARKING_OBJECT, parkingObject);
        bundle.putBoolean(ARG_IS_LOGGED_IN, isLoggedIn);
        setArguments(bundle);
    }

    @Nullable
    @Override
    public View onCreateView(
        @NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState
    ) {
        binding = DialogParkingObjectDetailsBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ParkingObject parkingObject = (ParkingObject) getArguments().getSerializable(ARG_PARKING_OBJECT);
        boolean isLoggedIn = getArguments().getBoolean(ARG_IS_LOGGED_IN);
        TextViewUtil.update(binding.dialogParkingObjectDetailsTitle, parkingObject.getAddress());
        TextViewUtil.update(binding.dialogParkingObjectDetailsSpace, formatSpace(parkingObject));
        TextViewUtil.update(binding.dialogParkingObjectDetailsPrice, formatPrice(parkingObject));
        ViewUtil.updateVisibility(binding.dialogParkingObjectDetailsActionButton, isLoggedIn);
        binding.dialogParkingObjectDetailsCloseButton.setOnClickListener(v -> dismiss());
        binding.dialogParkingObjectDetailsActionButton.setOnClickListener(
            v -> startActivity(CreateReservationActivity.newIntent(requireContext(), parkingObject))
        );
    }

    @NonNull
    private String formatSpace(@NonNull ParkingObject parkingObject) {
        return String.format(
            getString(R.string.parking_object_details_space_format),
            parkingObject.getFreeSlots(),
            parkingObject.getCapacity()
        );
    }

    @SuppressLint("DefaultLocale")
    @NonNull
    private String formatPrice(@NonNull ParkingObject parkingObject) {
        return String.format(
            getString(R.string.parking_object_details_price_format),
            String.format(
                "%d,%02d",
                parkingObject.getThirtyMinutePrice() / 100,
                parkingObject.getThirtyMinutePrice() % 100
            )
        );
    }
}
