package hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state;

import java.util.Objects;

import androidx.annotation.Nullable;

public final class LoginInformationFormState {

    @Nullable
    private final String email;

    @Nullable
    private final String password;

    public LoginInformationFormState(@Nullable String email, @Nullable String password) {
        this.email = email;
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LoginInformationFormState that = (LoginInformationFormState) o;
        return Objects.equals(email, that.email) &&
            Objects.equals(password, that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, password);
    }

    @Nullable
    public String getEmail() {
        return email;
    }

    @Nullable
    public String getPassword() {
        return password;
    }
}
