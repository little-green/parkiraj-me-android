package hr.fer.littlegreen.parkirajme.androidapp.data;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Administrator;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Company;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Person;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.User;

public final class UserConverter implements JsonDeserializer<User> {

    @NonNull
    private static final String MEMBER_NAME_ROLE = "role";

    @NonNull
    private static final String ROLE_PERSON = "p";

    @NonNull
    private static final String ROLE_COMPANY = "c";

    @NonNull
    private static final String ROLE_ADMINISTRATOR = "a";

    @Inject
    public UserConverter() {
    }

    @Override
    public User deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        switch (json.getAsJsonObject().get(MEMBER_NAME_ROLE).getAsString()) {
            case ROLE_PERSON:
                return context.deserialize(json, Person.class);
            case ROLE_COMPANY:
                return context.deserialize(json, Company.class);
            case ROLE_ADMINISTRATOR:
                return context.deserialize(json, Administrator.class);
            default:
                return null;
        }
    }
}
