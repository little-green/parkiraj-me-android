package hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.users;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.MutableLiveData;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.ParkirajMeService;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.models.login.LoginResponse;
import hr.fer.littlegreen.parkirajme.androidapp.data.local.LocalStorage;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Company;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Person;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.User;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseViewModel;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.main.AdministratorMainActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.users.events.UserEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.users.events.UserSelectedEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.users.events.FailedLoadUsersEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.automaticlogin.AutomaticLoginActivity;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class UsersViewModel extends BaseViewModel<UserEvent> {

    @NonNull
    private final ParkirajMeService parkirajMeService;

    @NonNull
    private final LocalStorage localStorage;

    @NonNull
    private final MutableLiveData<List<User>> user = new MutableLiveData<>();



    @ViewModelInject
    public UsersViewModel(
        @NonNull LocalStorage localStorage,
        @NonNull ParkirajMeService parkirajMeService
    ) {
        this.parkirajMeService = parkirajMeService;
        this.localStorage = localStorage;
    }

    public void initialize() {
        disposable.add(
            parkirajMeService.getRegisteredUsers()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    result -> user.setValue(result.getUsers()),
                    throwable -> emitEvent(new FailedLoadUsersEvent())
                ));
    }

    public void onUserSelected(User user) {
        emitEvent(new UserSelectedEvent(user));
    }

    @NonNull
    public MutableLiveData<List<User>> getUsers() {
        return user;
    }

}
