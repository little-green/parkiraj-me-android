package hr.fer.littlegreen.parkirajme.androidapp.ui.person.map;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import dagger.hilt.android.AndroidEntryPoint;
import hr.fer.littlegreen.parkirajme.androidapp.R;
import hr.fer.littlegreen.parkirajme.androidapp.data.local.LocalStorage;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.FragmentMapBinding;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.ParkingObject;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseFragment;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.map.dialogs.ParkingObjectDetailsDialog;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.map.events.ErrorEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.map.events.MapEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.map.events.MoveToParkingObjectEvent;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

@AndroidEntryPoint
public final class MapFragment extends BaseFragment {

    private static final int RC_ENABLE_LOCATION = 1;

    private static final double DEFAULT_LATITUDE = 45.8014;
    private static final double DEFAULT_LONGITUDE = 15.9711;
    private static final float DEFAULT_ZOOM = 12f;
    private static final float OBJECT_ZOOM = 15f;

    private MapViewModel viewModel;
    private FragmentMapBinding binding;
    private GoogleMap map;
    private final Map<String, ParkingObject> markers = new HashMap<>();

    private FusedLocationProviderClient locationProvider;
    private Location lastKnownLocation;

    private boolean permissionDenied = false;

    @Inject
    LocalStorage localStorage;

    @NonNull
    private final Observer<MapEvent> eventsObserver = event -> {
        if (event instanceof MoveToParkingObjectEvent) {
            ParkingObject parkingObject = ((MoveToParkingObjectEvent) event).getParkingObject();
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                parkingObject.getLatitude().doubleValue(),
                parkingObject.getLongitude().doubleValue()
            ), OBJECT_ZOOM));
        } else if (event instanceof ErrorEvent) {
            String message = ((ErrorEvent) event).getMessage();
            if (message == null) {
                message = getString(R.string.generic_error);
            }
            Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show();
        }
    };

    @NonNull
    private final Observer<List<ParkingObject>> parkingObjectsObserver = parkingObjects -> {
        if (parkingObjects == null) {
            return;
        }
        map.clear();
        markers.clear();
        for (ParkingObject parkingObject : parkingObjects) {
            Marker m = map.addMarker(new MarkerOptions().position(new LatLng(
                parkingObject.getLatitude().doubleValue(),
                parkingObject.getLongitude().doubleValue()
            )));
            markers.put(m.getId(), parkingObject);
        }
    };

    @NonNull
    private final Observer<ParkingObject> selectedParkingObjectObserver = parkingObject -> {
        if (parkingObject == null) {
            return;
        }
        new ParkingObjectDetailsDialog(parkingObject, localStorage.getSession() != null)
            .show(getChildFragmentManager(), ParkingObjectDetailsDialog.TAG);
    };

    @NonNull
    private final OnMapReadyCallback onMapReadyCallback = new OnMapReadyCallback() {

        @Override
        public void onMapReady(GoogleMap googleMap) {
            map = googleMap;
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(DEFAULT_LATITUDE, DEFAULT_LONGITUDE), DEFAULT_ZOOM));
            map.setOnMarkerClickListener(marker -> {
                viewModel.selectParkingObject(markers.get(marker.getId()));
                return true;
            });
            enableLocation();
            viewModel.getParkingObjects().observe(getViewLifecycleOwner(), parkingObjectsObserver);
        }
    };

    @NonNull
    private final LocationCallback locationCallback = new LocationCallback() {

        @Override
        public void onLocationResult(LocationResult locationResult) {
            lastKnownLocation = locationResult.getLastLocation();
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        locationProvider = LocationServices.getFusedLocationProviderClient(requireContext());
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentMapBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(this).get(MapViewModel.class);
        viewModel.getEvents().observe(getViewLifecycleOwner(), eventsObserver);
        viewModel.getSelectedParkingObject().observe(getViewLifecycleOwner(), selectedParkingObjectObserver);
        viewModel.initialize();
        binding.mapActionButton.setOnClickListener(v -> {
            if (lastKnownLocation != null) {
                viewModel.findTheClosestParkingObject(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude());
            } else {
                Toast.makeText(requireContext(), "Nije moguće pristupiti trenutnoj lokaciji.", Toast.LENGTH_SHORT).show();
                enableLocation();
            }
        });
        ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapFragmentContainer)).getMapAsync(onMapReadyCallback);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (permissionDenied) {
            return;
        }
        enableLocation();
    }

    @Override
    public void onPause() {
        super.onPause();
        locationProvider.removeLocationUpdates(locationCallback);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                enableLocation();
                permissionDenied = false;
            } else {
                permissionDenied = true;
            }
        }
    }

    private void enableLocation() {
        if (requireContext().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[] {ACCESS_FINE_LOCATION}, RC_ENABLE_LOCATION);
            return;
        }
        if (map != null) {
            map.setMyLocationEnabled(true);
        }
        locationProvider.requestLocationUpdates(LocationRequest.create(), locationCallback, Looper.getMainLooper());
    }
}
