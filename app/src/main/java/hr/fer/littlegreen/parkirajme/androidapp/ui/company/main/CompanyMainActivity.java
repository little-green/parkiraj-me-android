package hr.fer.littlegreen.parkirajme.androidapp.ui.company.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import dagger.hilt.android.AndroidEntryPoint;
import hr.fer.littlegreen.parkirajme.androidapp.R;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.ActivityCompanyMainBinding;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Company;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseFragment;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.information.CompanyInformationFragment;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.main.state.CompanyScreenType;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.parkingobjects.ParkingObjectsFragment;

@AndroidEntryPoint
public class CompanyMainActivity extends BaseActivity {

    @NonNull
    private static final String EXTRA_COMPANY = "EXTRA_COMPANY";
    private CompanyMainViewModel viewModel;
    private ActivityCompanyMainBinding binding;
    @Nullable
    private BaseFragment currentStep;
    @NonNull
    private final Observer<CompanyScreenType> companyScreenTypeObserver = companyScreenType -> {
        switch (companyScreenType) {
            case COMPANY_SCREEN_PARKING_OBJECTS:
                setFragment(new ParkingObjectsFragment());
                break;
            case COMPANY_SCREEN_PERSONAL_DETAILS:
                setFragment(new CompanyInformationFragment());
                break;
            default:
                removeFragment();
        }
    };

    @NonNull
    public static Intent newIntent(@NonNull Context context, @NonNull Company company) {
        Intent intent = new Intent(context, CompanyMainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(EXTRA_COMPANY, company);
        return intent;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Company company = (Company) getIntent().getSerializableExtra(EXTRA_COMPANY);
        viewModel = new ViewModelProvider(this).get(CompanyMainViewModel.class);
        viewModel.initialize(company);
        viewModel.getClientScreenType().observe(this, companyScreenTypeObserver);
        binding = ActivityCompanyMainBinding.inflate(getLayoutInflater());
        binding.companyScreenNavigation.setOnNavigationItemSelectedListener(item -> {
            int id = item.getItemId();
            if (id == R.id.parkingObjectList) {
                viewModel.onCompanyScreenSelected(CompanyScreenType.COMPANY_SCREEN_PARKING_OBJECTS);
                return true;
            } else if (id == R.id.companyInformation) {
                viewModel.onCompanyScreenSelected(CompanyScreenType.COMPANY_SCREEN_PERSONAL_DETAILS);
                return true;
            } else {
                viewModel.onCompanyScreenSelected(CompanyScreenType.COMPANY_SCREEN_PERSONAL_DETAILS);
                return true;
            }
        });
        setContentView(binding.getRoot());
    }

    private void setFragment(BaseFragment fragment) {
        currentStep = fragment;
        getSupportFragmentManager()
            .beginTransaction()
            .replace(R.id.companyScreenContainer, fragment)
            .commit();
    }

    private void removeFragment() {
        getSupportFragmentManager()
            .beginTransaction()
            .remove(currentStep)
            .commit();
    }
}
