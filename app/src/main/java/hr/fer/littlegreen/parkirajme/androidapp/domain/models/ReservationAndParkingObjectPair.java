package hr.fer.littlegreen.parkirajme.androidapp.domain.models;

import java.io.Serializable;
import java.util.Objects;

import androidx.annotation.NonNull;

public final class ReservationAndParkingObjectPair implements Serializable {

    @NonNull
    private final Reservation reservation;

    @NonNull
    private final ParkingObject parkingObject;

    public ReservationAndParkingObjectPair(
        @NonNull Reservation reservation,
        @NonNull ParkingObject parkingObject
    ) {
        this.reservation = reservation;
        this.parkingObject = parkingObject;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ReservationAndParkingObjectPair that = (ReservationAndParkingObjectPair) o;
        return reservation.equals(that.reservation) &&
            parkingObject.equals(that.parkingObject);
    }

    @Override
    public int hashCode() {
        return Objects.hash(reservation, parkingObject);
    }

    @NonNull
    public Reservation getReservation() {
        return reservation;
    }

    @NonNull
    public ParkingObject getParkingObject() {
        return parkingObject;
    }
}
