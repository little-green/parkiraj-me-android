package hr.fer.littlegreen.parkirajme.androidapp.ui.unregistereduser;

import androidx.hilt.lifecycle.ViewModelInject;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseViewModel;

public final class UnregisteredUserViewModel extends BaseViewModel {

    @ViewModelInject
    public UnregisteredUserViewModel() {
    }
}
