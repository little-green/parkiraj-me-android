package hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.stepmanagement;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public final class LinearStepSequence extends StepSequence {

    @NonNull
    private final List<StepSequence> sequences;

    public LinearStepSequence(@NonNull List<StepSequence> sequences) {
        this.sequences = sequences;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LinearStepSequence that = (LinearStepSequence) o;
        return sequences.equals(that.sequences);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sequences);
    }

    @Override
    public int getProgressWeight() {
        int steps = 0;
        for (StepSequence sequence : sequences) {
            steps += sequence.getProgressWeight();
        }
        return steps;
    }

    @NonNull
    public List<StepSequence> getSequences() {
        return sequences;
    }

    @NonNull
    @Override
    public StepSequenceIterator stepSequenceIterator() {
        return new LinearStepSequenceIterator();
    }

    private class LinearStepSequenceIterator implements StepSequenceIterator {

        private int i = 0;

        @NonNull
        private final List<StepSequenceIterator> stepSequenceIterators = new ArrayList<>(sequences.size());

        @Nullable
        private StepSequenceIterator currentStepSequenceIterator;

        public LinearStepSequenceIterator() {
            for (int j = 0; j < sequences.size(); ++j) {
                stepSequenceIterators.add(sequences.get(j).stepSequenceIterator());
            }
            if (i != sequences.size()) {
                currentStepSequenceIterator = stepSequenceIterators.get(i);
            }
        }

        @Override
        public boolean canDereference() {
            return currentStepSequenceIterator != null && currentStepSequenceIterator.canDereference();
        }

        @NonNull
        @Override
        public StepSequence dereference() throws IllegalStateException {
            if (canDereference()) {
                return currentStepSequenceIterator.dereference();
            } else {
                throw new IllegalStateException();
            }
        }

        @Override
        public void iterateForward() {
            if (currentStepSequenceIterator != null) {
                currentStepSequenceIterator.iterateForward();
                if (!currentStepSequenceIterator.canDereference()) {
                    iterateSequenceIndexForward();
                }
            } else {
                iterateSequenceIndexForward();
            }
        }

        @Override
        public void iterateBackward() {
            if (currentStepSequenceIterator != null) {
                currentStepSequenceIterator.iterateBackward();
                if (!currentStepSequenceIterator.canDereference()) {
                    iterateSequenceIndexBackward();
                }
            } else {
                iterateSequenceIndexBackward();
            }
        }

        @Override
        public double getIteratedWeight() {
            double iteratedWeight = 0d;
            for (int j = 0; j <= i && j < stepSequenceIterators.size(); ++j) {
                iteratedWeight += stepSequenceIterators.get(j).getIteratedWeight();
            }
            return iteratedWeight;
        }

        private void iterateSequenceIndexForward() {
            ++i;
            if (i < sequences.size()) {
                StepSequenceIterator stepSequenceIterator = sequences.get(i).stepSequenceIterator();
                stepSequenceIterators.set(i, stepSequenceIterator);
                currentStepSequenceIterator = stepSequenceIterator;
            } else {
                currentStepSequenceIterator = null;
            }
        }

        private void iterateSequenceIndexBackward() {
            --i;
            if (i >= 0) {
                currentStepSequenceIterator = stepSequenceIterators.get(i);
                if (!currentStepSequenceIterator.canDereference()) {
                    currentStepSequenceIterator.iterateBackward();
                }
            } else {
                currentStepSequenceIterator = null;
            }
        }
    }
}
