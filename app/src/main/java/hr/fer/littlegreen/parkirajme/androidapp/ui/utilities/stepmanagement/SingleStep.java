package hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.stepmanagement;

import androidx.annotation.NonNull;

public abstract class SingleStep extends StepSequence {

    @Override
    public int getProgressWeight() {
        return 1;
    }

    @NonNull
    @Override
    public StepSequenceIterator stepSequenceIterator() {
        return new SingleStepIterator();
    }

    private class SingleStepIterator implements StepSequenceIterator {

        private int i = 0;

        @Override
        public boolean canDereference() {
            return i == 0;
        }

        @NonNull
        @Override
        public StepSequence dereference() throws IllegalStateException {
            if (canDereference()) {
                return SingleStep.this;
            } else {
                throw new IllegalStateException();
            }
        }

        @Override
        public void iterateForward() {
            ++i;
        }

        @Override
        public void iterateBackward() {
            --i;
        }

        @Override
        public double getIteratedWeight() {
            return getProgressWeight();
        }
    }
}
