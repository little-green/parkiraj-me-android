package hr.fer.littlegreen.parkirajme.androidapp.ui.automaticlogin;

import android.os.Bundle;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import dagger.hilt.android.AndroidEntryPoint;
import hr.fer.littlegreen.parkirajme.androidapp.data.local.LocalStorage;
import hr.fer.littlegreen.parkirajme.androidapp.data.local.Session;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Administrator;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Company;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Person;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.User;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.main.AdministratorMainActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.main.CompanyMainActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.main.PersonMainActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.welcome.WelcomeActivity;

@AndroidEntryPoint
public final class AutomaticLoginActivity extends BaseActivity {

    @Inject
    LocalStorage localStorage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Session session = localStorage.getSession();
        if (session != null) {
            User user = session.getUser();
            if (user instanceof Person) {
                startActivity(PersonMainActivity.newIntent(this, (Person) user));
            } else if (user instanceof Company) {
                startActivity(CompanyMainActivity.newIntent(this, (Company) user));
            } else if (user instanceof Administrator) {
                startActivity(AdministratorMainActivity.newIntent(this, (Administrator) user));
            }
        } else {
            startActivity(WelcomeActivity.newIntent(this));
        }
        finish();
    }
}
