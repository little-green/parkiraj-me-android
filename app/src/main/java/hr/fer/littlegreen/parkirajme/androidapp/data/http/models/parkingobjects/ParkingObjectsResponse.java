package hr.fer.littlegreen.parkirajme.androidapp.data.http.models.parkingobjects;

import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.ParkingObject;

public class ParkingObjectsResponse {

    @NonNull
    private final List<ParkingObject> companyParkingObjectList;

    public ParkingObjectsResponse(@NonNull List<ParkingObject> parkingObjects) {
        this.companyParkingObjectList = parkingObjects;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ParkingObjectsResponse that = (ParkingObjectsResponse) o;
        return companyParkingObjectList.equals(that.companyParkingObjectList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(companyParkingObjectList);
    }

    @NonNull
    public List<ParkingObject> getCompanyParkingObjectList() {
        return companyParkingObjectList;
    }
}
