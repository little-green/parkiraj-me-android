package hr.fer.littlegreen.parkirajme.androidapp.ui.person.createreservation.state;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;

public final class CreateReservationChooseDateDialog implements CreateReservationDialog {

    @StringRes
    private final int title;

    private final long selection;

    @NonNull
    private final Listener listener;

    public CreateReservationChooseDateDialog(
        int title,
        long selection,
        @NonNull Listener listener
    ) {
        this.title = title;
        this.selection = selection;
        this.listener = listener;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CreateReservationChooseDateDialog that = (CreateReservationChooseDateDialog) o;
        return title == that.title &&
            selection == that.selection &&
            listener.equals(that.listener);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, selection, listener);
    }

    public int getTitle() {
        return title;
    }

    public long getSelection() {
        return selection;
    }

    @NonNull
    public Listener getListener() {
        return listener;
    }

    public interface Listener {

        void onDateSelected(Long epochDay);
    }
}
