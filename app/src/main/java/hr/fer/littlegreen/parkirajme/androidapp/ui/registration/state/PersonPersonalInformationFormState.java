package hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state;

import java.util.Objects;

import androidx.annotation.Nullable;

public final class PersonPersonalInformationFormState {

    @Nullable
    private final String oib;

    @Nullable
    private final String name;

    @Nullable
    private final String surname;

    public PersonPersonalInformationFormState(@Nullable String oib, @Nullable String name, @Nullable String surname) {
        this.oib = oib;
        this.name = name;
        this.surname = surname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PersonPersonalInformationFormState that = (PersonPersonalInformationFormState) o;
        return Objects.equals(oib, that.oib) &&
            Objects.equals(name, that.name) &&
            Objects.equals(surname, that.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(oib, name, surname);
    }

    @Nullable
    public String getOib() {
        return oib;
    }

    @Nullable
    public String getName() {
        return name;
    }

    @Nullable
    public String getSurname() {
        return surname;
    }
}
