package hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state;

import java.util.Arrays;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.stepmanagement.LinearStepSequence;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.stepmanagement.StepSequenceFork;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.stepmanagement.StepSequenceIterator;

public final class RegistrationStepSequenceManagerImpl implements RegistrationStepSequenceManager {

    @NonNull
    private final RegistrationStep userTypeStep = new RegistrationStep(RegistrationStepType.USER_TYPE);

    @NonNull
    private final LinearStepSequence personStepSequence = new LinearStepSequence(
        Arrays.asList(
            new RegistrationStep(RegistrationStepType.PERSON_PERSONAL_INFORMATION),
            new RegistrationStep(RegistrationStepType.PERSON_VEHICLE_INFORMATION),
            new RegistrationStep(RegistrationStepType.PERSON_CARD_INFORMATION)
        )
    );

    @NonNull
    private final RegistrationStep businessStepSequence = new RegistrationStep(RegistrationStepType.BUSINESS_INFORMATION);

    @NonNull
    private final StepSequenceFork personAndBusinessStepSequenceFork = new StepSequenceFork(personStepSequence, businessStepSequence);

    @NonNull
    private final RegistrationStep loginInformationStep = new RegistrationStep(RegistrationStepType.LOGIN_INFORMATION);

    @NonNull
    private final LinearStepSequence registrationStepSequence = new LinearStepSequence(
        Arrays.asList(
            userTypeStep,
            personAndBusinessStepSequenceFork,
            loginInformationStep
        )
    );

    @NonNull
    private final StepSequenceIterator registrationStepSequenceIterator = registrationStepSequence.stepSequenceIterator();

    @NonNull
    private final MutableLiveData<Double> progress = new MutableLiveData<>(calculateProgress());

    @NonNull
    private final MutableLiveData<RegistrationStepType> registrationStepType = new MutableLiveData<>(userTypeStep.getType());

    @Inject
    public RegistrationStepSequenceManagerImpl() {
    }

    @NonNull
    @Override
    public LiveData<RegistrationStepType> getRegistrationStepType() {
        return registrationStepType;
    }

    @Override
    public boolean isFirstStep() {
        registrationStepSequenceIterator.iterateBackward();
        boolean result = !registrationStepSequenceIterator.canDereference();
        registrationStepSequenceIterator.iterateForward();
        return result;
    }

    @Override
    public void iterateForward() {
        registrationStepSequenceIterator.iterateForward();
        if (!registrationStepSequenceIterator.canDereference()) {
            registrationStepSequenceIterator.iterateBackward();
        }
        updateState();
    }

    @Override
    public void iterateBackward() {
        registrationStepSequenceIterator.iterateBackward();
        if (!registrationStepSequenceIterator.canDereference()) {
            registrationStepSequenceIterator.iterateForward();
        }
        updateState();
    }

    @NonNull
    @Override
    public LiveData<Double> getProgress() {
        return progress;
    }

    @Override
    public void activatePersonStepSequence() {
        personAndBusinessStepSequenceFork.activateFirstSequence();
    }

    @Override
    public void activateBusinessStepSequence() {
        personAndBusinessStepSequenceFork.activateSecondSequence();
    }

    private void updateState() {
        RegistrationStep currentStep = (RegistrationStep) registrationStepSequenceIterator.dereference();
        progress.setValue(calculateProgress());
        registrationStepType.setValue(currentStep.getType());
    }

    private double calculateProgress() {
        return (double) registrationStepSequenceIterator.getIteratedWeight() / registrationStepSequence.getProgressWeight();
    }
}
