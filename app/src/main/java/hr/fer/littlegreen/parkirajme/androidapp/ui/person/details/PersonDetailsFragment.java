package hr.fer.littlegreen.parkirajme.androidapp.ui.person.details;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import java.time.YearMonth;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import dagger.hilt.android.AndroidEntryPoint;
import hr.fer.littlegreen.parkirajme.androidapp.R;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.FragmentPersonDetailsBinding;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Person;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseFragment;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.details.adapters.VehicleAdapter;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.details.events.AccountDeletedEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.details.events.ErrorEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.details.events.LogoutEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.details.events.PersonDetailsEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.details.listeners.CreditCardInformationChangedListener;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.details.listeners.ValueChangedListener;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.details.state.PersonDetailsDialogType;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.editvehicles.EditVehiclesActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.TextViewUtil;
import hr.fer.littlegreen.parkirajme.androidapp.ui.welcome.WelcomeActivity;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;

@AndroidEntryPoint
public final class PersonDetailsFragment extends BaseFragment {

    private static final int RC_EDIT_VEHICLES = 1;

    @NonNull
    private final VehicleAdapter vehicleAdapter = new VehicleAdapter();
    private PersonDetailsViewModel viewModel;
    private FragmentPersonDetailsBinding binding;

    @Nullable
    private AlertDialog dialog;

    @NonNull
    private final Observer<PersonDetailsEvent> eventObserver = event -> {
        if (event instanceof LogoutEvent) {
            startActivity(WelcomeActivity.newIntent(requireContext()));
        } else if (event instanceof AccountDeletedEvent) {
            startActivity(WelcomeActivity.newIntent(requireContext()));
        } else if (event instanceof ErrorEvent) {
            String message = ((ErrorEvent) event).getMessage();
            if (message == null) {
                message = getString(R.string.generic_error);
            }
            Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show();
        }
    };
    @Nullable
    AlertDialog editDialog;

    @NonNull
    private final Observer<PersonDetailsDialogType> dialogTypeObserver = dialogType -> {
        if (dialogType == null) {
            if (dialog != null) {
                dialog.dismiss();
            }
            dialog = null;
        } else if (dialogType == PersonDetailsDialogType.LOGOUT_CONFIRMATION) {
            dialog = new AlertDialog.Builder(requireActivity())
                .setTitle("Odjava")
                .setMessage("Jeste li sigurni da se želite odjaviti?")
                .setPositiveButton("Odjavi se", (dialog, which) -> viewModel.logout())
                .setNegativeButton("Odustani", (dialog, which) -> viewModel.dismissDialog())
                .setOnCancelListener(dialog -> viewModel.dismissDialog())
                .setOnDismissListener(dialog -> viewModel.dismissDialog())
                .create();
            dialog.show();
        } else if (dialogType == PersonDetailsDialogType.DELETE_ACCOUNT_CONFIRMATION) {
            dialog = new AlertDialog.Builder(requireActivity())
                .setTitle("Brisanje korisničkog računa")
                .setMessage("Jeste li sigurni da želite izbrisati korisnički račun? Ova se radnja ne može poništiti.")
                .setPositiveButton("Izrbiši korisnički račun", (dialog, which) -> viewModel.deleteAccount())
                .setNegativeButton("Odustani", (dialog, which) -> viewModel.dismissDialog())
                .setOnCancelListener(dialog -> viewModel.dismissDialog())
                .setOnDismissListener(dialog -> viewModel.dismissDialog())
                .create();
            dialog.show();
        } else if (dialogType == PersonDetailsDialogType.ERROR) {
            dialog = new AlertDialog.Builder(requireActivity())
                .setTitle("Greška")
                .setMessage("Došlo je do neočekivane pogreške. Pokušajte ponovo.")
                .setPositiveButton("U redu", (dialog, which) -> viewModel.dismissDialog())
                .setOnCancelListener(dialog -> viewModel.dismissDialog())
                .setOnDismissListener(dialog -> viewModel.dismissDialog())
                .create();
            dialog.show();
        }
    };

    @NonNull
    private final Observer<Person> personObserver = person -> {
        TextViewUtil.update(binding.personDetailsOibFieldValue, person.getOib());
        TextViewUtil.update(binding.personDetailsFirstNameFieldValue, person.getFirstName());
        TextViewUtil.update(binding.personDetailsLastNameFieldValue, person.getLastName());
        TextViewUtil.update(binding.personDetailsEmailFieldValue, person.getEmail());
        TextViewUtil.update(binding.personDetailsCardNumberValue, person.getCreditCardNumber());
        TextViewUtil.update(binding.personDetailsCardExpirationDateValue, person.getCreditCardExpirationDate().toString());
        vehicleAdapter.submitList(person.getVehicles());
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentPersonDetailsBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.personDetailsVehiclesList.setLayoutManager(new LinearLayoutManager(requireContext()) {

            @Override
            public boolean canScrollVertically() {
                return false;
            }
        });
        binding.personDetailsVehiclesList.setAdapter(vehicleAdapter);
        binding.personDetailsLogoutButton.setOnClickListener(v -> viewModel.onLogoutClicked());
        binding.personDetailsDeleteAccountButton.setOnClickListener(v -> viewModel.onDeleteAccountClicked());
        binding.personDetailsFirstNameEditButton.setOnClickListener(v -> showEditDialog(viewModel::editFirstName));
        binding.personDetailsLastNameEditButton.setOnClickListener(v -> showEditDialog(viewModel::editLastName));
        binding.personDetailsCardDetailsEditButton.setOnClickListener(
            v -> showEditCreditCardInformationDialog(viewModel::editCreditCardInformation)
        );
        binding.personDetailsVehiclesEditButton.setOnClickListener(v -> showEditVehiclesScreen());
        viewModel = new ViewModelProvider(this).get(PersonDetailsViewModel.class);
        viewModel.getPerson().observe(getViewLifecycleOwner(), personObserver);
        viewModel.getDialogType().observe(getViewLifecycleOwner(), dialogTypeObserver);
        viewModel.getEvents().observe(getViewLifecycleOwner(), eventObserver);
        viewModel.initialize();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_EDIT_VEHICLES) {
            if (resultCode == RESULT_OK) {
                viewModel.initialize();
            }
        }
    }

    private void showEditDialog(@NonNull ValueChangedListener listener) {
        editDialog = new AlertDialog.Builder(requireActivity())
            .setTitle(getString(R.string.edit_title))
            .setPositiveButton(getString(R.string.edit_finish), (dialog, which) -> {
                EditText editText = (EditText) editDialog.findViewById(R.id.newValue);
                listener.onValueChanged(editText.getText().toString());
            })
            .setView(R.layout.dialog_edit)
            .create();
        editDialog.show();
    }

    private void showEditCreditCardInformationDialog(@NonNull CreditCardInformationChangedListener listener) {
        editDialog = new AlertDialog.Builder(requireActivity())
            .setTitle(getString(R.string.edit_credit_card_information_title))
            .setPositiveButton(getString(R.string.edit_finish), (dialog, which) -> {
                EditText creditCardNumberField = (EditText) editDialog.findViewById(R.id.creditCardNumberField);
                EditText creditCardExpirationDateField = (EditText) editDialog.findViewById(R.id.creditCardExpirationDateField);
                String[] expiryDateParts = creditCardExpirationDateField.getText().toString().split("/");
                if (expiryDateParts.length != 2) {
                    return;
                }
                YearMonth creditCardExpirationDate;
                try {
                    int expiryDateMonth = Integer.parseInt(expiryDateParts[0]);
                    int expiryDateYear = Integer.parseInt("20" + expiryDateParts[1]);
                    creditCardExpirationDate = YearMonth.of(expiryDateYear, expiryDateMonth);
                } catch (Exception e) {
                    Timber.e(e);
                    creditCardExpirationDate = null;
                }
                listener.onCreditCardInformationChanged(creditCardNumberField.getText().toString(), creditCardExpirationDate);
            })
            .setView(R.layout.dialog_edit_credit_card)
            .create();
        editDialog.show();
    }

    private void showEditVehiclesScreen() {
        startActivityForResult(EditVehiclesActivity.newIntent(requireContext()), RC_EDIT_VEHICLES);
    }
}
