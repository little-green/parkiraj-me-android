package hr.fer.littlegreen.parkirajme.androidapp.ui.person.reservationdetails;

import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.ParkirajMeService;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.ParkingObject;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Reservation;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.ReservationAndParkingObjectPair;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseViewModel;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.reservationdetails.events.ErrorEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.reservationdetails.events.ReservationCancelledEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.reservationdetails.events.ReservationDetailsEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.ErrorExtractor;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.schedulers.Schedulers;
import timber.log.Timber;

public final class ReservationDetailsViewModel extends BaseViewModel<ReservationDetailsEvent> {

    @NonNull
    private final ParkirajMeService parkirajMeService;

    @NonNull
    private final ErrorExtractor errorExtractor;

    @NonNull
    private final MutableLiveData<Reservation> reservation = new MutableLiveData<>();

    @NonNull
    private final MutableLiveData<ParkingObject> parkingObject = new MutableLiveData<>();

    @ViewModelInject
    public ReservationDetailsViewModel(
        @NonNull ParkirajMeService parkirajMeService,
        @NonNull ErrorExtractor errorExtractor
    ) {
        this.parkirajMeService = parkirajMeService;
        this.errorExtractor = errorExtractor;
    }

    public void initialize(ReservationAndParkingObjectPair reservationAndParkingObjectPair) {
        this.reservation.setValue(reservationAndParkingObjectPair.getReservation());
        this.parkingObject.setValue(reservationAndParkingObjectPair.getParkingObject());
    }

    public void cancelReservation() {
        disposable.add(
            parkirajMeService.cancelReservation(reservation.getValue().getReservationId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> emitEvent(new ReservationCancelledEvent()), throwable -> {
                    Timber.e(throwable);
                    emitEvent(new ErrorEvent(errorExtractor.extract(throwable)));
                })
        );
    }

    @NonNull
    public LiveData<Reservation> getReservation() {
        return reservation;
    }

    @NonNull
    public LiveData<String> getParkingObjectAddress() {
        return Transformations.map(parkingObject, ParkingObject::getAddress);
    }
}
