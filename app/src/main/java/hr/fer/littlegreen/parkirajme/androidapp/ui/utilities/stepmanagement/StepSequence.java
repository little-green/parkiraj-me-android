package hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.stepmanagement;

import androidx.annotation.NonNull;

public abstract class StepSequence {

    public abstract int getProgressWeight();

    @NonNull
    public abstract StepSequenceIterator stepSequenceIterator();
}
