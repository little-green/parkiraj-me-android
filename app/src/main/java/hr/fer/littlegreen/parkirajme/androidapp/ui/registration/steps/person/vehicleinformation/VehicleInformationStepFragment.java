package hr.fer.littlegreen.parkirajme.androidapp.ui.registration.steps.person.vehicleinformation;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import dagger.hilt.android.AndroidEntryPoint;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.FragmentVehicleInformationBinding;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseFragment;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state.VehicleInformationFormState;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.TextViewUtil;

@AndroidEntryPoint
public final class VehicleInformationStepFragment extends BaseFragment {

    private VehicleInformationStepViewModel viewModel;
    private FragmentVehicleInformationBinding binding;

    @NonNull
    private final Observer<VehicleInformationFormState> vehicleInformationFormStateObserver = vehicleInformationState -> {
        if (vehicleInformationState == null) {
            return;
        }
        TextViewUtil.update(binding.registrationPlateField, vehicleInformationState.getRegistrationPlate());
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentVehicleInformationBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(this).get(VehicleInformationStepViewModel.class);
        viewModel.getVehicleInformationFormState().observe(getViewLifecycleOwner(), vehicleInformationFormStateObserver);
        TextViewUtil.addTextChangedListener(binding.registrationPlateField, viewModel::onRegistrationPlateChanged);
        binding.continueButton.setOnClickListener(v -> viewModel.onContinueClicked());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
