package hr.fer.littlegreen.parkirajme.androidapp.ui.person.details.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.ItemVehicleBinding;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.User;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Vehicle;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.SimpleDiffCallback;

public final class VehicleAdapter extends ListAdapter<Vehicle, VehicleAdapter.ViewHolder> {

    public VehicleAdapter() {
        super(new SimpleDiffCallback<>());
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(ItemVehicleBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(getItem(position));
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @NonNull
        private final ItemVehicleBinding binding;

        ViewHolder(@NonNull ItemVehicleBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(Vehicle item) {
            binding.getRoot().setText(item.getRegistrationNumber());
        }
    }
}
