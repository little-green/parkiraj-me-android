package hr.fer.littlegreen.parkirajme.androidapp.ui.login;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.ParkirajMeService;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.models.login.LoginRequest;
import hr.fer.littlegreen.parkirajme.androidapp.data.local.LocalStorage;
import hr.fer.littlegreen.parkirajme.androidapp.data.local.Session;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Administrator;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Company;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Person;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.User;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseViewModel;
import hr.fer.littlegreen.parkirajme.androidapp.ui.login.events.AdministratorLoginSuccessEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.login.events.CompanyLoginSuccessEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.login.events.LoginEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.login.events.LoginFailedEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.login.events.PersonLoginSuccessEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.login.state.LoginFormState;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.ErrorExtractor;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.StringUtil;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.schedulers.Schedulers;

public final class LoginViewModel extends BaseViewModel<LoginEvent> {

    @NonNull
    private final ParkirajMeService parkirajMeService;

    @NonNull
    private final LocalStorage localStorage;

    @NonNull
    private final ErrorExtractor errorExtractor;

    @NonNull
    private final MutableLiveData<LoginFormState> loginFormState = new MutableLiveData<>();

    @NonNull
    private final MutableLiveData<Boolean> isProgressShown = new MutableLiveData<>(false);

    @ViewModelInject
    public LoginViewModel(
        @NonNull ParkirajMeService parkirajMeService, @NonNull LocalStorage localStorage,
        @NonNull ErrorExtractor errorExtractor
    ) {
        this.parkirajMeService = parkirajMeService;
        this.localStorage = localStorage;
        this.errorExtractor = errorExtractor;
    }

    public void onEmailChanged(String email) {
        LoginFormState state = getOrCreateLoginFormState();
        loginFormState.setValue(new LoginFormState(
            email,
            state.getPassword()
        ));
    }

    public void onPasswordChanged(String password) {
        LoginFormState state = getOrCreateLoginFormState();
        loginFormState.setValue(new LoginFormState(
            state.getEmail(),
            password
        ));
    }

    public void login() {
        LoginFormState state = loginFormState.getValue();
        if (isValid(state)) {
            isProgressShown.setValue(true);
            disposable.add(
                parkirajMeService.login(new LoginRequest(state.getEmail(), state.getPassword()))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(result -> {
                        User user = result.getUser();
                        localStorage.putSession(new Session(result.getToken(), user));
                        isProgressShown.setValue(false);
                        if (user instanceof Person) {
                            emitEvent(new PersonLoginSuccessEvent((Person) user));
                        } else if (user instanceof Company) {
                            emitEvent(new CompanyLoginSuccessEvent((Company) user));
                        } else if (user instanceof Administrator) {
                            emitEvent(new AdministratorLoginSuccessEvent((Administrator) user));
                        }
                    }, throwable -> {
                        isProgressShown.setValue(false);
                        emitEvent(new LoginFailedEvent(errorExtractor.extract(throwable)));
                    })
            );
        }
    }

    @NonNull
    public LiveData<LoginFormState> getLoginFormState() {
        return loginFormState;
    }

    @NonNull
    public LiveData<Boolean> isProgressShown() {
        return isProgressShown;
    }

    @NonNull
    private LoginFormState getOrCreateLoginFormState() {
        LoginFormState state = loginFormState.getValue();
        if (state != null) {
            return state;
        } else {
            return new LoginFormState(null, null);
        }
    }

    private boolean isValid(@Nullable LoginFormState state) {
        return state != null && StringUtil.areNotNullOrEmpty(state.getEmail(), state.getPassword());
    }
}