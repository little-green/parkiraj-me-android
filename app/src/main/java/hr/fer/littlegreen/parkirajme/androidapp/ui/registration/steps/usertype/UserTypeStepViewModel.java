package hr.fer.littlegreen.parkirajme.androidapp.ui.registration.steps.usertype;

import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.LiveData;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseViewModel;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state.RegistrationFlowState;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state.RegistrationStepSequenceManager;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.steps.usertype.state.UserType;

public final class UserTypeStepViewModel extends BaseViewModel<BaseEvent> {

    @NonNull
    private final RegistrationStepSequenceManager registrationStepSequenceManager;

    @NonNull
    private final RegistrationFlowState registrationFlowState;

    @ViewModelInject
    public UserTypeStepViewModel(
        @NonNull RegistrationStepSequenceManager registrationStepSequenceManager,
        @NonNull RegistrationFlowState registrationFlowState
    ) {
        this.registrationStepSequenceManager = registrationStepSequenceManager;
        this.registrationFlowState = registrationFlowState;
    }

    public void selectPersonOption() {
        registrationFlowState.userType.setValue(UserType.PERSON);
        registrationStepSequenceManager.activatePersonStepSequence();
    }

    public void selectBusinessOption() {
        registrationFlowState.userType.setValue(UserType.COMPANY);
        registrationStepSequenceManager.activateBusinessStepSequence();
    }

    public void onContinueClicked() {
        if (registrationFlowState.userType.getValue() != null) {
            registrationStepSequenceManager.iterateForward();
        }
    }

    @NonNull
    public LiveData<UserType> getUserType() {
        return registrationFlowState.userType;
    }
}
