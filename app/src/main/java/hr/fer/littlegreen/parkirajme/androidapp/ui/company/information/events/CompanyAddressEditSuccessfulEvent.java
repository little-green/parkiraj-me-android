package hr.fer.littlegreen.parkirajme.androidapp.ui.company.information.events;

import org.jetbrains.annotations.NotNull;

public class CompanyAddressEditSuccessfulEvent implements CompanyInformationEvent {

    @NotNull
    private final String address;

    public CompanyAddressEditSuccessfulEvent(@NotNull String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }
}
