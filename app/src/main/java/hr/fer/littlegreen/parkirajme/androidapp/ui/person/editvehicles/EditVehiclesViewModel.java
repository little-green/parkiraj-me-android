package hr.fer.littlegreen.parkirajme.androidapp.ui.person.editvehicles;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.ParkirajMeService;
import hr.fer.littlegreen.parkirajme.androidapp.data.local.LocalStorage;
import hr.fer.littlegreen.parkirajme.androidapp.data.local.Session;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Person;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Vehicle;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseViewModel;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.editvehicles.events.EditVehiclesEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.editvehicles.events.ErrorEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.editvehicles.state.EditableVehicleItem;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.ErrorExtractor;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.schedulers.Schedulers;
import timber.log.Timber;

public final class EditVehiclesViewModel extends BaseViewModel<EditVehiclesEvent> {

    @NonNull
    private final LocalStorage localStorage;

    @NonNull
    private final ParkirajMeService parkirajMeService;

    @NonNull
    private final ErrorExtractor errorExtractor;

    @NonNull
    private final MutableLiveData<List<EditableVehicleItem>> editableVehicleItems = new MutableLiveData<>();

    @NonNull
    private final MutableLiveData<Boolean> isActionInProgress = new MutableLiveData<>(false);

    @ViewModelInject
    public EditVehiclesViewModel(
        @NonNull LocalStorage localStorage,
        @NonNull ParkirajMeService parkirajMeService,
        @NonNull ErrorExtractor errorExtractor
    ) {
        this.localStorage = localStorage;
        this.parkirajMeService = parkirajMeService;
        this.errorExtractor = errorExtractor;
    }

    public void initialize() {
        List<EditableVehicleItem> editableVehicleItems = new ArrayList<>();
        for (Vehicle vehicle : ((Person) localStorage.getSession().getUser()).getVehicles()) {
            editableVehicleItems.add(new EditableVehicleItem(vehicle, false));
        }
        this.editableVehicleItems.setValue(editableVehicleItems);
    }

    public void deleteVehicle(@NonNull EditableVehicleItem editableVehicleItem) {
        if (editableVehicleItems.getValue().size() == 1) {
            emitEvent(new ErrorEvent(null));
            return;
        }
        Person person = (Person) localStorage.getSession().getUser();
        updateVehicleItemProgress(editableVehicleItem, true);
        disposable.add(
            parkirajMeService.deleteVehicle(person.getUserUuid(), editableVehicleItem.getVehicle().getRegistrationNumber())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    removeVehicleItem(editableVehicleItem);
                    updateLocalStorage();
                }, throwable -> {
                    Timber.e(throwable);
                    updateVehicleItemProgress(editableVehicleItem, false);
                    emitEvent(new ErrorEvent(errorExtractor.extract(throwable)));
                })
        );
    }

    public void createVehicle(@NonNull Vehicle vehicle) {
        if (vehicle.getRegistrationNumber().isEmpty()) {
            emitEvent(new ErrorEvent(null));
            return;
        }
        Person person = (Person) localStorage.getSession().getUser();
        isActionInProgress.setValue(true);
        disposable.add(
            parkirajMeService.addVehicle(person.getUserUuid(), vehicle.getRegistrationNumber())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    isActionInProgress.setValue(false);
                    addVehicleItem(new EditableVehicleItem(vehicle, false));
                    updateLocalStorage();
                }, throwable -> {
                    Timber.e(throwable);
                    isActionInProgress.setValue(false);
                    emitEvent(new ErrorEvent(errorExtractor.extract(throwable)));
                })
        );
    }

    @NonNull
    public LiveData<List<EditableVehicleItem>> getEditableVehicleItems() {
        return editableVehicleItems;
    }

    @NonNull
    public LiveData<Boolean> isActionInProgress() {
        return isActionInProgress;
    }

    private void updateVehicleItemProgress(@NonNull EditableVehicleItem targetItem, boolean isActionInProgress) {
        List<EditableVehicleItem> items = new ArrayList<>();
        for (EditableVehicleItem item : editableVehicleItems.getValue()) {
            if (item.getVehicle().getRegistrationNumber().equals(targetItem.getVehicle().getRegistrationNumber())) {
                items.add(new EditableVehicleItem(targetItem.getVehicle(), isActionInProgress));
            } else {
                items.add(item);
            }
        }
        editableVehicleItems.setValue(items);
    }

    private void removeVehicleItem(@NonNull EditableVehicleItem targetItem) {
        List<EditableVehicleItem> items = new ArrayList<>(this.editableVehicleItems.getValue());
        items.removeIf(item -> item.getVehicle().getRegistrationNumber().equals(targetItem.getVehicle().getRegistrationNumber()));
        editableVehicleItems.setValue(items);
    }

    private void addVehicleItem(@NonNull EditableVehicleItem item) {
        List<EditableVehicleItem> items = new ArrayList<>(editableVehicleItems.getValue());
        items.add(item);
        editableVehicleItems.setValue(items);
    }

    private void updateLocalStorage() {
        Person currentPerson = (Person) localStorage.getSession().getUser();
        Person newPerson = new Person(
            currentPerson.getUserUuid(),
            currentPerson.getEmail(),
            currentPerson.getOib(),
            currentPerson.getFirstName(),
            currentPerson.getLastName(),
            currentPerson.getCreditCardNumber(),
            currentPerson.getCreditCardExpirationDate(),
            getVehicles()
        );
        localStorage.putSession(new Session(localStorage.getSession().getToken(), newPerson));
    }

    private List<Vehicle> getVehicles() {
        List<Vehicle> vehicles = new ArrayList<>();
        for (EditableVehicleItem item : editableVehicleItems.getValue()) {
            vehicles.add(item.getVehicle());
        }
        return vehicles;
    }
}
