package hr.fer.littlegreen.parkirajme.androidapp.ui.person.reservations.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;
import hr.fer.littlegreen.parkirajme.androidapp.R;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.ItemReservationBinding;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Reservation;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.ReservationAndParkingObjectPair;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.SimpleDiffCallback;

public final class ReservationAdapter extends ListAdapter<ReservationAndParkingObjectPair, ReservationAdapter.ViewHolder> {

    @NonNull
    private final ReservationClickListener reservationClickListener;

    public ReservationAdapter(@NonNull ReservationClickListener reservationClickListener) {
        super(new SimpleDiffCallback<>());
        this.reservationClickListener = reservationClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(ItemReservationBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(getItem(position));
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @NonNull
        private final ItemReservationBinding binding;

        @NonNull
        private final Context context;

        ViewHolder(@NonNull ItemReservationBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            context = binding.getRoot().getContext();
        }

        void bind(@NonNull ReservationAndParkingObjectPair reservationAndParkingObjectPair) {
            binding.reservationItemTitle.setText(reservationAndParkingObjectPair.getParkingObject().getAddress());
            binding.reservationItemType.setText(getReservationTypeText(reservationAndParkingObjectPair.getReservation()));
            binding.getRoot().setOnClickListener(v -> reservationClickListener.onClick(reservationAndParkingObjectPair));
        }

        @NonNull
        private String getReservationTypeText(@NonNull Reservation reservation) {
            switch (reservation.getReservationType()) {
                case ONE_TIME:
                    return context.getString(R.string.reservation_one_time);
                case PERIODIC:
                    return context.getString(R.string.reservation_periodic);
                case ALL_TIME:
                    return context.getString(R.string.reservation_all_time);
                default:
                    return "";
            }
        }
    }
}
