package hr.fer.littlegreen.parkirajme.androidapp.ui.company.main;

import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Company;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseViewModel;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.main.state.CompanyScreenType;

public class CompanyMainViewModel extends BaseViewModel<BaseEvent> {

    @NonNull
    private final MutableLiveData<CompanyScreenType> companyScreenType =
        new MutableLiveData<>(CompanyScreenType.COMPANY_SCREEN_PERSONAL_DETAILS);

    private Company company;

    @ViewModelInject
    public CompanyMainViewModel() {
    }

    public void initialize(@NonNull Company company) {
        this.company = company;
    }

    public void onCompanyScreenSelected(@NonNull CompanyScreenType companyScreenType) {
        this.companyScreenType.setValue(companyScreenType);
    }

    @NonNull
    public LiveData<CompanyScreenType> getClientScreenType() {
        return companyScreenType;
    }
}

