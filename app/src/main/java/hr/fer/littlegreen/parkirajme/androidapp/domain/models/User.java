package hr.fer.littlegreen.parkirajme.androidapp.domain.models;

import java.io.Serializable;
import java.util.Objects;

import androidx.annotation.NonNull;

public class User implements Serializable {

    @NonNull
    private final String userUuid;

    @NonNull
    private final String email;

    @NonNull
    private final String oib;


    public User(@NonNull String userUuid, @NonNull String email, @NonNull String oib) {
        this.userUuid = userUuid;
        this.email = email;
        this.oib = oib;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        User user = (User) o;
        return userUuid.equals(user.userUuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userUuid);
    }

    @NonNull
    public String getUserUuid() {
        return userUuid;
    }

    @NonNull
    public String getEmail() {
        return email;
    }

    @NonNull
    public String getOib() {
        return oib;
    }
}
