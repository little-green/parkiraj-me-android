package hr.fer.littlegreen.parkirajme.androidapp.ui.company.information;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import dagger.hilt.android.AndroidEntryPoint;
import hr.fer.littlegreen.parkirajme.androidapp.R;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.FragmentCompanyInformationBinding;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Company;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseFragment;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.information.events.AccountDeletedEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.information.events.CompanyAddressEditSuccessfulEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.information.events.CompanyEditFailedEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.information.events.CompanyInformationEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.information.events.CompanyNameEditSuccessfulEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.information.events.LogoutEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.information.state.CompanyInformationDialogType;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.TextViewUtil;
import hr.fer.littlegreen.parkirajme.androidapp.ui.welcome.WelcomeActivity;

@AndroidEntryPoint
public class CompanyInformationFragment extends BaseFragment {

    private CompanyInformationViewModel viewModel;
    private FragmentCompanyInformationBinding binding;

    @Nullable
    private AlertDialog dialog;

    @NonNull
    private final Observer<CompanyInformationDialogType> dialogTypeObserver = dialogType -> {
        if (dialogType == null) {
            if (dialog != null) {
                dialog.dismiss();
            }
            dialog = null;
        } else if (dialogType == CompanyInformationDialogType.LOGOUT_CONFIRMATION) {
            dialog = new AlertDialog.Builder(requireActivity())
                .setTitle("Odjava")
                .setMessage("Jeste li sigurni da se želite odjaviti?")
                .setPositiveButton("Odjavi se", (dialog, which) -> viewModel.logout())
                .setNegativeButton("Odustani", (dialog, which) -> viewModel.dismissDialog())
                .setOnCancelListener(dialog -> viewModel.dismissDialog())
                .setOnDismissListener(dialog -> viewModel.dismissDialog())
                .create();
            dialog.show();
        } else if (dialogType == CompanyInformationDialogType.DELETE_ACCOUNT_CONFIRMATION) {
            dialog = new AlertDialog.Builder(requireActivity())
                .setTitle("Brisanje korisničkog računa")
                .setMessage("Jeste li sigurni da želite izbrisati korisnički račun? Ova se radnja ne može poništiti.")
                .setPositiveButton("Izbriši korisnički račun", (dialog, which) -> viewModel.deleteAccount())
                .setNegativeButton("Odustani", (dialog, which) -> viewModel.dismissDialog())
                .setOnCancelListener(dialog -> viewModel.dismissDialog())
                .setOnDismissListener(dialog -> viewModel.dismissDialog())
                .create();
            dialog.show();
        } else if (dialogType == CompanyInformationDialogType.ERROR) {
            dialog = new AlertDialog.Builder(requireActivity())
                .setTitle("Greška")
                .setMessage("Došlo je do neočekivane pogreške. Pokušajte ponovo.")
                .setPositiveButton("U redu", (dialog, which) -> viewModel.dismissDialog())
                .setOnCancelListener(dialog -> viewModel.dismissDialog())
                .setOnDismissListener(dialog -> viewModel.dismissDialog())
                .create();
            dialog.show();
        }
    };
    @Nullable
    private AlertDialog editCompanyNameDialog;

    @NonNull
    private final Observer<CompanyInformationEvent> eventObserver = event -> {
        if (event instanceof LogoutEvent) {
            startActivity(WelcomeActivity.newIntent(requireContext()));
        } else if (event instanceof AccountDeletedEvent) {
            startActivity(WelcomeActivity.newIntent(requireContext()));
        } else if (event instanceof CompanyNameEditSuccessfulEvent) {
            TextViewUtil.update(binding.companyName, ((CompanyNameEditSuccessfulEvent) event).getName());
            Toast.makeText(getContext(), getResources().getString(R.string.edit_successful_message), Toast.LENGTH_LONG).show();
        } else if (event instanceof CompanyAddressEditSuccessfulEvent) {
            TextViewUtil.update(binding.companyAddress, ((CompanyAddressEditSuccessfulEvent) event).getAddress());
            Toast.makeText(getContext(), getResources().getString(R.string.edit_successful_message), Toast.LENGTH_LONG).show();
        } else if (event instanceof CompanyEditFailedEvent) {
            Toast.makeText(getContext(), getResources().getString(R.string.edit_failed_message), Toast.LENGTH_LONG).show();
        }
    };

    @NonNull
    private final Observer<Company> companyObserver = company -> {
        TextViewUtil.update(binding.companyOib, company.getOib());
        TextViewUtil.update(binding.companyName, company.getName());
        TextViewUtil.update(binding.companyAddress, company.getHeadquarterAddress());
    };

    @Nullable
    private AlertDialog editCompanyAddressDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentCompanyInformationBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.logout.setOnClickListener(v -> viewModel.onLogoutClicked());
        binding.deleteAccount.setOnClickListener(v -> viewModel.onDeleteAccountClicked());
        viewModel = new ViewModelProvider(this).get(CompanyInformationViewModel.class);
        viewModel.getCompany().observe(getViewLifecycleOwner(), companyObserver);
        viewModel.getDialogType().observe(getViewLifecycleOwner(), dialogTypeObserver);
        viewModel.getEvents().observe(getViewLifecycleOwner(), eventObserver);

        binding.companyAddressTitle.setOnClickListener(v -> {
            editCompanyAddressDialog =
                new AlertDialog.Builder(requireActivity())
                    .setTitle(getResources().getString(R.string.edit_title))
                    .setPositiveButton(getResources().getString(R.string.edit_finish), (dialog, which) -> {
                        EditText editText = (EditText) editCompanyAddressDialog.findViewById(R.id.newValue);
                        String newAddress = editText.getText().toString();
                        editCompanyAddressDialog.dismiss();
                        viewModel.editAddress(newAddress);
                    })
                    .setNegativeButton(getResources().getString(R.string.edit_cancel), (dialog, which) -> {
                        editCompanyAddressDialog.dismiss();
                    })
                    .setOnCancelListener(dialog -> editCompanyAddressDialog.dismiss())
                    .setOnDismissListener(dialog -> editCompanyAddressDialog.dismiss())
                    .setView(R.layout.dialog_edit)
                    .create();
            editCompanyAddressDialog.show();
        });

        binding.companyNameTitle.setOnClickListener(v -> {
            editCompanyNameDialog =
                new AlertDialog.Builder(requireActivity())
                    .setTitle(getResources().getString(R.string.edit_title))
                    .setPositiveButton(getResources().getString(R.string.edit_finish), (dialog, which) -> {
                        EditText editText = (EditText) editCompanyNameDialog.findViewById(R.id.newValue);
                        String newName = editText.getText().toString();
                        editCompanyNameDialog.dismiss();
                        viewModel.editName(newName);
                    })
                    .setNegativeButton(getResources().getString(R.string.edit_cancel), (dialog, which) -> {
                        editCompanyNameDialog.dismiss();
                    })
                    .setOnCancelListener(dialog -> editCompanyNameDialog.dismiss())
                    .setOnDismissListener(dialog -> editCompanyNameDialog.dismiss())
                    .setView(R.layout.dialog_edit)
                    .create();
            editCompanyNameDialog.show();
        });

        viewModel.initialize();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}