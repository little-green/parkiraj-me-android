package hr.fer.littlegreen.parkirajme.androidapp.ui.utilities;

import androidx.annotation.NonNull;

public final class MoneyUtil {

    public static int parsePrice(@NonNull String data) {
        try {
            String[] parts = data.split("[.,]");
            if (parts.length == 1) {
                return 100 * Integer.parseInt(parts[0]);
            } else if (parts.length == 2) {
                if (parts[1].length() == 1) {
                    return 100 * Integer.parseInt(parts[0]) + 10 * Integer.parseInt(parts[1]);
                } else if (parts[1].length() == 2) {
                    return 100 * Integer.parseInt(parts[0]) + Integer.parseInt(parts[1]);
                } else {
                    return -1;
                }
            } else {
                return -1;
            }
        } catch (Exception e) {
            return -1;
        }
    }
}
