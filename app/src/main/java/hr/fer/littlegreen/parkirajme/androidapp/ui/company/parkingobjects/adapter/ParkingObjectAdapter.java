package hr.fer.littlegreen.parkirajme.androidapp.ui.company.parkingobjects.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.TextRowParkingObjectBinding;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.ParkingObject;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.SimpleDiffCallback;

public class ParkingObjectAdapter extends ListAdapter<ParkingObject, ParkingObjectAdapter.ViewHolder> {

    private ParkingObjectClickListener clickListener;

    public ParkingObjectAdapter(@NotNull ParkingObjectClickListener clickListener) {
        super(new SimpleDiffCallback<>());
        this.clickListener = clickListener;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        return new ViewHolder(TextRowParkingObjectBinding.inflate(LayoutInflater.from(viewGroup.getContext()), viewGroup,
            false
        ));
    }

    @Override
    public void onBindViewHolder(@NonNull ParkingObjectAdapter.ViewHolder holder, int position) {
        holder.getTextView().setText(getItem(position).getName());
        holder.setOnClickListener(getItem(position));
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextRowParkingObjectBinding binding;

        private Context context;

        ViewHolder(@NonNull TextRowParkingObjectBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            context = binding.getRoot().getContext();
        }

        void setOnClickListener(ParkingObject parkingObject) {
            binding.getRoot().setOnClickListener(v -> clickListener.onClick(parkingObject));
        }

        public TextView getTextView() {
            return binding.parkingObjectName;
        }
    }
}

