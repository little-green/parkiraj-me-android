package hr.fer.littlegreen.parkirajme.androidapp.data.http.models.parkingobject;

import java.util.Objects;

import androidx.annotation.Nullable;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.ParkingObject;

public final class ParkingObjectResponse {

    @Nullable
    private final ParkingObject parkingObject;

    public ParkingObjectResponse(@Nullable ParkingObject parkingObject) {
        this.parkingObject = parkingObject;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ParkingObjectResponse that = (ParkingObjectResponse) o;
        return Objects.equals(parkingObject, that.parkingObject);
    }

    @Override
    public int hashCode() {
        return Objects.hash(parkingObject);
    }

    @Nullable
    public ParkingObject getParkingObject() {
        return parkingObject;
    }
}
