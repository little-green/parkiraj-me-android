package hr.fer.littlegreen.parkirajme.androidapp.domain.models;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

import androidx.annotation.NonNull;

public final class ParkingObject implements Serializable {

    @NonNull
    private final String objectUuid;

    @NonNull
    private final String companyUuid;

    @NonNull
    private final String address;

    @NonNull
    private final String name;

    @NotNull
    private final int freeSlots;

    @NotNull
    private final int thirtyMinutePrice;

    @NotNull
    private final int capacity;

    @NonNull
    private final BigDecimal latitude;

    @NonNull
    private final BigDecimal longitude;

    public ParkingObject(
        @NonNull String objectUuid,
        @NonNull String companyUuid,
        int freeSlots,
        int thirtyMinutePrice,
        @NonNull String address,
        @NonNull String objectName,
        int capacity,
        @NonNull BigDecimal latitude,
        @NonNull BigDecimal longitude
    ) {
        this.objectUuid = objectUuid;
        this.companyUuid = companyUuid;
        this.freeSlots = freeSlots;
        this.thirtyMinutePrice = thirtyMinutePrice;
        this.address = address;
        this.name = objectName;
        this.capacity = capacity;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ParkingObject that = (ParkingObject) o;
        return objectUuid.equals(that.objectUuid) &&
            freeSlots == that.freeSlots &&
            thirtyMinutePrice == that.thirtyMinutePrice &&
            capacity == that.capacity &&
            companyUuid.equals(that.companyUuid) &&
            address.equals(that.address) &&
            name.equals(that.name) &&
            latitude.equals(that.latitude) &&
            longitude.equals(that.longitude);
    }

    @Override
    public int hashCode() {
        return Objects.hash(objectUuid, companyUuid, freeSlots, thirtyMinutePrice, address, name, capacity, latitude, longitude);
    }

    @NonNull
    public String getObjectUuid() {
        return objectUuid;
    }

    @NonNull
    public String getCompanyUuid() {
        return companyUuid;
    }

    public int getFreeSlots() {
        return freeSlots;
    }

    public int getThirtyMinutePrice() {
        return thirtyMinutePrice;
    }

    @NonNull
    public String getAddress() {
        return address;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public int getCapacity() {
        return capacity;
    }

    @NonNull
    public BigDecimal getLatitude() {
        return latitude;
    }

    @NonNull
    public BigDecimal getLongitude() {
        return longitude;
    }
}

