package hr.fer.littlegreen.parkirajme.androidapp.ui.person.reservationdetails;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import dagger.hilt.android.AndroidEntryPoint;
import hr.fer.littlegreen.parkirajme.androidapp.R;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.ActivityReservationDetailsBinding;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Reservation;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.ReservationAndParkingObjectPair;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.reservationdetails.events.ErrorEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.reservationdetails.events.ReservationCancelledEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.reservationdetails.events.ReservationDetailsEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.TextViewUtil;

@AndroidEntryPoint
public final class ReservationDetailsActivity extends BaseActivity {

    @NonNull
    private static final String EXTRA_RESERVATION_AND_PARKING_OBJECT_PAIR = "EXTRA_RESERVATION_AND_PARKING_OBJECT_PAIR";

    @NonNull
    private static final String TIME_PATTERN = "HH:mm";

    @NonNull
    private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern(TIME_PATTERN);

    @NonNull
    private static final String RESERVATION_TIME_FORMAT = "%s - %s";

    @NonNull
    public static Intent newIntent(@NonNull Context context, @NonNull ReservationAndParkingObjectPair reservation) {
        return new Intent(context, ReservationDetailsActivity.class)
            .putExtra(EXTRA_RESERVATION_AND_PARKING_OBJECT_PAIR, reservation);
    }

    private ReservationDetailsViewModel viewModel;
    private ActivityReservationDetailsBinding binding;

    @NonNull
    private final Observer<ReservationDetailsEvent> eventsObserver = event -> {
        if (event instanceof ReservationCancelledEvent) {
            setResult(RESULT_OK);
            finish();
        } else if (event instanceof ErrorEvent) {
            String message = ((ErrorEvent) event).getMessage();
            if (message == null) {
                message = getString(R.string.generic_error);
            }
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    };

    @NonNull
    private final Observer<Reservation> reservationObserver = reservation -> {
        TextViewUtil.update(binding.reservationDetailsTypeValue, getReservationTypeText(reservation));
        String time = String.format(
            RESERVATION_TIME_FORMAT,
            reservation.getStartTime().format(TIME_FORMATTER),
            reservation.getEndTime().format(TIME_FORMATTER)
        );
        switch (reservation.getReservationType()) {
            case ONE_TIME:
                showDate(reservation.getStartTime().format(DateTimeFormatter.ISO_LOCAL_DATE));
                showTime(time);
                hideWeekDays();
                hideExpirationDate();
                break;
            case PERIODIC:
                showDate(reservation.getStartTime().format(DateTimeFormatter.ISO_LOCAL_DATE));
                showTime(time);
                showWeekDays(reservation.getDaysOfWeekList());
                showExpirationDate(reservation.getEndTime().toLocalDate());
                break;
            case ALL_TIME:
                hideDate();
                hideTime();
                hideWeekDays();
                showExpirationDate(reservation.getEndTime().toLocalDate());
                break;
        }
    };

    @NonNull
    private final Observer<String> parkingObjectAddressObserver = parkingObjectAddress -> {
        TextViewUtil.update(binding.reservationDetailsTitle, parkingObjectAddress);
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(ReservationDetailsViewModel.class);
        viewModel.getEvents().observe(this, eventsObserver);
        viewModel.getReservation().observe(this, reservationObserver);
        viewModel.getParkingObjectAddress().observe(this, parkingObjectAddressObserver);
        viewModel.initialize((ReservationAndParkingObjectPair) getIntent().getSerializableExtra(EXTRA_RESERVATION_AND_PARKING_OBJECT_PAIR));
        binding = ActivityReservationDetailsBinding.inflate(getLayoutInflater());
        binding.reservationDetailsBackButton.setOnClickListener(v -> finish());
        binding.reservationDetailsCancelButton.setOnClickListener(v -> showConfirmationDialog());
        setContentView(binding.getRoot());
    }

    private void showDate(@NonNull String date) {
        binding.reservationDetailsStartDateLayout.setVisibility(View.VISIBLE);
        TextViewUtil.update(binding.reservationDetailsStartDateValue, date);
    }

    private void hideDate() {
        binding.reservationDetailsStartDateLayout.setVisibility(View.GONE);
    }

    private void showTime(@NonNull String time) {
        binding.reservationDetailsTimeLayout.setVisibility(View.VISIBLE);
        TextViewUtil.update(binding.reservationDetailsTimeValue, time);
    }

    private void hideTime() {
        binding.reservationDetailsTimeLayout.setVisibility(View.GONE);
    }

    private void showWeekDays(@NonNull List<DayOfWeek> weekDays) {
        binding.reservationDetailsWeekDaysLayout.setVisibility(View.VISIBLE);
        TextViewUtil.update(binding.reservationDetailsWeekDaysValue, getWeekDaysText(weekDays));
    }

    private void hideWeekDays() {
        binding.reservationDetailsWeekDaysLayout.setVisibility(View.GONE);
    }

    private void showExpirationDate(@NonNull LocalDate expirationDate) {
        binding.reservationDetailsExpirationDateLayout.setVisibility(View.VISIBLE);
        TextViewUtil.update(binding.reservationDetailsExpirationDateValue, expirationDate.toString());
    }

    private void hideExpirationDate() {
        binding.reservationDetailsExpirationDateLayout.setVisibility(View.GONE);
    }

    private void showConfirmationDialog() {
        new AlertDialog.Builder(this)
            .setTitle("Otkazivanje rezervacije")
            .setMessage("Jeste li sigurni da želite otkazati rezervaciju? Ova se radnja ne može poništiti.")
            .setPositiveButton(getString(R.string.action_cancel_reservation), (dialog, which) -> viewModel.cancelReservation())
            .setNegativeButton(getString(R.string.edit_cancel), null)
            .create()
            .show();
    }

    @NonNull
    private String getReservationTypeText(@NonNull Reservation reservation) {
        switch (reservation.getReservationType()) {
            case ONE_TIME:
                return getString(R.string.reservation_one_time);
            case PERIODIC:
                return getString(R.string.reservation_periodic);
            case ALL_TIME:
                return getString(R.string.reservation_all_time);
            default:
                return "";
        }
    }

    @NonNull
    private String getWeekDaysText(@NonNull List<DayOfWeek> weekDays) {
        StringBuilder result = new StringBuilder();
        for (DayOfWeek dayOfWeek : weekDays) {
            result.append(getWeekDayText(dayOfWeek));
            result.append("\n");
        }
        return result.toString().trim();
    }

    @NonNull
    private String getWeekDayText(@NonNull DayOfWeek dayOfWeek) {
        switch (dayOfWeek) {
            case MONDAY:
                return getString(R.string.monday);
            case TUESDAY:
                return getString(R.string.tuesday);
            case WEDNESDAY:
                return getString(R.string.wednesday);
            case THURSDAY:
                return getString(R.string.thursday);
            case FRIDAY:
                return getString(R.string.friday);
            case SATURDAY:
                return getString(R.string.saturday);
            case SUNDAY:
                return getString(R.string.sunday);
            default:
                return "";
        }
    }
}
