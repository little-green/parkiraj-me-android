package hr.fer.littlegreen.parkirajme.androidapp.ui.welcome;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.ActivityWelcomeBinding;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.login.LoginActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.RegistrationActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.unregistereduser.UnregisteredUserActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.welcome.events.NavigateToLoginEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.welcome.events.NavigateToRegistrationEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.welcome.events.NavigateToUnregisteredUserScreenEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.welcome.events.WelcomeEvent;

public final class WelcomeActivity extends BaseActivity {

    public static Intent newIntent(Context context) {
        return new Intent(context, WelcomeActivity.class)
            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    }

    @NonNull
    private final Observer<WelcomeEvent> eventObserver = event -> {
        if (event instanceof NavigateToLoginEvent) {
            navigateToLogin();
        } else if (event instanceof NavigateToRegistrationEvent) {
            navigateToRegistration();
        } else if (event instanceof NavigateToUnregisteredUserScreenEvent) {
            navigateToUnregisteredUserScreen();
        }
    };

    private WelcomeViewModel viewModel;
    private ActivityWelcomeBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(WelcomeViewModel.class);
        viewModel.getEvents().observe(this, eventObserver);
        binding = ActivityWelcomeBinding.inflate(getLayoutInflater());
        binding.loginButton.setOnClickListener(v -> viewModel.onLoginClicked());
        binding.signUpButton.setOnClickListener(v -> viewModel.onRegisterClicked());
        binding.continueWithoutLoginButton.setOnClickListener(v -> viewModel.onContinueWithoutLoginClicked());
        setContentView(binding.getRoot());
    }

    public void navigateToLogin() {
        startActivity(new Intent(this, LoginActivity.class));
    }

    public void navigateToRegistration() {
        startActivity(new Intent(this, RegistrationActivity.class));
    }

    public void navigateToUnregisteredUserScreen() {
        startActivity(new Intent(this, UnregisteredUserActivity.class));
    }
}