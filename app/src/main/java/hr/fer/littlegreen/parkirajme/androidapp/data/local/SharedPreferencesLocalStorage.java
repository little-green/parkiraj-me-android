package hr.fer.littlegreen.parkirajme.androidapp.data.local;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import dagger.hilt.android.qualifiers.ApplicationContext;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Administrator;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Company;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Person;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.User;

public final class SharedPreferencesLocalStorage implements LocalStorage {

    @NonNull
    private static final String KEY_SESSION_TOKEN = "KEY_SESSION_TOKEN";

    @NonNull
    private static final String KEY_SESSION_USER_TYPE = "KEY_SESSION_USER_TYPE";

    @NonNull
    private static final String KEY_SESSION_USER = "KEY_SESSION_USER";

    private static final int USER_TYPE_PERSON = 0;
    private static final int USER_TYPE_COMPANY = 1;
    private static final int USER_TYPE_ADMINISTRATOR = 2;
    private static final int USER_TYPE_UNKNOWN = -1;

    @NonNull
    private final SharedPreferences sharedPreferences;

    @NonNull
    private final Gson gson;

    @Inject
    public SharedPreferencesLocalStorage(@NonNull @ApplicationContext Context applicationContext, @NonNull Gson gson) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(applicationContext);
        this.gson = gson;
    }

    @Override
    public void putSession(@NonNull Session session) {
        sharedPreferences.edit()
            .putString(KEY_SESSION_TOKEN, session.getToken())
            .putInt(KEY_SESSION_USER_TYPE, getUserType(session.getUser()))
            .putString(KEY_SESSION_USER, gson.toJson(session.getUser()))
            .apply();
    }

    @Override
    public void clearSession() {
        sharedPreferences.edit()
            .remove(KEY_SESSION_TOKEN)
            .remove(KEY_SESSION_USER_TYPE)
            .remove(KEY_SESSION_USER)
            .apply();
    }

    @Nullable
    @Override
    public Session getSession() {
        String token = sharedPreferences.getString(KEY_SESSION_TOKEN, null);
        int userType = sharedPreferences.getInt(KEY_SESSION_USER_TYPE, USER_TYPE_UNKNOWN);
        String userJson = sharedPreferences.getString(KEY_SESSION_USER, null);
        if (token == null || userType == USER_TYPE_UNKNOWN || userJson == null) {
            return null;
        }
        switch (userType) {
            case USER_TYPE_PERSON:
                return new Session(token, gson.fromJson(userJson, Person.class));
            case USER_TYPE_COMPANY:
                return new Session(token, gson.fromJson(userJson, Company.class));
            case USER_TYPE_ADMINISTRATOR:
                return new Session(token, gson.fromJson(userJson, Administrator.class));
            default:
                return null;
        }
    }

    private int getUserType(User user) {
        if (user instanceof Person) {
            return USER_TYPE_PERSON;
        } else if (user instanceof Company) {
            return USER_TYPE_COMPANY;
        } else if (user instanceof Administrator) {
            return USER_TYPE_ADMINISTRATOR;
        } else {
            return USER_TYPE_UNKNOWN;
        }
    }
}
