package hr.fer.littlegreen.parkirajme.androidapp.ui.company.main.state;

public enum CompanyScreenType {
    COMPANY_SCREEN_PARKING_OBJECTS,
    COMPANY_SCREEN_PERSONAL_DETAILS
}
