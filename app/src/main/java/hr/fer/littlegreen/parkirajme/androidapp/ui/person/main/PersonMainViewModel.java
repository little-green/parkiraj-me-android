package hr.fer.littlegreen.parkirajme.androidapp.ui.person.main;

import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Person;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseViewModel;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.main.state.PersonScreenType;

public final class PersonMainViewModel extends BaseViewModel<BaseEvent> {

    @NonNull
    private final MutableLiveData<PersonScreenType> personScreenType = new MutableLiveData<>(PersonScreenType.MAP);

    private Person person;

    @ViewModelInject
    public PersonMainViewModel() {
    }

    public void initialize(@NonNull Person person) {
        this.person = person;
    }

    public void onPersonScreenSelected(@NonNull PersonScreenType personScreenType) {
        this.personScreenType.setValue(personScreenType);
    }

    @NonNull
    public LiveData<PersonScreenType> getPersonScreenType() {
        return personScreenType;
    }
}
