package hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.companyuserinformation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import dagger.hilt.android.AndroidEntryPoint;
import hr.fer.littlegreen.parkirajme.androidapp.data.local.LocalStorage;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.ActivityCompanyUserInformationBinding;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.FragmentCompanyInformationBinding;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.ActivityCompanyUserInformationBinding;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Administrator;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Company;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.User;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseFragment;
import androidx.annotation.NonNull;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.companyuserinformation.events.CompanyAccountDeletedEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.companyuserinformation.events.CompanyUserInformationEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.main.AdministratorMainActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.information.CompanyInformationViewModel;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.information.events.AccountDeletedEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.information.events.CompanyInformationEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.information.events.LogoutEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.information.state.CompanyInformationDialogType;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.TextViewUtil;
import hr.fer.littlegreen.parkirajme.androidapp.ui.welcome.WelcomeActivity;

@AndroidEntryPoint
public class CompanyUserInformationActivity extends BaseActivity {

    @NonNull
    private static final String EXTRA_COMPANY = "EXTRA_COMPANY";

    private CompanyUserInformationViewModel viewModel;
    private ActivityCompanyUserInformationBinding binding;

    @Inject
    LocalStorage localStorage;

    @NonNull
    private final Observer<CompanyUserInformationEvent> eventObserver = event -> {
        if (event instanceof CompanyAccountDeletedEvent) {
            startActivity(AdministratorMainActivity.newIntent(this, (Administrator) localStorage.getSession().getUser() ));
        }
    };

    @NonNull
    private final Observer<Company> companyObserver = company -> {
        TextViewUtil.update(binding.companyInformationOibFieldValue, company.getOib());
        TextViewUtil.update(binding.companyInformationNameFieldValue, company.getName());
        TextViewUtil.update(binding.companyInformationAddressFieldValue, company.getHeadquarterAddress());
        TextViewUtil.update(binding.companyInformationEmailFieldValue, company.getEmail());
    };

    @Nullable
    private AlertDialog dialog;

    @NonNull
    private final Observer<CompanyInformationDialogType> dialogTypeObserver = dialogType -> {
        if (dialogType == null) {
            if (dialog != null) {
                dialog.dismiss();
            }
            dialog = null;
        } else if (dialogType == CompanyInformationDialogType.DELETE_ACCOUNT_CONFIRMATION) {
            dialog = new AlertDialog.Builder(CompanyUserInformationActivity.this)
                .setTitle("Brisanje korisničkog računa")
                .setMessage("Jeste li sigurni da želite izbrisati korisnički račun? Ova se radnja ne može poništiti.")
                .setPositiveButton("Izrbiši korisnički račun", (dialog, which) -> viewModel.deleteAccount())
                .setNegativeButton("Odustani", (dialog, which) -> viewModel.dismissDialog())
                .setOnCancelListener(dialog -> viewModel.dismissDialog())
                .setOnDismissListener(dialog -> viewModel.dismissDialog())
                .create();
            dialog.show();
        } else if (dialogType == CompanyInformationDialogType.ERROR) {
            dialog = new AlertDialog.Builder(CompanyUserInformationActivity.this)
                .setTitle("Greška")
                .setMessage("Došlo je do neočekivane pogreške. Pokušajte ponovo.")
                .setPositiveButton("U redu", (dialog, which) -> viewModel.dismissDialog())
                .setOnCancelListener(dialog -> viewModel.dismissDialog())
                .setOnDismissListener(dialog -> viewModel.dismissDialog())
                .create();
            dialog.show();
        }
    };

    @Nullable
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(CompanyUserInformationViewModel.class);
        viewModel.getCompany().observe(this, companyObserver);
        viewModel.initialize((Company)getIntent().getSerializableExtra(EXTRA_COMPANY));
        binding = ActivityCompanyUserInformationBinding.inflate(getLayoutInflater());
        binding.companyUserInformationBackButton.setOnClickListener(v -> finish());
        binding.companyInformationDeleteAccountButton.setOnClickListener(v -> viewModel.onDeleteAccountClicked());
        viewModel.getDialogType().observe(this, dialogTypeObserver);
        viewModel.getEvents().observe(this, eventObserver);
        viewModel.initialize(viewModel.getCompany().getValue());
        setContentView(binding.getRoot());
    }

    public static Intent newIntent(@NonNull Context context, @NonNull Company company) {
        return new Intent(context, CompanyUserInformationActivity.class)
            .putExtra(EXTRA_COMPANY, company);
    }
}
