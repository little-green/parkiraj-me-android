package hr.fer.littlegreen.parkirajme.androidapp.data.http.models.editperson;

import java.time.YearMonth;
import java.util.Objects;

import androidx.annotation.NonNull;

public final class EditPersonRequest {

    @NonNull
    private final String firstName;

    @NonNull
    private final String lastName;

    @NonNull
    private final String creditCardNumber;

    @NonNull
    private final YearMonth creditCardExpirationDate;

    public EditPersonRequest(
        @NonNull String firstName,
        @NonNull String lastName,
        @NonNull String creditCardNumber,
        @NonNull YearMonth creditCardExpirationDate
    ) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.creditCardNumber = creditCardNumber;
        this.creditCardExpirationDate = creditCardExpirationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EditPersonRequest that = (EditPersonRequest) o;
        return firstName.equals(that.firstName) &&
            lastName.equals(that.lastName) &&
            creditCardNumber.equals(that.creditCardNumber) &&
            creditCardExpirationDate.equals(that.creditCardExpirationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, creditCardNumber, creditCardExpirationDate);
    }

    @NonNull
    public String getFirstName() {
        return firstName;
    }

    @NonNull
    public String getLastName() {
        return lastName;
    }

    @NonNull
    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    @NonNull
    public YearMonth getCreditCardExpirationDate() {
        return creditCardExpirationDate;
    }
}
