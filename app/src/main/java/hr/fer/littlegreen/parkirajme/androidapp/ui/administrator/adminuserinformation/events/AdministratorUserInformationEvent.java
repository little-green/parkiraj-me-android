package hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.adminuserinformation.events;

import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseEvent;

public interface AdministratorUserInformationEvent extends BaseEvent {
}
