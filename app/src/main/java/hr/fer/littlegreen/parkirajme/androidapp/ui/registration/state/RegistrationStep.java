package hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state;

import java.util.Objects;

import androidx.annotation.NonNull;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.stepmanagement.SingleStep;

public final class RegistrationStep extends SingleStep {

    @NonNull
    private final RegistrationStepType type;

    public RegistrationStep(@NonNull RegistrationStepType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RegistrationStep that = (RegistrationStep) o;
        return type == that.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(type);
    }

    @NonNull
    public RegistrationStepType getType() {
        return type;
    }
}
