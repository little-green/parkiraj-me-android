package hr.fer.littlegreen.parkirajme.androidapp.data.http.models.parkingobject;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public final class ParkingObjectEditRequest {

    @NotNull
    private final int capacity;

    @NotNull
    private final int price;

    @NotNull
    private final int freeSlots;

    public ParkingObjectEditRequest(
        @NotNull int capacity,
        @NotNull int price,
        @NotNull int freeSlots
    ) {
        this.capacity = capacity;
        this.price = price;
        this.freeSlots = freeSlots;
    }

    public int getCapacity() {
        return capacity;
    }

    public int getPrice() {
        return price;
    }

    public int getFreeSlots() {
        return freeSlots;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ParkingObjectEditRequest that = (ParkingObjectEditRequest) o;
        return capacity == that.capacity &&
            price == that.price &&
            freeSlots == that.freeSlots;
    }

    @Override
    public int hashCode() {
        return Objects.hash(capacity, price, freeSlots);
    }
}
