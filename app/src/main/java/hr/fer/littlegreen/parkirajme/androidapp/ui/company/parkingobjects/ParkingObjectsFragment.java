package hr.fer.littlegreen.parkirajme.androidapp.ui.company.parkingobjects;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import dagger.hilt.android.AndroidEntryPoint;
import hr.fer.littlegreen.parkirajme.androidapp.R;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.FragmentParkingObjectsBinding;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.ParkingObject;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseFragment;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.parkingobjects.adapter.ParkingObjectAdapter;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.parkingobjects.details.ParkingObjectDetailsActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.parkingobjects.events.FailedLoadParkingObjectsEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.parkingobjects.events.ParkingObjectEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.parkingobjects.events.ParkingObjectSelectedEvent;

@AndroidEntryPoint
public class ParkingObjectsFragment extends BaseFragment {

    @NonNull
    private final Observer<ParkingObjectEvent> eventObserver = event -> {
        if (event instanceof ParkingObjectSelectedEvent) {
            startActivity(ParkingObjectDetailsActivity.newIntent(
                requireContext(),
                ((ParkingObjectSelectedEvent) event).getParkingObject()
            ));
        } else if (event instanceof FailedLoadParkingObjectsEvent) {
            Toast.makeText(getContext(), getResources().getString(R.string.parking_object_load_list_error), Toast.LENGTH_LONG).show();
        }
    };

    private ParkingObjectAdapter adapter;

    private ParkingObjectsViewModel viewModel;
    private FragmentParkingObjectsBinding binding;

    @NonNull
    private final Observer<List<ParkingObject>> parkingObjectObserver = parkingObjects -> {
        adapter.submitList(parkingObjects);
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentParkingObjectsBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(this).get(ParkingObjectsViewModel.class);
        viewModel.getParkingObjects().observe(getViewLifecycleOwner(), parkingObjectObserver);
        viewModel.getEvents().observe(getViewLifecycleOwner(), eventObserver);
        viewModel.initialize();

        adapter = new ParkingObjectAdapter(viewModel::onParkingObjectSelected);
        binding.rvParkingObjects.setLayoutManager(new LinearLayoutManager(requireContext()));
        binding.rvParkingObjects.setAdapter(adapter);
        binding.addParkingObjectButton.setOnClickListener(v -> startActivity(NewParkingObjectActivity.newIntent(requireContext())));
        binding.refreshLayout.setOnRefreshListener(() -> {
            viewModel.initialize();
            binding.refreshLayout.setRefreshing(false);
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.initialize();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
