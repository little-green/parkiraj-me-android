package hr.fer.littlegreen.parkirajme.androidapp.domain.models;

import java.io.Serializable;
import java.util.Objects;

import androidx.annotation.NonNull;

public final class Vehicle implements Serializable {

    @NonNull
    private final String registrationNumber;

    public Vehicle(@NonNull String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Vehicle vehicle = (Vehicle) o;
        return registrationNumber.equals(vehicle.registrationNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(registrationNumber);
    }

    @NonNull
    public String getRegistrationNumber() {
        return registrationNumber;
    }
}
