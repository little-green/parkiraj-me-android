package hr.fer.littlegreen.parkirajme.androidapp.ui.registration;

import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.LiveData;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseViewModel;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.events.FinishEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.events.RegistrationEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state.RegistrationStepSequenceManager;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state.RegistrationStepType;

public final class RegistrationViewModel extends BaseViewModel<RegistrationEvent> {

    @NonNull
    private final RegistrationStepSequenceManager registrationStepSequenceManager;

    @ViewModelInject
    public RegistrationViewModel(@NonNull RegistrationStepSequenceManager registrationStepSequenceManager) {
        this.registrationStepSequenceManager = registrationStepSequenceManager;
    }

    public void onBackPressed() {
        if (registrationStepSequenceManager.isFirstStep()) {
            emitEvent(new FinishEvent());
        } else {
            registrationStepSequenceManager.iterateBackward();
        }
    }

    @NonNull
    public LiveData<RegistrationStepType> getRegistrationStepType() {
        return registrationStepSequenceManager.getRegistrationStepType();
    }

    @NonNull
    public LiveData<Double> getProgress() {
        return registrationStepSequenceManager.getProgress();
    }
}
