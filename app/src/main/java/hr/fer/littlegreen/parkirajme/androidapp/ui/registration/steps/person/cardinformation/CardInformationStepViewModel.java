package hr.fer.littlegreen.parkirajme.androidapp.ui.registration.steps.person.cardinformation;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.LiveData;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseViewModel;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state.CardInformationFormState;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state.RegistrationFlowState;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state.RegistrationStepSequenceManager;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.StringUtil;

public final class CardInformationStepViewModel extends BaseViewModel<BaseEvent> {

    @NonNull
    private final RegistrationStepSequenceManager registrationStepSequenceManager;

    @NonNull
    private final RegistrationFlowState registrationFlowState;

    @ViewModelInject
    public CardInformationStepViewModel(
        @NonNull RegistrationStepSequenceManager registrationStepSequenceManager,
        @NonNull RegistrationFlowState registrationFlowState
    ) {
        this.registrationStepSequenceManager = registrationStepSequenceManager;
        this.registrationFlowState = registrationFlowState;
    }

    public void onCardNumberChanged(String cardNumber) {
        CardInformationFormState cardInformationFormState = getOrCreateCardInformationFormState();
        registrationFlowState.cardInformationFormState.setValue(new CardInformationFormState(
            cardNumber,
            cardInformationFormState.getExpiryDate(),
            cardInformationFormState.getCvv()
        ));
    }

    public void onExpiryDateChanged(String expiryDate) {
        CardInformationFormState cardInformationFormState = getOrCreateCardInformationFormState();
        registrationFlowState.cardInformationFormState.setValue(new CardInformationFormState(
            cardInformationFormState.getCardNumber(),
            expiryDate,
            cardInformationFormState.getCvv()
        ));
    }

    public void onCvvChanged(String cvv) {
        CardInformationFormState cardInformationFormState = getOrCreateCardInformationFormState();
        registrationFlowState.cardInformationFormState.setValue(new CardInformationFormState(
            cardInformationFormState.getCardNumber(),
            cardInformationFormState.getExpiryDate(),
            cvv
        ));
    }

    public void onContinueClicked() {
        if (isValid(registrationFlowState.cardInformationFormState.getValue())) {
            registrationStepSequenceManager.iterateForward();
        }
    }

    @NonNull
    public LiveData<CardInformationFormState> getCardInformationFormState() {
        return registrationFlowState.cardInformationFormState;
    }

    @NonNull
    private CardInformationFormState getOrCreateCardInformationFormState() {
        CardInformationFormState cardInformationFormState = registrationFlowState.cardInformationFormState.getValue();
        if (cardInformationFormState != null) {
            return cardInformationFormState;
        } else {
            return new CardInformationFormState(null, null, null);
        }
    }

    private boolean isValid(@Nullable CardInformationFormState cardInformationFormState) {
        return cardInformationFormState != null && StringUtil.areNotNullOrEmpty(
            cardInformationFormState.getCardNumber(),
            cardInformationFormState.getExpiryDate(),
            cardInformationFormState.getCvv()
        );
    }
}
