package hr.fer.littlegreen.parkirajme.androidapp.ui.person.reservations.adapters;

import hr.fer.littlegreen.parkirajme.androidapp.domain.models.ReservationAndParkingObjectPair;

public interface ReservationClickListener {

    void onClick(ReservationAndParkingObjectPair reservation);
}
