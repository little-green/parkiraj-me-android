package hr.fer.littlegreen.parkirajme.androidapp.data.local;

import java.util.Objects;

import androidx.annotation.NonNull;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.User;

public final class Session {

    @NonNull
    private final String token;

    @NonNull
    private final User user;

    public Session(@NonNull String token, @NonNull User user) {
        this.token = token;
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Session session = (Session) o;
        return token.equals(session.token) &&
            user.equals(session.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(token, user);
    }

    @NonNull
    public String getToken() {
        return token;
    }

    @NonNull
    public User getUser() {
        return user;
    }
}
