package hr.fer.littlegreen.parkirajme.androidapp.ui.company.parkingobjects;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import dagger.hilt.android.AndroidEntryPoint;
import hr.fer.littlegreen.parkirajme.androidapp.R;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.ActivityNewParkingObjectBinding;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.parkingobjects.events.FailedAddNewParkingObjectEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.parkingobjects.events.FinishAddNewParkingObjectEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.parkingobjects.events.ParkingObjectEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.MoneyUtil;

@AndroidEntryPoint
public class NewParkingObjectActivity extends BaseActivity {

    private ActivityNewParkingObjectBinding binding;
    private NewParkingObjectViewModel viewModel;

    private final Observer<ParkingObjectEvent> eventObserver = event -> {
        if (event instanceof FinishAddNewParkingObjectEvent) {
            finish();
        } else if (event instanceof FailedAddNewParkingObjectEvent) {
            Toast.makeText(this, getResources().getString(R.string.parking_object_add_error), Toast.LENGTH_LONG).show();
        }
    };

    public static Intent newIntent(@NotNull Context context) {
        return new Intent(context, NewParkingObjectActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityNewParkingObjectBinding.inflate(getLayoutInflater());
        viewModel = new ViewModelProvider(this).get(NewParkingObjectViewModel.class);
        viewModel.getEvents().observe(this, eventObserver);
        binding.backButton.setOnClickListener(l -> finish());
        binding.finish.setOnClickListener(l -> onFinishAddingClicked());
        setContentView(binding.getRoot());
    }

    public void onFinishAddingClicked() {
        String objectName = binding.objectName.getText().toString().trim();
        String objectAddress = binding.objectAddress.getText().toString().trim();

        int thirtyMinPrice = MoneyUtil.parsePrice(binding.object30MinPrice.getText().toString().trim());
        if (thirtyMinPrice < 0) {
            Toast.makeText(this, getResources().getString(R.string.parking_object_add_error), Toast.LENGTH_LONG).show();
            return;
        }

        int capacity;
        try {
            capacity = Integer.parseInt(binding.objectCapacity.getText().toString().trim());
        } catch (NumberFormatException ex) {
            Toast.makeText(this, getResources().getString(R.string.parking_object_add_error), Toast.LENGTH_LONG).show();
            return;
        }

        Geocoder geocoder = new Geocoder(this);
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocationName(objectAddress, 10);
        } catch (IOException ex) {
            Toast.makeText(this, getResources().getString(R.string.parking_object_add_error), Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        if (addresses == null || addresses.size() == 0) {
            Toast.makeText(this, getResources().getString(R.string.parking_object_invalid_address), Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        BigDecimal latitude = null;
        BigDecimal longitude = null;
        boolean found = false;
        for (Address address : addresses) {
            latitude = new BigDecimal(address.getLatitude());
            longitude = new BigDecimal(address.getLongitude());

            if (latitude.compareTo(new BigDecimal(45.75)) >= 0 && latitude.compareTo(new BigDecimal(45.85)) <= 0) {
                if (longitude.compareTo(new BigDecimal(15.83)) >= 0 && longitude.compareTo(new BigDecimal(16.14)) <= 0) {
                    found = true;
                    break;
                }
            }
        }

        if (found == false || latitude == null || latitude == null) {
            Toast.makeText(this, getResources().getString(R.string.parking_object_invalid_address), Toast.LENGTH_LONG).show();
            return;
        }

        viewModel.addNewParkingObject(objectName, objectAddress, capacity, thirtyMinPrice, longitude, latitude);
    }
}
