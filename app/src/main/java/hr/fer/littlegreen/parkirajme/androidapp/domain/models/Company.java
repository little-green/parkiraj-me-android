package hr.fer.littlegreen.parkirajme.androidapp.domain.models;

import androidx.annotation.NonNull;

public final class Company extends User {

    @NonNull
    private String name;

    @NonNull
    private String headquarterAddress;

    public Company(
        @NonNull String userUuid,
        @NonNull String email,
        @NonNull String oib,
        @NonNull String name,
        @NonNull String headquarterAddress
    ) {
        super(userUuid, email, oib);
        this.name = name;
        this.headquarterAddress = headquarterAddress;
    }

    @NonNull
    public String getName() {
        return name;
    }

    @NonNull
    public String getHeadquarterAddress() {
        return headquarterAddress;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    public void setHeadquarterAddress(@NonNull String headquarterAddress) {
        this.headquarterAddress = headquarterAddress;
    }
}
