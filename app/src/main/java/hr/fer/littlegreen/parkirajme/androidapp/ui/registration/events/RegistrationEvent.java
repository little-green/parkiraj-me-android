package hr.fer.littlegreen.parkirajme.androidapp.ui.registration.events;

import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseEvent;

public interface RegistrationEvent extends BaseEvent {
}
