package hr.fer.littlegreen.parkirajme.androidapp.ui.company.parkingobjects.adapter;

import hr.fer.littlegreen.parkirajme.androidapp.domain.models.ParkingObject;

public interface ParkingObjectClickListener {

    void onClick(ParkingObject parkingObject);
}
