package hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.stepmanagement.StepSequenceManager;

public interface RegistrationStepSequenceManager extends StepSequenceManager {

    @NonNull
    LiveData<RegistrationStepType> getRegistrationStepType();

    boolean isFirstStep();

    void activatePersonStepSequence();

    void activateBusinessStepSequence();
}
