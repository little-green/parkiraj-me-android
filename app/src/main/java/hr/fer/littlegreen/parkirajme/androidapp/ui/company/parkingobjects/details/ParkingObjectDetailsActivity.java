package hr.fer.littlegreen.parkirajme.androidapp.ui.company.parkingobjects.details;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import dagger.hilt.android.AndroidEntryPoint;
import hr.fer.littlegreen.parkirajme.androidapp.R;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.ActivityParkingObjectDetailsBinding;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.ParkingObject;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.parkingobjects.events.EditFailedEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.parkingobjects.events.FailedDeleteParkingObjectEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.parkingobjects.events.FinishDeleteParkingObjectEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.parkingobjects.events.ParkingObjectEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.MoneyUtil;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.TextViewUtil;

@AndroidEntryPoint
public class ParkingObjectDetailsActivity extends BaseActivity {

    @NonNull
    private static final String EXTRA_PARKING_OBJECT = "EXTRA_PARKING_OBJECT";

    @Nullable
    private AlertDialog editCapacityDialog;

    @Nullable
    private AlertDialog editThirtyMinutePriceDialog;

    private ParkingObjectDetailsViewModel viewModel;
    private ActivityParkingObjectDetailsBinding binding;

    @NotNull
    private final Observer<ParkingObject> parkingObjectObserver = parkingObject -> {
        TextViewUtil.update(binding.parkingObjectNameTitle, parkingObject.getName());
        TextViewUtil.update(binding.adressValue, parkingObject.getAddress());
        TextViewUtil.update(binding.freeSlotsValue, String.valueOf(parkingObject.getFreeSlots()));
        TextViewUtil.update(binding.capacityValue, String.valueOf(parkingObject.getCapacity()));
        TextViewUtil.update(binding.thirtyMinPriceValue, formatPrice(parkingObject));
    };

    private final Observer<ParkingObjectEvent> eventObserver = event -> {
        if (event instanceof FailedDeleteParkingObjectEvent) {
            Toast.makeText(this, getResources().getString(R.string.parking_object_add_error), Toast.LENGTH_LONG).show();
        } else if (event instanceof FinishDeleteParkingObjectEvent) {
            finish();
        } else if (event instanceof EditFailedEvent) {
            Toast.makeText(this, getResources().getString(R.string.edit_failed_message), Toast.LENGTH_LONG).show();
        }
    };

    @NotNull
    public static Intent newIntent(@NotNull Context context, @NotNull ParkingObject parkingObject) {
        return new Intent(context, ParkingObjectDetailsActivity.class)
            .putExtra(EXTRA_PARKING_OBJECT, parkingObject);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(ParkingObjectDetailsViewModel.class);
        viewModel.getParkingObject().observe(this, parkingObjectObserver);
        viewModel.initialize((ParkingObject) getIntent().getSerializableExtra(EXTRA_PARKING_OBJECT));
        viewModel.getEvents().observe(this, eventObserver);
        binding = ActivityParkingObjectDetailsBinding.inflate(getLayoutInflater());
        binding.parkingObjectDetailsBackButton.setOnClickListener(v -> finish());
        binding.deleteParkingObject.setOnClickListener(v -> viewModel.deleteParkingObject());

        binding.capacityTitle.setOnClickListener(v -> {
            editCapacityDialog =
                new AlertDialog.Builder(this)
                    .setTitle(getResources().getString(R.string.edit_title))
                    .setPositiveButton(getResources().getString(R.string.edit_finish), (dialog, which) -> {
                        EditText editText = (EditText) editCapacityDialog.findViewById(R.id.newValue);
                        int newCapacity;
                        try {
                            newCapacity = Integer.parseInt(editText.getText().toString());
                        } catch (NumberFormatException ex) {
                            Toast.makeText(this, getResources().getString(R.string.parking_object_add_error), Toast.LENGTH_LONG).show();
                            return;
                        }
                        editCapacityDialog.dismiss();
                        viewModel.editCapacity(newCapacity);
                    })
                    .setNegativeButton(getResources().getString(R.string.edit_cancel), (dialog, which) -> editCapacityDialog.dismiss())
                    .setOnCancelListener(dialog -> editCapacityDialog.dismiss())
                    .setOnDismissListener(dialog -> editCapacityDialog.dismiss())
                    .setView(R.layout.dialog_edit_numeric)
                    .create();
            editCapacityDialog.show();
        });

        binding.thirtyMinPriceTitle.setOnClickListener(v -> {
            editThirtyMinutePriceDialog =
                new AlertDialog.Builder(this)
                    .setTitle(getResources().getString(R.string.edit_title))
                    .setPositiveButton(getResources().getString(R.string.edit_finish), (dialog, which) -> {
                        EditText editText = (EditText) editThirtyMinutePriceDialog.findViewById(R.id.newValue);
                        int thirtyMinPrice = MoneyUtil.parsePrice(editText.getText().toString().trim());
                        if (thirtyMinPrice < 0) {
                            Toast.makeText(this, getResources().getString(R.string.parking_object_add_error), Toast.LENGTH_LONG).show();
                            return;
                        }
                        editThirtyMinutePriceDialog.dismiss();
                        viewModel.editThirtyMinutePrice(thirtyMinPrice);
                    })
                    .setNegativeButton(
                        getResources().getString(R.string.edit_cancel),
                        (dialog, which) -> editThirtyMinutePriceDialog.dismiss()
                    )
                    .setOnCancelListener(dialog -> editThirtyMinutePriceDialog.dismiss())
                    .setOnDismissListener(dialog -> editThirtyMinutePriceDialog.dismiss())
                    .setView(R.layout.dialog_edit_numeric)
                    .create();
            editThirtyMinutePriceDialog.show();
        });

        setContentView(binding.getRoot());
    }

    @SuppressLint("DefaultLocale")
    @NonNull
    private String formatPrice(@NonNull ParkingObject parkingObject) {
        return String.format(
            getString(R.string.parking_object_details_price_format),
            String.format(
                "%d,%02d",
                parkingObject.getThirtyMinutePrice() / 100,
                parkingObject.getThirtyMinutePrice() % 100
            )
        );
    }
}
