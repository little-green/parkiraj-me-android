package hr.fer.littlegreen.parkirajme.androidapp.ui.person.editvehicles;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import dagger.hilt.android.AndroidEntryPoint;
import hr.fer.littlegreen.parkirajme.androidapp.R;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.ActivityEditVehiclesBinding;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Vehicle;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.editvehicles.adapters.EditableVehicleItemAdapter;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.editvehicles.events.EditVehiclesEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.editvehicles.events.ErrorEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.editvehicles.state.EditableVehicleItem;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.ViewUtil;

@AndroidEntryPoint
public final class EditVehiclesActivity extends BaseActivity {

    @NonNull
    private final Observer<EditVehiclesEvent> eventObserver = event -> {
        if (event instanceof ErrorEvent) {
            String message = ((ErrorEvent) event).getMessage();
            if (message == null) {
                message = getString(R.string.generic_error);
            }
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    };
    private EditVehiclesViewModel viewModel;
    private ActivityEditVehiclesBinding binding;
    @NonNull
    private final Observer<Boolean> isActionInProgressObserver = isActionInProgress -> {
        ViewUtil.updateVisibility(binding.editVehiclesProgressBar, isActionInProgress);
    };
    private EditableVehicleItemAdapter adapter;
    @NonNull
    private final Observer<List<EditableVehicleItem>> editableVehicleItemsObserver = editableVehicleItems -> {
        adapter.submitList(editableVehicleItems);
    };
    @Nullable
    private AlertDialog addVehicleDialog = null;

    @NonNull
    public static Intent newIntent(@NonNull Context context) {
        return new Intent(context, EditVehiclesActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(EditVehiclesViewModel.class);
        viewModel.getEvents().observe(this, eventObserver);
        viewModel.getEditableVehicleItems().observe(this, editableVehicleItemsObserver);
        viewModel.isActionInProgress().observe(this, isActionInProgressObserver);
        adapter = new EditableVehicleItemAdapter(viewModel::deleteVehicle);
        binding = ActivityEditVehiclesBinding.inflate(getLayoutInflater());
        binding.editVehiclesCloseButton.setOnClickListener(v -> {
            setResult(RESULT_OK);
            finish();
        });
        binding.editVehiclesList.setLayoutManager(new LinearLayoutManager(this));
        binding.editVehiclesList.setAdapter(adapter);
        binding.editVehiclesAddButton.setOnClickListener(v -> showAddVehicleDialog());
        setContentView(binding.getRoot());
        viewModel.initialize();
    }

    private void showAddVehicleDialog() {
        addVehicleDialog = new AlertDialog.Builder(this)
            .setTitle(getString(R.string.add_vehicle_title))
            .setPositiveButton(getString(R.string.edit_finish), (dialog, which) -> {
                EditText editText = (EditText) addVehicleDialog.findViewById(R.id.registrationNumberField);
                viewModel.createVehicle(new Vehicle(editText.getText().toString()));
            })
            .setView(R.layout.dialog_add_vehicle)
            .create();
        addVehicleDialog.show();
    }
}
