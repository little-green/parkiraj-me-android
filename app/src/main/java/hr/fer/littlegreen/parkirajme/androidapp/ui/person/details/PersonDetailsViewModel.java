package hr.fer.littlegreen.parkirajme.androidapp.ui.person.details;

import java.time.YearMonth;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.ParkirajMeService;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.models.editperson.EditPersonRequest;
import hr.fer.littlegreen.parkirajme.androidapp.data.local.LocalStorage;
import hr.fer.littlegreen.parkirajme.androidapp.data.local.Session;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Person;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseViewModel;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.details.events.AccountDeletedEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.details.events.ErrorEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.details.events.LogoutEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.details.events.PersonDetailsEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.details.state.PersonDetailsDialogType;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.ErrorExtractor;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.schedulers.Schedulers;

public final class PersonDetailsViewModel extends BaseViewModel<PersonDetailsEvent> {

    @NonNull
    private final ParkirajMeService parkirajMeService;

    @NonNull
    private final LocalStorage localStorage;

    @NonNull
    private final ErrorExtractor errorExtractor;

    @NonNull
    private final MutableLiveData<Person> person = new MutableLiveData<>();

    @NonNull
    private final MutableLiveData<PersonDetailsDialogType> dialogType = new MutableLiveData<>();

    @ViewModelInject
    public PersonDetailsViewModel(
        @NonNull ParkirajMeService parkirajMeService,
        @NonNull LocalStorage localStorage,
        @NonNull ErrorExtractor errorExtractor
    ) {
        this.parkirajMeService = parkirajMeService;
        this.localStorage = localStorage;
        this.errorExtractor = errorExtractor;
    }

    public void initialize() {
        person.setValue((Person) localStorage.getSession().getUser());
    }

    public void onLogoutClicked() {
        dialogType.setValue(PersonDetailsDialogType.LOGOUT_CONFIRMATION);
    }

    public void onDeleteAccountClicked() {
        dialogType.setValue(PersonDetailsDialogType.DELETE_ACCOUNT_CONFIRMATION);
    }

    public void dismissDialog() {
        dialogType.setValue(null);
    }

    public void editFirstName(@NonNull String firstName) {
        if (firstName.isEmpty()) {
            emitEvent(new ErrorEvent(null));
            return;
        }
        Person currentPerson = (Person) localStorage.getSession().getUser();
        Person newPerson = new Person(
            currentPerson.getUserUuid(),
            currentPerson.getEmail(),
            currentPerson.getOib(),
            firstName,
            currentPerson.getLastName(),
            currentPerson.getCreditCardNumber(),
            currentPerson.getCreditCardExpirationDate(),
            currentPerson.getVehicles()
        );
        editPerson(newPerson);
    }

    public void editLastName(@NonNull String lastName) {
        if (lastName.isEmpty()) {
            emitEvent(new ErrorEvent(null));
            return;
        }
        Person currentPerson = (Person) localStorage.getSession().getUser();
        Person newPerson = new Person(
            currentPerson.getUserUuid(),
            currentPerson.getEmail(),
            currentPerson.getOib(),
            currentPerson.getFirstName(),
            lastName,
            currentPerson.getCreditCardNumber(),
            currentPerson.getCreditCardExpirationDate(),
            currentPerson.getVehicles()
        );
        editPerson(newPerson);
    }

    public void editCreditCardInformation(@NonNull String creditCardNumber, @Nullable YearMonth creditCardExpirationDate) {
        if (creditCardNumber.isEmpty() || creditCardExpirationDate == null) {
            emitEvent(new ErrorEvent(null));
            return;
        }
        Person currentPerson = (Person) localStorage.getSession().getUser();
        Person newPerson = new Person(
            currentPerson.getUserUuid(),
            currentPerson.getEmail(),
            currentPerson.getOib(),
            currentPerson.getFirstName(),
            currentPerson.getLastName(),
            creditCardNumber,
            creditCardExpirationDate,
            currentPerson.getVehicles()
        );
        editPerson(newPerson);
    }

    public void logout() {
        dialogType.setValue(null);
        disposable.add(
            parkirajMeService.logout()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    localStorage.clearSession();
                    emitEvent(new LogoutEvent());
                }, throwable -> dialogType.setValue(PersonDetailsDialogType.ERROR))
        );
    }

    public void deleteAccount() {
        dialogType.setValue(null);
        disposable.add(
            parkirajMeService.deleteAccount(person.getValue().getUserUuid())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    localStorage.clearSession();
                    emitEvent(new AccountDeletedEvent());
                }, throwable -> dialogType.setValue(PersonDetailsDialogType.ERROR))
        );
    }

    @NonNull
    public LiveData<Person> getPerson() {
        return person;
    }

    @NonNull
    public LiveData<PersonDetailsDialogType> getDialogType() {
        return dialogType;
    }

    private void editPerson(@NonNull Person person) {
        EditPersonRequest editPersonRequest = new EditPersonRequest(
            person.getFirstName(),
            person.getLastName(),
            person.getCreditCardNumber(),
            person.getCreditCardExpirationDate()
        );
        disposable.add(
            parkirajMeService.editPerson(person.getUserUuid(), editPersonRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    localStorage.putSession(new Session(localStorage.getSession().getToken(), person));
                    this.person.setValue(person);
                }, throwable -> emitEvent(new ErrorEvent(errorExtractor.extract(throwable))))
        );
    }
}
