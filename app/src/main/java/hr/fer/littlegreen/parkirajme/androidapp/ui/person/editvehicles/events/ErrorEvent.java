package hr.fer.littlegreen.parkirajme.androidapp.ui.person.editvehicles.events;

import java.util.Objects;

import androidx.annotation.Nullable;

public final class ErrorEvent implements EditVehiclesEvent {

    @Nullable
    private final String message;

    public ErrorEvent(@Nullable String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ErrorEvent that = (ErrorEvent) o;
        return Objects.equals(message, that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(message);
    }

    @Nullable
    public String getMessage() {
        return message;
    }
}
