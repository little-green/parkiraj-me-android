package hr.fer.littlegreen.parkirajme.androidapp.ui.login;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import dagger.hilt.android.AndroidEntryPoint;
import hr.fer.littlegreen.parkirajme.androidapp.R;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.ActivityLoginBinding;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.main.AdministratorMainActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.main.CompanyMainActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.login.events.AdministratorLoginSuccessEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.login.events.CompanyLoginSuccessEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.login.events.LoginEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.login.events.LoginFailedEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.login.events.PersonLoginSuccessEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.login.state.LoginFormState;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.main.PersonMainActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.TextViewUtil;

@AndroidEntryPoint
public final class LoginActivity extends BaseActivity {

    private LoginViewModel viewModel;
    private ActivityLoginBinding binding;

    @NonNull
    private final Observer<LoginFormState> loginFormStateObserver = loginFormState -> {
        if (loginFormState == null) {
            return;
        }
        TextViewUtil.update(binding.email, loginFormState.getEmail());
        TextViewUtil.update(binding.password, loginFormState.getPassword());
    };

    @NonNull
    private final Observer<Boolean> isProgressShownObserver = isProgressShown -> {
        if (isProgressShown) {
            binding.loading.setVisibility(View.VISIBLE);
        } else {
            binding.loading.setVisibility(View.GONE);
        }
    };

    @NonNull
    private final Observer<LoginEvent> eventObserver = event -> {
        if (event instanceof PersonLoginSuccessEvent) {
            startActivity(PersonMainActivity.newIntent(this, ((PersonLoginSuccessEvent) event).getPerson()));
            finish();
        } else if (event instanceof CompanyLoginSuccessEvent) {
            startActivity(CompanyMainActivity.newIntent(this, ((CompanyLoginSuccessEvent) event).getCompany()));
            finish();
        } else if (event instanceof AdministratorLoginSuccessEvent) {
            startActivity(AdministratorMainActivity.newIntent(this, ((AdministratorLoginSuccessEvent) event).getAdministrator()));
            finish();
        } else if (event instanceof LoginFailedEvent) {
            String message = ((LoginFailedEvent) event).getMessage();
            if (message == null) {
                message = getString(R.string.login_failed);
            }
            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        viewModel.getLoginFormState().observe(this, loginFormStateObserver);
        viewModel.isProgressShown().observe(this, isProgressShownObserver);
        viewModel.getEvents().observe(this, eventObserver);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        binding.loginBackArrow.setOnClickListener(v -> onBackPressed());
        TextViewUtil.addTextChangedListener(binding.email, viewModel::onEmailChanged);
        TextViewUtil.addTextChangedListener(binding.password, viewModel::onPasswordChanged);
        binding.login.setOnClickListener(v -> {
            hideKeyboard();
            viewModel.login();
        });
        setContentView(binding.getRoot());
    }
}
