package hr.fer.littlegreen.parkirajme.androidapp.ui.registration.steps.usertype.state;

public enum UserType {

    PERSON,
    COMPANY
}
