package hr.fer.littlegreen.parkirajme.androidapp.ui.registration.steps.usertype;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import dagger.hilt.android.AndroidEntryPoint;
import hr.fer.littlegreen.parkirajme.androidapp.R;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.FragmentUserTypeStepBinding;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseFragment;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.steps.usertype.state.UserType;

@AndroidEntryPoint
public final class UserTypeStepFragment extends BaseFragment {

    private UserTypeStepViewModel viewModel;
    private FragmentUserTypeStepBinding binding;

    private final Observer<UserType> userTypeObserver = userType -> {
        switch (userType) {
            case PERSON:
                stylePersonOptionAsSelected();
                styleBusinessOptionAsUnselected();
                break;
            case COMPANY:
                stylePersonOptionAsUnselected();
                styleBusinessOptionAsSelected();
                break;
            default:
                stylePersonOptionAsUnselected();
                styleBusinessOptionAsUnselected();
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentUserTypeStepBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(this).get(UserTypeStepViewModel.class);
        viewModel.getUserType().observe(getViewLifecycleOwner(), userTypeObserver);
        binding.personOptionContainer.setOnClickListener(v -> viewModel.selectPersonOption());
        binding.businessOptionContainer.setOnClickListener(v -> viewModel.selectBusinessOption());
        binding.userTypeStepContinueButton.setOnClickListener(v -> viewModel.onContinueClicked());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    private void stylePersonOptionAsSelected() {
        binding.personOptionContainer.setBackgroundResource(R.drawable.gradient_secondary);
        binding.personOptionIcon.setColorFilter(getColorOnSecondary());
        binding.personOptionLabel.setTextColor(getColorOnSecondary());
        binding.personOptionRadioButton.setImageResource(R.drawable.ic_check_circle_black_24dp);
        binding.personOptionRadioButton.setColorFilter(getColorOnSecondary());
    }

    private void stylePersonOptionAsUnselected() {
        binding.personOptionContainer.setBackground(null);
        binding.personOptionIcon.setColorFilter(getColorOnPrimary());
        binding.personOptionLabel.setTextColor(getColorOnPrimary());
        binding.personOptionRadioButton.setImageResource(R.drawable.circle_outline_on_primary);
        binding.personOptionRadioButton.clearColorFilter();
    }

    private void styleBusinessOptionAsSelected() {
        binding.businessOptionContainer.setBackgroundResource(R.drawable.gradient_secondary);
        binding.businessOptionIcon.setColorFilter(getColorOnSecondary());
        binding.businessOptionLabel.setTextColor(getColorOnSecondary());
        binding.businessOptionRadioButton.setImageResource(R.drawable.ic_check_circle_black_24dp);
        binding.businessOptionRadioButton.setColorFilter(getColorOnSecondary());
    }

    private void styleBusinessOptionAsUnselected() {
        binding.businessOptionContainer.setBackground(null);
        binding.businessOptionIcon.setColorFilter(getColorOnPrimary());
        binding.businessOptionLabel.setTextColor(getColorOnPrimary());
        binding.businessOptionRadioButton.setImageResource(R.drawable.circle_outline_on_primary);
        binding.businessOptionRadioButton.clearColorFilter();
    }
}
