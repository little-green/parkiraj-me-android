package hr.fer.littlegreen.parkirajme.androidapp.ui;

import androidx.annotation.ColorInt;
import androidx.fragment.app.Fragment;

public abstract class BaseFragment extends Fragment {

    @ColorInt
    public int getColorPrimary() {
        return ((BaseActivity) requireActivity()).getColorPrimary();
    }

    @ColorInt
    public int getColorOnPrimary() {
        return ((BaseActivity) requireActivity()).getColorOnPrimary();
    }

    @ColorInt
    public int getColorSecondary() {
        return ((BaseActivity) requireActivity()).getColorSecondary();
    }

    @ColorInt
    public int getColorOnSecondary() {
        return ((BaseActivity) requireActivity()).getColorOnSecondary();
    }

    public void hideKeyboard() {
        ((BaseActivity) requireActivity()).hideKeyboardFrom(getView().getRootView());
    }
}
