package hr.fer.littlegreen.parkirajme.androidapp.ui.person.createreservation.state;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;

public final class CreateReservationChooseTimeDialog implements CreateReservationDialog {

    @StringRes
    private final int title;

    private final int hour;

    private final int minute;

    @NonNull
    private final Listener listener;

    public CreateReservationChooseTimeDialog(
        int title,
        int hour,
        int minute,
        @NonNull Listener listener
    ) {
        this.title = title;
        this.hour = hour;
        this.minute = minute;
        this.listener = listener;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CreateReservationChooseTimeDialog that = (CreateReservationChooseTimeDialog) o;
        return title == that.title &&
            hour == that.hour &&
            minute == that.minute &&
            listener.equals(that.listener);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, hour, minute, listener);
    }

    public int getTitle() {
        return title;
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    @NonNull
    public Listener getListener() {
        return listener;
    }

    public interface Listener {

        void onTimeSelected(int hour, int minute);
    }
}
