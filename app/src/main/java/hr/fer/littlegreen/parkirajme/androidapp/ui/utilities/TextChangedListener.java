package hr.fer.littlegreen.parkirajme.androidapp.ui.utilities;

public interface TextChangedListener {

    void onTextChanged(String text);
}
