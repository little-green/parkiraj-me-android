package hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.adminuserinformation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import dagger.hilt.android.AndroidEntryPoint;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.ActivityAdministratorUserInformationBinding;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.ActivityPersonUserInformationBinding;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Administrator;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Person;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.User;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseFragment;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.personuserdetails.PersonUserInformationViewModel;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.TextViewUtil;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.annotations.Nullable;

@AndroidEntryPoint
public class AdministratorUserInformationActivity extends BaseActivity {

    private AdministratorUserInformationViewModel viewModel;
    private ActivityAdministratorUserInformationBinding binding;
    private static final String EXTRA_ADMINISTRATOR = "EXTRA_ADMINISTRATOR";

    @NonNull
    private final Observer<Administrator> administratorObserver = administrator ->{
        TextViewUtil.update(binding.administratorUserInformationEmailFieldValue, administrator.getEmail());
    };

    public static Intent newIntent(@NonNull Context context,@NonNull Administrator administrator) {
        return new Intent(context, AdministratorUserInformationActivity.class)
            .putExtra(EXTRA_ADMINISTRATOR, administrator);
    }

    @Nullable
    @Override
    public void onCreate(@androidx.annotation.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(AdministratorUserInformationViewModel.class);
        viewModel.getAdministrator().observe(this, administratorObserver);
        viewModel.initialize((Administrator)getIntent().getSerializableExtra(EXTRA_ADMINISTRATOR));
        binding = ActivityAdministratorUserInformationBinding.inflate(getLayoutInflater());
        binding.administratorUserInformationBackButton.setOnClickListener(v -> finish());
        viewModel.initialize(viewModel.getAdministrator().getValue());
        setContentView(binding.getRoot());
    }
}
