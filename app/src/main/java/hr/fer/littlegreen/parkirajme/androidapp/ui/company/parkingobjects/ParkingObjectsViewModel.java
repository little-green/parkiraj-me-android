package hr.fer.littlegreen.parkirajme.androidapp.ui.company.parkingobjects;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.ParkirajMeService;
import hr.fer.littlegreen.parkirajme.androidapp.data.local.LocalStorage;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.ParkingObject;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseViewModel;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.parkingobjects.events.FailedLoadParkingObjectsEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.parkingobjects.events.ParkingObjectEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.parkingobjects.events.ParkingObjectSelectedEvent;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class ParkingObjectsViewModel extends BaseViewModel<ParkingObjectEvent> {

    @NonNull
    private final ParkirajMeService parkirajMeService;

    @NonNull
    private final LocalStorage localStorage;

    @NonNull
    private final MutableLiveData<List<ParkingObject>> parkingObjects = new MutableLiveData<>();

    @ViewModelInject
    public ParkingObjectsViewModel(
        @NonNull ParkirajMeService parkirajMeService,
        @NonNull LocalStorage localStorage
    ) {
        this.parkirajMeService = parkirajMeService;
        this.localStorage = localStorage;
    }

    public void initialize() {
        disposable.add(
            parkirajMeService.getParkingObjects(localStorage.getSession().getUser().getUserUuid(), localStorage.getSession().getToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    result -> parkingObjects.setValue(result.getCompanyParkingObjectList()),
                    throwable -> emitEvent(new FailedLoadParkingObjectsEvent())
                ));
    }

    public void onParkingObjectSelected(ParkingObject parkingObject) {
        emitEvent(new ParkingObjectSelectedEvent(parkingObject));
    }

    @NonNull
    public LiveData<List<ParkingObject>> getParkingObjects() {
        return parkingObjects;
    }
}
