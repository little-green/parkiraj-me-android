package hr.fer.littlegreen.parkirajme.androidapp.ui.person.createreservation;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;

import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import hr.fer.littlegreen.parkirajme.androidapp.R;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.ParkirajMeService;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.models.createreservation.CreateReservationRequest;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.ParkingObject;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.ReservationType;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseViewModel;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.createreservation.events.CreateReservationEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.createreservation.events.ErrorEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.createreservation.events.ReservationCreatedEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.createreservation.state.CreateReservationChooseDateDialog;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.createreservation.state.CreateReservationChooseTimeDialog;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.createreservation.state.CreateReservationDialog;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.ErrorExtractor;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.schedulers.Schedulers;

public final class CreateReservationViewModel extends BaseViewModel<CreateReservationEvent> {

    @NonNull
    private static final String TIME_PATTERN = "HH:mm";

    @NonNull
    private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern(TIME_PATTERN);

    @NonNull
    private final ParkirajMeService parkirajMeService;

    @NonNull
    private final ErrorExtractor errorExtractor;

    @NonNull
    private final MutableLiveData<ParkingObject> parkingObject = new MutableLiveData<>();

    @NonNull
    private final MutableLiveData<ReservationType> reservationType = new MutableLiveData<>(ReservationType.ONE_TIME);

    @NonNull
    private final MutableLiveData<LocalDate> date = new MutableLiveData<>(LocalDate.now());

    @NonNull
    private final MutableLiveData<LocalTime> startTime = new MutableLiveData<>(LocalTime.of(7, 30));

    @NonNull
    private final MutableLiveData<LocalTime> endTime = new MutableLiveData<>(LocalTime.of(9, 30));

    @NonNull
    private final MutableLiveData<Set<DayOfWeek>> days = new MutableLiveData<>(new HashSet<>());

    @NonNull
    private final MutableLiveData<CreateReservationDialog> dialog = new MutableLiveData<>();

    @ViewModelInject
    public CreateReservationViewModel(
        @NonNull ParkirajMeService parkirajMeService,
        @NonNull ErrorExtractor errorExtractor
    ) {
        this.parkirajMeService = parkirajMeService;
        this.errorExtractor = errorExtractor;
    }

    public void initialize(@NonNull ParkingObject parkingObject) {
        this.parkingObject.setValue(parkingObject);
    }

    public void selectReservationType(@NonNull ReservationType selectedReservationType) {
        reservationType.setValue(selectedReservationType);
    }

    public void selectDate() {
        dialog.setValue(new CreateReservationChooseDateDialog(
            R.string.create_reservation_choose_date,
            date.getValue().atStartOfDay().toInstant(ZoneOffset.UTC).toEpochMilli(),
            epochMillisecond -> date.setValue(Instant.ofEpochMilli(epochMillisecond).atOffset(ZoneOffset.UTC).toLocalDate())
        ));
    }

    public void selectStartTime() {
        dialog.setValue(new CreateReservationChooseTimeDialog(
            R.string.create_reservation_choose_start_time,
            startTime.getValue().getHour(),
            startTime.getValue().getMinute(),
            (hour, minute) -> startTime.setValue(LocalTime.of(hour, minute))
        ));
    }

    public void selectEndTime() {
        dialog.setValue(new CreateReservationChooseTimeDialog(
            R.string.create_reservation_choose_end_time,
            endTime.getValue().getHour(),
            endTime.getValue().getMinute(),
            (hour, minute) -> endTime.setValue(LocalTime.of(hour, minute))
        ));
    }

    public void dismissDialog() {
        dialog.setValue(null);
    }

    public void updateDaySelection(@NonNull DayOfWeek day, boolean isSelected) {
        if (isSelected) {
            days.getValue().add(day);
        } else {
            days.getValue().remove(day);
        }
    }

    public void createReservation() {
        if (reservationType.getValue() == ReservationType.ALL_TIME) {
            createAllTimeReservation();
        } else {
            createOneTimeOrPeriodicReservation();
        }
    }

    @NonNull
    public LiveData<String> getParkingObjectAddress() {
        return Transformations.map(parkingObject, ParkingObject::getAddress);
    }

    @NonNull
    public LiveData<ReservationType> getReservationType() {
        return reservationType;
    }

    @NonNull
    public LiveData<Boolean> getDateSectionVisibility() {
        return Transformations.map(reservationType, t -> t != ReservationType.ALL_TIME);
    }

    @NonNull
    public LiveData<String> getDate() {
        return Transformations.map(date, DateTimeFormatter.ISO_LOCAL_DATE::format);
    }

    @NonNull
    public LiveData<Boolean> getTimeSectionVisibility() {
        return Transformations.map(reservationType, t -> t != ReservationType.ALL_TIME);
    }

    @NonNull
    public LiveData<String> getStartTime() {
        return Transformations.map(startTime, TIME_FORMATTER::format);
    }

    @NonNull
    public LiveData<String> getEndTime() {
        return Transformations.map(endTime, TIME_FORMATTER::format);
    }

    @NonNull
    public LiveData<Boolean> getDaysSectionVisibility() {
        return Transformations.map(reservationType, t -> t == ReservationType.PERIODIC);
    }

    @NonNull
    public LiveData<Set<DayOfWeek>> getDays() {
        return days;
    }

    @NonNull
    public LiveData<CreateReservationDialog> getDialog() {
        return dialog;
    }

    private void createAllTimeReservation() {
        LocalDateTime start = LocalDate.now().plusDays(1).atTime(0, 0, 0);
        LocalDateTime end = start.plusMonths(1).withHour(23).withMinute(59);
        disposable.add(
            parkirajMeService.createReservation(
                new CreateReservationRequest(
                    start,
                    end,
                    "1111111",
                    parkingObject.getValue().getObjectUuid()
                )
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    result -> emitEvent(new ReservationCreatedEvent()),
                    throwable -> emitEvent(new ErrorEvent(errorExtractor.extract(throwable)))
                )
        );
    }

    private void createOneTimeOrPeriodicReservation() {
        if (endTime.getValue().compareTo(startTime.getValue()) <= 0) {
            emitEvent(new ErrorEvent("Završetak termina mora biti kasnije od početka."));
            return;
        } else if (areMinutesIncorrect(startTime.getValue().getMinute()) || areMinutesIncorrect(endTime.getValue().getMinute())) {
            emitEvent(new ErrorEvent("Termin može početi ili završiti samo na puni sat, ili puni sat i pol."));
            return;
        } else if (Duration.between(LocalDateTime.now(), date.getValue().atTime(startTime.getValue())).compareTo(Duration.ofHours(6)) < 0) {
            emitEvent(new ErrorEvent("Termin mora biti rezerviran barem 6h unaprijed."));
            return;
        }
        String daysOfWeek = "0000000";
        if (reservationType.getValue() == ReservationType.PERIODIC) {
            daysOfWeek = getDaysOfWeekString();
        }
        disposable.add(
            parkirajMeService.createReservation(
                new CreateReservationRequest(
                    startTime.getValue().atDate(date.getValue()),
                    endTime.getValue().atDate(getExpirationDate()),
                    daysOfWeek,
                    parkingObject.getValue().getObjectUuid()
                )
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    result -> emitEvent(new ReservationCreatedEvent()),
                    throwable -> emitEvent(new ErrorEvent(errorExtractor.extract(throwable)))
                )
        );
    }

    private LocalDate getExpirationDate() {
        if (reservationType.getValue() == ReservationType.ONE_TIME) {
            return date.getValue();
        } else {
            return date.getValue().plusMonths(1);
        }
    }

    private String getDaysOfWeekString() {
        Set<DayOfWeek> days = this.days.getValue();
        StringBuilder result = new StringBuilder();
        if (days.contains(DayOfWeek.MONDAY)) {
            result.append('1');
        } else {
            result.append('0');
        }
        if (days.contains(DayOfWeek.TUESDAY)) {
            result.append('1');
        } else {
            result.append('0');
        }
        if (days.contains(DayOfWeek.WEDNESDAY)) {
            result.append('1');
        } else {
            result.append('0');
        }
        if (days.contains(DayOfWeek.THURSDAY)) {
            result.append('1');
        } else {
            result.append('0');
        }
        if (days.contains(DayOfWeek.FRIDAY)) {
            result.append('1');
        } else {
            result.append('0');
        }
        if (days.contains(DayOfWeek.SATURDAY)) {
            result.append('1');
        } else {
            result.append('0');
        }
        if (days.contains(DayOfWeek.SUNDAY)) {
            result.append('1');
        } else {
            result.append('0');
        }
        return result.toString();
    }

    private boolean areMinutesIncorrect(int minutes) {
        return minutes != 0 && minutes != 30;
    }
}
