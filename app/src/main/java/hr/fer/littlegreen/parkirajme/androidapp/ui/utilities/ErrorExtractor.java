package hr.fer.littlegreen.parkirajme.androidapp.ui.utilities;

import com.google.gson.Gson;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.models.ErrorResponse;
import retrofit2.HttpException;
import timber.log.Timber;

public final class ErrorExtractor {

    @NonNull
    private final Gson gson;

    @Inject
    public ErrorExtractor(@NonNull Gson gson) {
        this.gson = gson;
    }

    @Nullable
    public String extract(@NonNull Throwable throwable) {
        if (throwable instanceof HttpException) {
            try {
                return gson
                    .fromJson(((HttpException) throwable).response().errorBody().charStream(), ErrorResponse.class)
                    .getError();
            } catch (Exception e) {
                Timber.e(e);
            }
        }
        return null;
    }
}
