package hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state;

import java.util.Objects;

import androidx.annotation.Nullable;

public final class CompanyFormState {

    @Nullable
    private final String oib;

    @Nullable
    private final String name;

    @Nullable
    private final String address;

    public CompanyFormState(@Nullable String oib, @Nullable String name, @Nullable String address) {
        this.oib = oib;
        this.name = name;
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CompanyFormState that = (CompanyFormState) o;
        return Objects.equals(oib, that.oib) &&
            Objects.equals(name, that.name) &&
            Objects.equals(address, that.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(oib, name, address);
    }

    @Nullable
    public String getOib() {
        return oib;
    }

    @Nullable
    public String getName() {
        return name;
    }

    @Nullable
    public String getAddress() {
        return address;
    }
}
