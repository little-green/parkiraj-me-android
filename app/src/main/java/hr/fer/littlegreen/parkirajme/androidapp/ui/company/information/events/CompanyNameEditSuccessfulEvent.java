package hr.fer.littlegreen.parkirajme.androidapp.ui.company.information.events;

import org.jetbrains.annotations.NotNull;

public class CompanyNameEditSuccessfulEvent implements CompanyInformationEvent {

    @NotNull
    private final String name;

    public CompanyNameEditSuccessfulEvent(@NotNull String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
