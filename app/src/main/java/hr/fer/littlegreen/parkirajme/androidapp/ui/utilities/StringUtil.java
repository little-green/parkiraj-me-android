package hr.fer.littlegreen.parkirajme.androidapp.ui.utilities;

import androidx.annotation.Nullable;

public final class StringUtil {

    public static boolean isNotNullOrEmpty(@Nullable String string) {
        return string != null && !string.isEmpty();
    }

    public static boolean areNotNullOrEmpty(@Nullable String... strings) {
        for (String s : strings) {
            if (!isNotNullOrEmpty(s)) {
                return false;
            }
        }
        return true;
    }
}
