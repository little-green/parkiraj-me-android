package hr.fer.littlegreen.parkirajme.androidapp.ui.person.createreservation.state;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import androidx.annotation.NonNull;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.ReservationType;

public final class ReservationTypeOption {

    @NonNull
    private final ReservationType reservationType;

    @NonNull
    private final String text;

    public ReservationTypeOption(@NonNull ReservationType reservationType, @NonNull String text) {
        this.reservationType = reservationType;
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ReservationTypeOption that = (ReservationTypeOption) o;
        return reservationType == that.reservationType &&
            text.equals(that.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(reservationType, text);
    }

    @Override
    @NotNull
    public String toString() {
        return text;
    }

    @NonNull
    public ReservationType getReservationType() {
        return reservationType;
    }

    @NonNull
    public String getText() {
        return text;
    }
}
