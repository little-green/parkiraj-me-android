package hr.fer.littlegreen.parkirajme.androidapp.ui;

import android.content.res.TypedArray;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.ColorInt;
import androidx.appcompat.app.AppCompatActivity;
import hr.fer.littlegreen.parkirajme.androidapp.R;

public abstract class BaseActivity extends AppCompatActivity {

    @ColorInt
    public int getColorPrimary() {
        TypedValue typedValue = new TypedValue();
        TypedArray a = obtainStyledAttributes(typedValue.data, new int[] {R.attr.colorPrimary});
        int color = a.getColor(0, 0);
        a.recycle();
        return color;
    }

    @ColorInt
    public int getColorOnPrimary() {
        TypedValue typedValue = new TypedValue();
        TypedArray a = obtainStyledAttributes(typedValue.data, new int[] {R.attr.colorOnPrimary});
        int color = a.getColor(0, 0);
        a.recycle();
        return color;
    }

    @ColorInt
    public int getColorSecondary() {
        TypedValue typedValue = new TypedValue();
        TypedArray a = obtainStyledAttributes(typedValue.data, new int[] {R.attr.colorSecondary});
        int color = a.getColor(0, 0);
        a.recycle();
        return color;
    }

    @ColorInt
    public int getColorOnSecondary() {
        TypedValue typedValue = new TypedValue();
        TypedArray a = obtainStyledAttributes(typedValue.data, new int[] {R.attr.colorOnSecondary});
        int color = a.getColor(0, 0);
        a.recycle();
        return color;
    }

    public void hideKeyboard() {
        View view = getCurrentFocus();
        if (view == null) {
            view = new View(this);
        }
        hideKeyboardFrom(view);
    }

    public void hideKeyboardFrom(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
