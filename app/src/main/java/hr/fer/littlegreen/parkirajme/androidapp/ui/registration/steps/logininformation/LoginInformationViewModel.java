package hr.fer.littlegreen.parkirajme.androidapp.ui.registration.steps.logininformation;

import java.time.YearMonth;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.ParkirajMeService;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.models.registration.CompanyRegistrationRequest;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.models.registration.PersonRegistrationRequest;
import hr.fer.littlegreen.parkirajme.androidapp.data.local.LocalStorage;
import hr.fer.littlegreen.parkirajme.androidapp.data.local.Session;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Company;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Person;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Vehicle;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseViewModel;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state.CardInformationFormState;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state.CompanyFormState;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state.LoginInformationFormState;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state.PersonPersonalInformationFormState;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state.RegistrationFlowState;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state.VehicleInformationFormState;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.steps.logininformation.events.CompanyRegistrationSuccessEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.steps.logininformation.events.LoginInformationStepEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.steps.logininformation.events.PersonRegistrationSuccessEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.steps.logininformation.events.RegistrationFailedEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.steps.usertype.state.UserType;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.ErrorExtractor;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.StringUtil;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.schedulers.Schedulers;
import timber.log.Timber;

public final class LoginInformationViewModel extends BaseViewModel<LoginInformationStepEvent> {

    @NonNull
    private final RegistrationFlowState registrationFlowState;

    @NonNull
    private final ParkirajMeService parkirajMeService;

    @NonNull
    private final LocalStorage localStorage;

    @NonNull
    private final ErrorExtractor errorExtractor;

    @NonNull
    private final MutableLiveData<Boolean> isProgressShown = new MutableLiveData<>(false);

    @ViewModelInject
    public LoginInformationViewModel(
        @NonNull RegistrationFlowState registrationFlowState,
        @NonNull ParkirajMeService parkirajMeService, @NonNull LocalStorage localStorage,
        @NonNull ErrorExtractor errorExtractor
    ) {
        this.registrationFlowState = registrationFlowState;
        this.parkirajMeService = parkirajMeService;
        this.localStorage = localStorage;
        this.errorExtractor = errorExtractor;
    }

    public void onEmailChanged(@Nullable String email) {
        LoginInformationFormState loginInformationFormState = getOrCreateLoginInformationFormState();
        registrationFlowState.loginInformationFormState.setValue(new LoginInformationFormState(
            email,
            loginInformationFormState.getPassword()
        ));
    }

    public void onPasswordChanged(@Nullable String password) {
        LoginInformationFormState loginInformationFormState = getOrCreateLoginInformationFormState();
        registrationFlowState.loginInformationFormState.setValue(new LoginInformationFormState(
            loginInformationFormState.getEmail(),
            password
        ));
    }

    public void onFinishRegistrationClicked() {
        UserType userType = registrationFlowState.userType.getValue();
        PersonPersonalInformationFormState personPersonalInformationFormState =
            registrationFlowState.personPersonalInformationFormState.getValue();
        VehicleInformationFormState vehicleInformationFormState = registrationFlowState.vehicleInformationFormState.getValue();
        CardInformationFormState cardInformationFormState = registrationFlowState.cardInformationFormState.getValue();
        CompanyFormState companyFormState = registrationFlowState.companyFormState.getValue();
        LoginInformationFormState loginInformationFormState = registrationFlowState.loginInformationFormState.getValue();
        if (isValid(loginInformationFormState)) {
            if (userType == UserType.PERSON) {
                String[] expiryDateParts = cardInformationFormState.getExpiryDate().split("/");
                if (expiryDateParts.length != 2) {
                    emitEvent(new RegistrationFailedEvent("Neispravan format datuma isteka kartice."));
                    return;
                }
                int expiryDateMonth, expiryDateYear;
                try {
                    expiryDateMonth = Integer.parseInt(expiryDateParts[0]);
                    expiryDateYear = Integer.parseInt("20" + expiryDateParts[1]);
                } catch (Exception e) {
                    Timber.e(e);
                    emitEvent(new RegistrationFailedEvent("Neispravan format datuma isteka kartice."));
                    return;
                }
                String oib = personPersonalInformationFormState.getOib();
                String firstName = personPersonalInformationFormState.getName();
                String lastName = personPersonalInformationFormState.getSurname();
                List<String> registrationPlates = Collections.singletonList(vehicleInformationFormState.getRegistrationPlate());
                String creditCardNumber = cardInformationFormState.getCardNumber();
                YearMonth creditCardExpirationDate = YearMonth.of(expiryDateYear, expiryDateMonth);
                String email = loginInformationFormState.getEmail();
                String password = loginInformationFormState.getPassword();
                isProgressShown.setValue(true);
                disposable.add(
                    parkirajMeService
                        .registerPerson(new PersonRegistrationRequest(
                            oib,
                            firstName,
                            lastName,
                            registrationPlates,
                            creditCardNumber,
                            creditCardExpirationDate,
                            email,
                            password
                        ))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(result -> {
                            Person person = new Person(
                                result.getUserUuid(),
                                email,
                                oib,
                                firstName,
                                lastName,
                                creditCardNumber,
                                creditCardExpirationDate,
                                registrationPlates.stream().map(Vehicle::new).collect(Collectors.toList())
                            );
                            localStorage.putSession(new Session(result.getToken(), person));
                            isProgressShown.setValue(false);
                            emitEvent(new PersonRegistrationSuccessEvent(person));
                        }, throwable -> {
                            Timber.e(throwable);
                            isProgressShown.setValue(false);
                            emitEvent(new RegistrationFailedEvent(errorExtractor.extract(throwable)));
                        })
                );
            } else {
                String oib = companyFormState.getOib();
                String name = companyFormState.getName();
                String address = companyFormState.getAddress();
                String email = loginInformationFormState.getEmail();
                String password = loginInformationFormState.getPassword();
                isProgressShown.setValue(true);
                disposable.add(
                    parkirajMeService.registerCompany(new CompanyRegistrationRequest(oib, name, address, email, password))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(result -> {
                            Company company = new Company(result.getUserUuid(), email, oib, name, address);
                            localStorage.putSession(new Session(result.getToken(), company));
                            isProgressShown.setValue(false);
                            emitEvent(new CompanyRegistrationSuccessEvent(company));
                        }, throwable -> {
                            Timber.e(throwable);
                            isProgressShown.setValue(false);
                            emitEvent(new RegistrationFailedEvent(errorExtractor.extract(throwable)));
                        })
                );
            }
        }
    }

    @NonNull
    public LiveData<LoginInformationFormState> getLoginInformationFormState() {
        return registrationFlowState.loginInformationFormState;
    }

    @NonNull
    public LiveData<Boolean> isProgressShown() {
        return isProgressShown;
    }

    @NonNull
    private LoginInformationFormState getOrCreateLoginInformationFormState() {
        LoginInformationFormState loginInformationFormState = registrationFlowState.loginInformationFormState.getValue();
        if (loginInformationFormState != null) {
            return loginInformationFormState;
        } else {
            return new LoginInformationFormState(null, null);
        }
    }

    private boolean isValid(@Nullable LoginInformationFormState loginInformationFormState) {
        return loginInformationFormState != null && StringUtil.areNotNullOrEmpty(
            loginInformationFormState.getEmail(),
            loginInformationFormState.getPassword()
        );
    }
}
