package hr.fer.littlegreen.parkirajme.androidapp.data.http.models.registration;

import java.util.Objects;

import androidx.annotation.NonNull;

public final class UserRegistrationResponse {

    @NonNull
    private final String token;

    @NonNull
    private final String userUuid;

    public UserRegistrationResponse(@NonNull String token, @NonNull String userUuid) {
        this.token = token;
        this.userUuid = userUuid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserRegistrationResponse that = (UserRegistrationResponse) o;
        return token.equals(that.token) &&
            userUuid.equals(that.userUuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(token, userUuid);
    }

    @NonNull
    public String getToken() {
        return token;
    }

    @NonNull
    public String getUserUuid() {
        return userUuid;
    }
}
