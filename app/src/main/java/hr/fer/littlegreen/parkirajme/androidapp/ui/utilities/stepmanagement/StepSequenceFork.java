package hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.stepmanagement;

import java.util.Objects;

import androidx.annotation.NonNull;

public final class StepSequenceFork extends StepSequence {

    @NonNull
    private final StepSequence firstSequence;

    @NonNull
    private final StepSequence secondSequence;

    private boolean isFirstSequenceActive = true;

    public StepSequenceFork(
        @NonNull StepSequence firstSequence,
        @NonNull StepSequence secondSequence
    ) {
        this.firstSequence = firstSequence;
        this.secondSequence = secondSequence;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        StepSequenceFork that = (StepSequenceFork) o;
        return isFirstSequenceActive == that.isFirstSequenceActive &&
            firstSequence.equals(that.firstSequence) &&
            secondSequence.equals(that.secondSequence);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstSequence, secondSequence, isFirstSequenceActive);
    }

    @Override
    public int getProgressWeight() {
        return Math.max(firstSequence.getProgressWeight(), secondSequence.getProgressWeight());
    }

    @NonNull
    @Override
    public StepSequenceIterator stepSequenceIterator() {
        return new StepSequenceForkIterator();
    }

    @NonNull
    public StepSequence getActiveSequence() {
        if (isFirstSequenceActive) {
            return firstSequence;
        } else {
            return secondSequence;
        }
    }

    public void activateFirstSequence() {
        isFirstSequenceActive = true;
    }

    public void activateSecondSequence() {
        isFirstSequenceActive = false;
    }

    private class StepSequenceForkIterator implements StepSequenceIterator {

        @NonNull
        private final StepSequenceIterator activeStepSequenceIterator = getActiveSequence().stepSequenceIterator();

        @Override
        public boolean canDereference() {
            return activeStepSequenceIterator.canDereference();
        }

        @NonNull
        @Override
        public StepSequence dereference() throws IllegalStateException {
            if (canDereference()) {
                return activeStepSequenceIterator.dereference();
            } else {
                throw new IllegalStateException();
            }
        }

        @Override
        public void iterateForward() {
            activeStepSequenceIterator.iterateForward();
        }

        @Override
        public void iterateBackward() {
            activeStepSequenceIterator.iterateBackward();
        }

        @Override
        public double getIteratedWeight() {
            return (activeStepSequenceIterator.getIteratedWeight() / getActiveSequence().getProgressWeight()) * getProgressWeight();
        }
    }
}
