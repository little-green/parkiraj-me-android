package hr.fer.littlegreen.parkirajme.androidapp.ui.person.reservations.events;

import java.util.Objects;

import androidx.annotation.NonNull;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.ReservationAndParkingObjectPair;

public final class ReservationSelectedEvent implements ReservationsEvent {

    @NonNull
    private final ReservationAndParkingObjectPair selectedReservation;

    public ReservationSelectedEvent(@NonNull ReservationAndParkingObjectPair selectedReservation) {
        this.selectedReservation = selectedReservation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ReservationSelectedEvent that = (ReservationSelectedEvent) o;
        return selectedReservation.equals(that.selectedReservation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(selectedReservation);
    }

    @NonNull
    public ReservationAndParkingObjectPair getSelectedReservation() {
        return selectedReservation;
    }
}
