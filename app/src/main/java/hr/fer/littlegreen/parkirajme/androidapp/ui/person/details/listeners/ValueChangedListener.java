package hr.fer.littlegreen.parkirajme.androidapp.ui.person.details.listeners;

import androidx.annotation.NonNull;

public interface ValueChangedListener {

    void onValueChanged(@NonNull String value);
}
