package hr.fer.littlegreen.parkirajme.androidapp.data.http.models.registration;

import java.util.Objects;

import androidx.annotation.NonNull;

public final class CompanyRegistrationRequest {

    @NonNull
    private final String oib;

    @NonNull
    private final String name;

    @NonNull
    private final String headquarterAddress;

    @NonNull
    private final String email;

    @NonNull
    private final String password;

    public CompanyRegistrationRequest(
        @NonNull String oib,
        @NonNull String name,
        @NonNull String headquarterAddress,
        @NonNull String email,
        @NonNull String password
    ) {
        this.oib = oib;
        this.name = name;
        this.headquarterAddress = headquarterAddress;
        this.email = email;
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CompanyRegistrationRequest that = (CompanyRegistrationRequest) o;
        return oib.equals(that.oib) &&
            name.equals(that.name) &&
            headquarterAddress.equals(that.headquarterAddress) &&
            email.equals(that.email) &&
            password.equals(that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(oib, name, headquarterAddress, email, password);
    }

    @NonNull
    public String getOib() {
        return oib;
    }

    @NonNull
    public String getName() {
        return name;
    }

    @NonNull
    public String getHeadquarterAddress() {
        return headquarterAddress;
    }

    @NonNull
    public String getEmail() {
        return email;
    }

    @NonNull
    public String getPassword() {
        return password;
    }
}
