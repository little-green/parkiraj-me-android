package hr.fer.littlegreen.parkirajme.androidapp.ui.person.map;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.ParkirajMeService;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.ParkingObject;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseViewModel;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.map.events.ErrorEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.map.events.MapEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.map.events.MoveToParkingObjectEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.ErrorExtractor;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.schedulers.Schedulers;
import timber.log.Timber;

public final class MapViewModel extends BaseViewModel<MapEvent> {

    @NonNull
    private final ParkirajMeService parkirajMeService;

    @NonNull
    private final ErrorExtractor errorExtractor;

    @NonNull
    private final MutableLiveData<List<ParkingObject>> parkingObjects = new MutableLiveData<>();

    @NonNull
    private final MutableLiveData<ParkingObject> selectedParkingObject = new MutableLiveData<>();

    @ViewModelInject
    public MapViewModel(
        @NonNull ParkirajMeService parkirajMeService,
        @NonNull ErrorExtractor errorExtractor
    ) {
        this.parkirajMeService = parkirajMeService;
        this.errorExtractor = errorExtractor;
    }

    public void initialize() {
        disposable.add(
            parkirajMeService.getAllParkingObjects()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> parkingObjects.setValue(result.getParkingObjectList()), throwable -> {
                    Timber.e(throwable);
                    emitEvent(new ErrorEvent(errorExtractor.extract(throwable)));
                })
        );
    }

    public void findTheClosestParkingObject(double latitude, double longitude) {
        disposable.add(
            parkirajMeService.getParkingObject(latitude, longitude)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> emitEvent(new MoveToParkingObjectEvent(result.getParkingObject())), throwable -> {
                    Timber.e(throwable);
                    emitEvent(new ErrorEvent(errorExtractor.extract(throwable)));
                })
        );
    }

    public void selectParkingObject(ParkingObject parkingObject) {
        selectedParkingObject.setValue(parkingObject);
    }

    @NonNull
    public LiveData<List<ParkingObject>> getParkingObjects() {
        return parkingObjects;
    }

    @NonNull
    public LiveData<ParkingObject> getSelectedParkingObject() {
        return selectedParkingObject;
    }
}
