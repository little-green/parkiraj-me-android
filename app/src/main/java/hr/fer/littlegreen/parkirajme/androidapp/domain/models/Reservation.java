package hr.fer.littlegreen.parkirajme.androidapp.domain.models;

import java.io.Serializable;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;

import static hr.fer.littlegreen.parkirajme.androidapp.domain.models.ReservationType.ALL_TIME;
import static hr.fer.littlegreen.parkirajme.androidapp.domain.models.ReservationType.ONE_TIME;
import static hr.fer.littlegreen.parkirajme.androidapp.domain.models.ReservationType.PERIODIC;

public final class Reservation implements Serializable {

    @NonNull
    private final String reservationId;

    @NonNull
    private final String userUuid;

    @NonNull
    private final String objectUuid;

    @NonNull
    private final LocalDateTime startTime;

    @NonNull
    private final LocalDateTime endTime;

    @NonNull
    private final String daysOfWeek;

    public Reservation(
        @NonNull String reservationId,
        @NonNull String userUuid,
        @NonNull String objectUuid,
        @NonNull LocalDateTime startTime,
        @NonNull LocalDateTime endTime,
        @NonNull String daysOfWeek
    ) {
        this.reservationId = reservationId;
        this.userUuid = userUuid;
        this.objectUuid = objectUuid;
        this.startTime = startTime;
        this.endTime = endTime;
        this.daysOfWeek = daysOfWeek;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Reservation that = (Reservation) o;
        return reservationId.equals(that.reservationId) &&
            userUuid.equals(that.userUuid) &&
            objectUuid.equals(that.objectUuid) &&
            startTime.equals(that.startTime) &&
            endTime.equals(that.endTime) &&
            daysOfWeek.equals(that.daysOfWeek);
    }

    @Override
    public int hashCode() {
        return Objects.hash(reservationId, userUuid, objectUuid, startTime, endTime, daysOfWeek);
    }

    @NonNull
    public String getReservationId() {
        return reservationId;
    }

    @NonNull
    public String getUserUuid() {
        return userUuid;
    }

    @NonNull
    public String getObjectUuid() {
        return objectUuid;
    }

    @NonNull
    public LocalDateTime getStartTime() {
        return startTime;
    }

    @NonNull
    public LocalDateTime getEndTime() {
        return endTime;
    }

    @NonNull
    public String getDaysOfWeek() {
        return daysOfWeek;
    }

    @NonNull
    public ReservationType getReservationType() {
        if (getNumberOfDays() == 0) {
            return ONE_TIME;
        } else if (isAllTime()) {
            return ALL_TIME;
        } else {
            return PERIODIC;
        }
    }

    @NonNull
    public List<DayOfWeek> getDaysOfWeekList() {
        List<DayOfWeek> daysOfWeekList = new ArrayList<>();
        if (daysOfWeek.charAt(0) == '1') {
            daysOfWeekList.add(DayOfWeek.MONDAY);
        }
        if (daysOfWeek.charAt(1) == '1') {
            daysOfWeekList.add(DayOfWeek.TUESDAY);
        }
        if (daysOfWeek.charAt(2) == '1') {
            daysOfWeekList.add(DayOfWeek.WEDNESDAY);
        }
        if (daysOfWeek.charAt(3) == '1') {
            daysOfWeekList.add(DayOfWeek.THURSDAY);
        }
        if (daysOfWeek.charAt(4) == '1') {
            daysOfWeekList.add(DayOfWeek.FRIDAY);
        }
        if (daysOfWeek.charAt(5) == '1') {
            daysOfWeekList.add(DayOfWeek.SATURDAY);
        }
        if (daysOfWeek.charAt(6) == '1') {
            daysOfWeekList.add(DayOfWeek.SUNDAY);
        }
        return daysOfWeekList;
    }

    private boolean isAllTime() {
        return getNumberOfDays() == 7
            && startTime.getHour() == 0
            && startTime.getMinute() == 0
            && endTime.getHour() == 23
            && endTime.getMinute() == 59;
    }

    private int getNumberOfDays() {
        int days = 0;
        for (int i = 0; i < daysOfWeek.length(); ++i) {
            if (daysOfWeek.charAt(i) == '1') {
                days += 1;
            }
        }
        return days;
    }
}
