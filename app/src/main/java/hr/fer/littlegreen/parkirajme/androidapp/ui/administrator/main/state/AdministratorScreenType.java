package hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.main.state;

public enum AdministratorScreenType {

    USERS,
    ADMINDETAILS
}
