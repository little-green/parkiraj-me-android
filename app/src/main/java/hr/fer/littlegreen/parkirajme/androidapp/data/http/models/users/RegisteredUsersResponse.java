package hr.fer.littlegreen.parkirajme.androidapp.data.http.models.users;

import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.User;

public class RegisteredUsersResponse {

    @NonNull
    private final List<User> userList;

    public RegisteredUsersResponse(@NonNull List<User> userList) {
        this.userList = userList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RegisteredUsersResponse that = (RegisteredUsersResponse) o;
        return userList.equals(that.userList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userList);
    }

    @NonNull
    public List<User> getUsers() {
        return userList;
    }
}
