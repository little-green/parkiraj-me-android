package hr.fer.littlegreen.parkirajme.androidapp.data.http.models.createreservation;

import java.util.Objects;

import androidx.annotation.Nullable;

public final class CreateReservationResponse {

    @Nullable
    private final String reservationId;

    public CreateReservationResponse(@Nullable String reservationId) {
        this.reservationId = reservationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CreateReservationResponse that = (CreateReservationResponse) o;
        return Objects.equals(reservationId, that.reservationId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(reservationId);
    }

    @Nullable
    public String getReservationId() {
        return reservationId;
    }
}
