package hr.fer.littlegreen.parkirajme.androidapp.ui.registration.steps.logininformation.events;

import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseEvent;

public interface LoginInformationStepEvent extends BaseEvent {
}
