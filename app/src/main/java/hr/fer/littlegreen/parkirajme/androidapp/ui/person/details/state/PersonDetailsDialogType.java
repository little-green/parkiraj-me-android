package hr.fer.littlegreen.parkirajme.androidapp.ui.person.details.state;

public enum PersonDetailsDialogType {

    LOGOUT_CONFIRMATION,
    DELETE_ACCOUNT_CONFIRMATION,
    ERROR
}
