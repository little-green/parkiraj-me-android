package hr.fer.littlegreen.parkirajme.androidapp.data.http.models.parkingobjects;

import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.ParkingObject;

public class AllParkingObjectsResponse {

    @NonNull
    private final List<ParkingObject> parkingObjectList;

    public AllParkingObjectsResponse(@NonNull List<ParkingObject> parkingObjects) {
        this.parkingObjectList = parkingObjects;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AllParkingObjectsResponse that = (AllParkingObjectsResponse) o;
        return parkingObjectList.equals(that.parkingObjectList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(parkingObjectList);
    }

    @NonNull
    public List<ParkingObject> getParkingObjectList() {
        return parkingObjectList;
    }
}
