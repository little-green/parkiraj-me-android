package hr.fer.littlegreen.parkirajme.androidapp.ui.registration.steps.company.companyinformation;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import dagger.hilt.android.AndroidEntryPoint;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.FragmentCompanyInformationStepBinding;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseFragment;
import hr.fer.littlegreen.parkirajme.androidapp.ui.registration.state.CompanyFormState;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.TextViewUtil;

@AndroidEntryPoint
public final class CompanyInformationStepFragment extends BaseFragment {

    private CompanyInformationViewModel viewModel;
    private FragmentCompanyInformationStepBinding binding;

    @NonNull
    private final Observer<CompanyFormState> companyFormStateObserver = companyFormState -> {
        if (companyFormState == null) {
            return;
        }
        TextViewUtil.update(binding.companyOib, companyFormState.getOib());
        TextViewUtil.update(binding.companyName, companyFormState.getName());
        TextViewUtil.update(binding.companyAddress, companyFormState.getAddress());
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentCompanyInformationStepBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(this).get(CompanyInformationViewModel.class);
        viewModel.getCompanyFormState().observe(getViewLifecycleOwner(), companyFormStateObserver);
        TextViewUtil.addTextChangedListener(binding.companyOib, viewModel::onOibModified);
        TextViewUtil.addTextChangedListener(binding.companyName, viewModel::onNameModified);
        TextViewUtil.addTextChangedListener(binding.companyAddress, viewModel::onAddressModified);
        binding.continueButton.setOnClickListener(v -> viewModel.onContinueClicked());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
