package hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.users.adapters;

import hr.fer.littlegreen.parkirajme.androidapp.domain.models.User;

public interface UserClickListener {

    void onClick(User user);
}
