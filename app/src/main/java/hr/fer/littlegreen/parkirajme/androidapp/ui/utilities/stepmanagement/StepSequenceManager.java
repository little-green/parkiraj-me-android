package hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.stepmanagement;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

public interface StepSequenceManager {

    void iterateForward();

    void iterateBackward();

    @NonNull
    LiveData<Double> getProgress();
}
