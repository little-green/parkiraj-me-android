package hr.fer.littlegreen.parkirajme.androidapp.data.http.models;

import java.util.Objects;

import androidx.annotation.Nullable;

public final class ErrorResponse {

    @Nullable
    private final String error;

    public ErrorResponse(@Nullable String error) {
        this.error = error;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ErrorResponse that = (ErrorResponse) o;
        return Objects.equals(error, that.error);
    }

    @Override
    public int hashCode() {
        return Objects.hash(error);
    }

    @Nullable
    public String getError() {
        return error;
    }
}
