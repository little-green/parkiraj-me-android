package hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.personuserdetails;

import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.ParkirajMeService;
import hr.fer.littlegreen.parkirajme.androidapp.data.local.LocalStorage;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Person;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseViewModel;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.companyuserinformation.events.CompanyAccountDeletedEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.personuserdetails.events.PersonAccountDeletedEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.personuserdetails.events.PersonUserInformationEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.details.state.PersonDetailsDialogType;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class PersonUserInformationViewModel extends BaseViewModel<PersonUserInformationEvent> {

    @NonNull
    private final ParkirajMeService parkirajMeService;

    @NonNull
    private final LocalStorage localStorage;

    @NonNull
    private final MutableLiveData<Person> person = new MutableLiveData<>();

    @NonNull
    private final MutableLiveData<PersonDetailsDialogType> dialogType = new MutableLiveData<>();

    @ViewModelInject
    public PersonUserInformationViewModel(
        @NonNull ParkirajMeService parkirajMeService,
        @NonNull LocalStorage localStorage){
        this.parkirajMeService=parkirajMeService;
        this.localStorage=localStorage;
    }

    public void initialize(Person person){
        this.person.setValue(person);
    }

    public void onDeleteAccountClicked(){dialogType.setValue((PersonDetailsDialogType.DELETE_ACCOUNT_CONFIRMATION));}

    public void dismissDialog(){dialogType.setValue(null);}

    public void deletePerson(){
        dialogType.setValue(null);
        disposable.add(
             parkirajMeService.deleteAccount(person.getValue().getUserUuid())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    emitEvent(new PersonAccountDeletedEvent());
                }, throwable -> {
                    dialogType.setValue(PersonDetailsDialogType.ERROR);
                })
        );
    }

    @NonNull
    public LiveData<Person> getPerson(){return person;}

    public LiveData<PersonDetailsDialogType> getDialogType(){return dialogType;}
}
