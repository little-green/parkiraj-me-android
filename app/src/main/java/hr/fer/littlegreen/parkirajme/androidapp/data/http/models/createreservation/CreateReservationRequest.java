package hr.fer.littlegreen.parkirajme.androidapp.data.http.models.createreservation;

import java.time.LocalDateTime;
import java.util.Objects;

import androidx.annotation.NonNull;

public final class CreateReservationRequest {

    @NonNull
    private final LocalDateTime startTime;

    @NonNull
    private final LocalDateTime endTime;

    @NonNull
    private final String daysOfWeek;

    @NonNull
    private final String parkingId;

    public CreateReservationRequest(
        @NonNull LocalDateTime startTime,
        @NonNull LocalDateTime endTime,
        @NonNull String daysOfWeek,
        @NonNull String parkingId
    ) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.daysOfWeek = daysOfWeek;
        this.parkingId = parkingId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CreateReservationRequest that = (CreateReservationRequest) o;
        return startTime.equals(that.startTime) &&
            endTime.equals(that.endTime) &&
            daysOfWeek.equals(that.daysOfWeek) &&
            parkingId.equals(that.parkingId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(startTime, endTime, daysOfWeek, parkingId);
    }

    @NonNull
    public LocalDateTime getStartTime() {
        return startTime;
    }

    @NonNull
    public LocalDateTime getEndTime() {
        return endTime;
    }

    @NonNull
    public String getDaysOfWeek() {
        return daysOfWeek;
    }

    @NonNull
    public String getParkingId() {
        return parkingId;
    }
}
