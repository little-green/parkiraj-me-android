package hr.fer.littlegreen.parkirajme.androidapp.ui.company.parkingobjects.events;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import hr.fer.littlegreen.parkirajme.androidapp.domain.models.ParkingObject;

public class ParkingObjectSelectedEvent implements ParkingObjectEvent {

    @NotNull
    private final ParkingObject parkingObject;

    public ParkingObjectSelectedEvent(@NotNull ParkingObject parkingObject) {
        this.parkingObject = parkingObject;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ParkingObjectSelectedEvent that = (ParkingObjectSelectedEvent) o;
        return parkingObject.equals(that.parkingObject);
    }

    @Override
    public int hashCode() {
        return Objects.hash(parkingObject);
    }

    public ParkingObject getParkingObject() {
        return parkingObject;
    }
}
