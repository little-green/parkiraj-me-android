package hr.fer.littlegreen.parkirajme.androidapp.ui.person.editvehicles.adapters;

import androidx.annotation.NonNull;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.editvehicles.state.EditableVehicleItem;

public interface DeleteVehicleListener {

    void deleteVehicle(@NonNull EditableVehicleItem editableVehicleItem);
}
