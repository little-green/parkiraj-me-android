package hr.fer.littlegreen.parkirajme.androidapp.ui.utilities;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public final class TextViewUtil {

    public static void update(@NonNull TextView view, @Nullable String text) {
        if (!view.getText().toString().equals(text)) {
            view.setText(text);
        }
    }

    public static void addTextChangedListener(@NonNull TextView view, @NonNull TextChangedListener listener) {
        view.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                listener.onTextChanged(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }
}
