package hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.personuserdetails;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.Observer;
import java.util.logging.Level;

import javax.inject.Inject;

import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import dagger.hilt.android.AndroidEntryPoint;
import hr.fer.littlegreen.parkirajme.androidapp.data.local.LocalStorage;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.ActivityReservationDetailsBinding;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.ActivityPersonUserInformationBinding;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Administrator;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Company;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.ParkingObject;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Person;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Reservation;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.User;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Vehicle;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseFragment;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.companyuserinformation.CompanyUserInformationActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.companyuserinformation.CompanyUserInformationViewModel;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.companyuserinformation.events.CompanyUserInformationEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.main.AdministratorMainActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.personuserdetails.events.PersonAccountDeletedEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.personuserdetails.events.PersonUserInformationEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.information.events.AccountDeletedEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.details.PersonDetailsViewModel;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.details.adapters.VehicleAdapter;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.details.state.PersonDetailsDialogType;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.reservationdetails.ReservationDetailsViewModel;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.TextViewUtil;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.annotations.Nullable;

@AndroidEntryPoint
public class PersonUserInformationActivity extends BaseActivity {

    @Nullable
    private AlertDialog dialog;

    private PersonUserInformationViewModel viewModel;

    private ActivityPersonUserInformationBinding binding;
    @NonNull
    private final VehicleAdapter vehicleAdapter = new VehicleAdapter();

    @Inject
    LocalStorage localStorage;

    private static final String EXTRA_PERSON = "EXTRA_PERSON";

    private final Observer<PersonUserInformationEvent> eventObserver = event -> {
        if (event instanceof PersonAccountDeletedEvent) {
            startActivity(AdministratorMainActivity.newIntent(this, (Administrator) localStorage.getSession().getUser() ));
        }
    };

    private final Observer<PersonDetailsDialogType> dialogTypeObserver = dialogType -> {
        if(dialogType==null){
            if(dialog!=null){
                dialog.dismiss();
            }
            dialog=null;
        } else if (dialogType == PersonDetailsDialogType.DELETE_ACCOUNT_CONFIRMATION){
            dialog = new AlertDialog.Builder(this)
                .setTitle("Brisanje računa")
                .setMessage("Jeste li sigurni da želite izbrisati ovaj korisnički račun?")
                .setPositiveButton("Izbriši korisnički račun", (dialog, which) -> viewModel.deletePerson())
                .setNegativeButton("Odustani", (dialog, which) -> viewModel.dismissDialog())
                .setOnCancelListener(dialog -> viewModel.dismissDialog())
                .setOnDismissListener(dialog -> viewModel.dismissDialog())
                .create();
            dialog.show();
        } else if (dialogType == PersonDetailsDialogType.ERROR) {
            dialog = new AlertDialog.Builder(this)
                .setTitle("Greška")
                .setMessage("Došlo je do neočekivane pogreške. Pokušajte ponovo.")
                .setPositiveButton("U redu", (dialog, which) -> viewModel.dismissDialog())
                .setOnCancelListener(dialog -> viewModel.dismissDialog())
                .setOnDismissListener(dialog -> viewModel.dismissDialog())
                .create();
            dialog.show();
        }
    };

    @NonNull
    private final Observer<Person> personObserver = person -> {
        TextViewUtil.update(binding.personUserInformationOibFieldValue, person.getOib());
        TextViewUtil.update(binding.personUserInformationFirstNameFieldValue, person.getFirstName());
        TextViewUtil.update(binding.personUserInformationLastNameFieldValue, person.getLastName());
        TextViewUtil.update(binding.personUserInformationEmailFieldValue, person.getEmail());
        TextViewUtil.update(binding.personUserInformationCardNumberValue, person.getCreditCardNumber());
        TextViewUtil.update(binding.personUserInformationCardExpirationDateValue, person.getCreditCardExpirationDate().toString());
        vehicleAdapter.submitList(person.getVehicles()); //TODO make vehicle list work
    };

    @Nullable
    @Override
    public void onCreate(@androidx.annotation.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(PersonUserInformationViewModel.class);
        viewModel.getPerson().observe(this, personObserver);
        viewModel.initialize((Person)getIntent().getSerializableExtra(EXTRA_PERSON));
        binding = ActivityPersonUserInformationBinding.inflate(getLayoutInflater());
        binding.personUserInformationBackButton.setOnClickListener(v -> finish());
        binding.personUserInformationVehiclesList.setAdapter(vehicleAdapter);
        binding.personUserInformationVehiclesList.setLayoutManager(new LinearLayoutManager(this));
        binding.personUserInformationDeleteAccountButton.setOnClickListener(v -> viewModel.onDeleteAccountClicked());
        viewModel.getDialogType().observe(this, dialogTypeObserver);
        viewModel.getEvents().observe(this, eventObserver);
        viewModel.initialize(viewModel.getPerson().getValue());
        setContentView(binding.getRoot());
    }

    public static Intent newIntent(@androidx.annotation.NonNull Context context, @androidx.annotation.NonNull Person person) {
        return new Intent(context, PersonUserInformationActivity.class)
            .putExtra(EXTRA_PERSON, person);
    }


}
