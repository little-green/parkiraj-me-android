package hr.fer.littlegreen.parkirajme.androidapp.ui.unregistereduser;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import dagger.hilt.android.AndroidEntryPoint;
import hr.fer.littlegreen.parkirajme.androidapp.R;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.ActivityUnregisteredUserBinding;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseActivity;
import hr.fer.littlegreen.parkirajme.androidapp.ui.person.map.MapFragment;

@AndroidEntryPoint
public final class UnregisteredUserActivity extends BaseActivity {

    private UnregisteredUserViewModel viewModel;
    private ActivityUnregisteredUserBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(UnregisteredUserViewModel.class);
        binding = ActivityUnregisteredUserBinding.inflate(getLayoutInflater());
        binding.unregisteredUserBackButton.setOnClickListener(v -> finish());
        getSupportFragmentManager()
            .beginTransaction()
            .replace(R.id.unregisteredUserScreenContainer, new MapFragment())
            .commit();
        setContentView(binding.getRoot());
    }
}
