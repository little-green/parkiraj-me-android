package hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.information;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import dagger.hilt.android.AndroidEntryPoint;
import hr.fer.littlegreen.parkirajme.androidapp.databinding.FragmentAdministratorInformationBinding;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Administrator;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseFragment;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.information.events.AdministratorInformationEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.information.state.AdministratorInformationDialogType;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.information.events.LogoutEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.utilities.TextViewUtil;
import hr.fer.littlegreen.parkirajme.androidapp.ui.welcome.WelcomeActivity;

@AndroidEntryPoint
public class AdministratorInformationFragment extends BaseFragment {
    private AdministratorInformationViewModel viewModel;
    private FragmentAdministratorInformationBinding binding;

    @NonNull
    private final Observer<LogoutEvent> eventObserver = event -> {
        if (event instanceof LogoutEvent) {
            startActivity(WelcomeActivity.newIntent(requireContext()));
        }
    };

    @NonNull
    private final Observer<Administrator> administratorObserver = administrator -> {
        TextViewUtil.update(binding.administratorDetailsEmailFieldValue, administrator.getEmail());
    };

    @Nullable
    private AlertDialog dialog;

    @NonNull
    private final Observer<AdministratorInformationDialogType> dialogTypeObserver = dialogType -> {
        if (dialogType == null) {
            if (dialog != null) {
                dialog.dismiss();
            }
            dialog = null;
        } else if (dialogType == AdministratorInformationDialogType.LOGOUT_CONFIRMATION) {
            dialog = new AlertDialog.Builder(requireActivity())
                .setTitle("Odjava")
                .setMessage("Jeste li sigurni da se želite odjaviti?")
                .setPositiveButton("Odjavi se", (dialog, which) -> viewModel.logout())
                .setNegativeButton("Odustani", (dialog, which) -> viewModel.dismissDialog())
                .setOnCancelListener(dialog -> viewModel.dismissDialog())
                .setOnDismissListener(dialog -> viewModel.dismissDialog())
                .create();
            dialog.show();
        } else if (dialogType == AdministratorInformationDialogType.ERROR) {
            dialog = new AlertDialog.Builder(requireActivity())
                .setTitle("Greška")
                .setMessage("Došlo je do neočekivane pogreške. Pokušajte ponovo.")
                .setPositiveButton("U redu", (dialog, which) -> viewModel.dismissDialog())
                .setOnCancelListener(dialog -> viewModel.dismissDialog())
                .setOnDismissListener(dialog -> viewModel.dismissDialog())
                .create();
            dialog.show();
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentAdministratorInformationBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.administratorDetailsLogoutButton.setOnClickListener(v -> viewModel.onLogoutClicked());
        viewModel = new ViewModelProvider(this).get(AdministratorInformationViewModel.class);
        viewModel.getAdministrator().observe(getViewLifecycleOwner(), administratorObserver);
        viewModel.getDialogType().observe(getViewLifecycleOwner(), dialogTypeObserver);
        viewModel.getEvents().observe(getViewLifecycleOwner(), eventObserver);
        viewModel.initialize();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
