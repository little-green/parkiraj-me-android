package hr.fer.littlegreen.parkirajme.androidapp.data.http.models.parkingobject;

import org.jetbrains.annotations.NotNull;

import java.math.BigDecimal;
import java.util.Objects;

public class ParkingObjectRequest {

    @NotNull
    private final String name;

    @NotNull
    private final String address;

    @NotNull
    private final int capacity;

    @NotNull
    private final int price;

    @NotNull
    private final int free_slots;

    @NotNull
    private final BigDecimal latitude;

    @NotNull
    private final BigDecimal longitude;

    public ParkingObjectRequest(
        @NotNull String objectName,
        @NotNull String objectAddress,
        int capacity,
        int thirtyMinutePrice,
        int freeSlots, @NotNull BigDecimal latitude,
        @NotNull BigDecimal longitude
    ) {
        this.name = objectName;
        this.address = objectAddress;
        this.capacity = capacity;
        this.price = thirtyMinutePrice;
        this.free_slots = freeSlots;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ParkingObjectRequest that = (ParkingObjectRequest) o;
        return capacity == that.capacity &&
            price == that.price &&
            name.equals(that.name) &&
            address.equals(that.address) &&
            latitude.equals(that.latitude) &&
            longitude.equals(that.longitude);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, address, capacity, price, latitude, longitude);
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public int getCapacity() {
        return capacity;
    }

    public int getPrice() {
        return price;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public int getFree_slots() {
        return free_slots;
    }
}
