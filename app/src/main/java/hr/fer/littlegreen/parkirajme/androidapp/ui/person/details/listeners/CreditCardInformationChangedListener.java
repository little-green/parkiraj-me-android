package hr.fer.littlegreen.parkirajme.androidapp.ui.person.details.listeners;

import java.time.YearMonth;

import androidx.annotation.NonNull;

public interface CreditCardInformationChangedListener {

    void onCreditCardInformationChanged(@NonNull String creditCardNumber, @NonNull YearMonth creditCardExpirationDate);
}
