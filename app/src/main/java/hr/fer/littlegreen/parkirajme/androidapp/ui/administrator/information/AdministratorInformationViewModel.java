package hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.information;

import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import hr.fer.littlegreen.parkirajme.androidapp.data.http.ParkirajMeService;
import hr.fer.littlegreen.parkirajme.androidapp.data.local.LocalStorage;
import hr.fer.littlegreen.parkirajme.androidapp.domain.models.Administrator;
import hr.fer.littlegreen.parkirajme.androidapp.ui.BaseViewModel;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.information.events.AdministratorInformationEvent;
import hr.fer.littlegreen.parkirajme.androidapp.ui.administrator.information.state.AdministratorInformationDialogType;
import hr.fer.littlegreen.parkirajme.androidapp.ui.company.information.events.LogoutEvent;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class AdministratorInformationViewModel extends BaseViewModel<LogoutEvent> {

    @NonNull
    private final ParkirajMeService parkirajMeService;

    @NonNull
    private final LocalStorage localStorage;

    @NonNull
    private final MutableLiveData<Administrator> administrator = new MutableLiveData<>();

    @NonNull
    private final MutableLiveData<AdministratorInformationDialogType> dialogType = new MutableLiveData<>();

    @ViewModelInject
    public AdministratorInformationViewModel(
        @NonNull ParkirajMeService parkirajMeService,
        @NonNull LocalStorage localStorage
    ) {
        this.parkirajMeService = parkirajMeService;
        this.localStorage = localStorage;
    }

    public void initialize() {
        administrator.setValue((Administrator) localStorage.getSession().getUser());
    }

    public void onLogoutClicked() {
        dialogType.setValue(AdministratorInformationDialogType.LOGOUT_CONFIRMATION);
    }

    public void dismissDialog() {
        dialogType.setValue(null);
    }

    public void logout() {
        dialogType.setValue(null);
        disposable.add(
            parkirajMeService.logout()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    localStorage.clearSession();
                    emitEvent( new LogoutEvent()); //trebalo bi raditi
                }, throwable -> dialogType.setValue(AdministratorInformationDialogType.ERROR))
        );
    }

    @NonNull
    public LiveData<Administrator> getAdministrator() {
        return administrator;
    }

    @NonNull
    public LiveData<AdministratorInformationDialogType> getDialogType() {
        return dialogType;
    }
}
