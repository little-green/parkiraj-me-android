package hr.fer.littlegreen.parkirajme.androidapp.domain.models;

public enum ReservationType {

    ONE_TIME,
    PERIODIC,
    ALL_TIME
}
