package hr.fer.littlegreen.parkirajme.androidapp.data.http.models.company;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public final class CompanyEditRequest {

    @NotNull
    private final String name;

    @NotNull
    private final String headquarterAddress;

    public CompanyEditRequest(@NotNull String name, @NotNull String headquarterAddress) {
        this.name = name;
        this.headquarterAddress = headquarterAddress;
    }

    public String getName() {
        return name;
    }

    public String getHeadquarterAddress() {
        return headquarterAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CompanyEditRequest that = (CompanyEditRequest) o;
        return name.equals(that.name) &&
            headquarterAddress.equals(that.headquarterAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, headquarterAddress);
    }
}
